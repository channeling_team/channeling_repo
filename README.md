# Channeling Web

La aplicación pretende solventar los problemas generados en la capa de conectividad cada vez que un cliente propone utilizar una nomenclatura diferente a la estándar, o utiliza distintos protocolos de cifrado y firma, así como la visualización de esta información.

## Tecnologías

Channeling utiliza una serie de proyectos open source para su correcot funcionamiento:

* [AngularJS] - HTML enhanced for web apps!
* [Webpack] - module bundler for JavaScript files usage in a browser
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend
* [npm] - package manager for JavaScript
* [Gulp] - the streaming build system
* [Spring] - designed to get you up and running as quickly as possible
* [Maven] - software project management and comprehension tool

### Instalación

Channeling requiere de maven para generar el war.

Install the dependencies and devDependencies and start the server.
Instalar las dependencias y generar el war.

```sh
$ cd channeling
$ mvn clean package
```

#### Compilando fuentes front
Para compilar las fuentes html para producción
```sh
$ cd src/front
$ npm run build
```

#### Desarrollo Front
Instalar dependencias npm
```sh
$ cd src/front
$ npm install
$ npm run serve
```

