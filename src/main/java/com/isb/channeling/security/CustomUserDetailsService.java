package com.isb.channeling.security;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.isb.channeling.service.UserService;

import User.ListarUsuariosResponse.User;

/**
 * The Class CustomUserDetailsService.
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

  /** The user service. */
  @Resource(name = "userService")
  private UserService userService;

  /** The Constant ROLE_PREFIX. */
  private static final String ROLE_PREFIX = "ROLE_";

  /*
   * (non-Javadoc)
   * 
   * @see org.springframework.security.core.userdetails.UserDetailsService#
   * loadUserByUsername(java.lang.String)
   */
  @Override
  public UserDetails loadUserByUsername(String username){

    if (StringUtils.isBlank(username)) {
      throw new UsernameNotFoundException("Username is empty");
    }

    User user = userService.logInUser(username.toUpperCase());
    if (user == null) {
      throw new UsernameNotFoundException("User " + username + " not found");
    }

    return new org.springframework.security.core.userdetails.User(user.getName(), StringUtils.EMPTY,
        getGrantedAuthorities(user));
  }

  /**
   * Gets the granted authorities.
   *
   * @param user
   *          the user
   * @return the granted authorities
   */
  private List<GrantedAuthority> getGrantedAuthorities(User user) {
    final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + StringUtils.upperCase(user.getRole())));
    return authorities;
  }

  // FOR TESTS
  /**
   * set user service to test
   * 
   * @param service
   */
  public void setUserService(UserService service) {
    this.userService = service;
  }
}
