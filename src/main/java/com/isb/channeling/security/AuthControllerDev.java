package com.isb.channeling.security;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.Closure;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.util.AuthUtil;
import com.isb.channeling.util.Utils;

/**
 * The Class AuthController.
 */
@Controller
@ConditionalOnExpression("${mock.auth.enabled}")
public class AuthControllerDev {

  private static final String REDIRECT = "redirect:";

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(AuthControllerDev.class.getName());

  /** The url logout. */
  @Value("${url.logout}")
  private String urlLogout;

  @Value("${mock.token}")
  private String mockToken;

  /** The custom user details service. */
  @Resource(name = "customUserDetailsService")
  private UserDetailsService customUserDetailsService;

  /**
   * Authenticate.BRUSSELSL
   *
   * @param urlToken the url token
   * @param cookieToken the cookie token
   * @param request the request
   * @return the string
   */
  @GetMapping("/auth")
  public String authenticate(@RequestParam(name = "token", required = false) String urlToken,
      @CookieValue(name = "NewUniversalCookie", required = false) String cookieToken) {

    // Validating user in token SSO
    LOGGER.info("Validating token ");
    String redirectTo = null;
    String username = null;

    try {
      if (StringUtils.isBlank(urlToken)) {
        urlToken = mockToken;
        LOGGER.info("Token from url: " + urlToken);
        username = AuthUtil.getUserNameFromToken(new String(Base64.decodeBase64(urlToken)));
        redirectTo = REDIRECT + "/";
      } else if (StringUtils.isNotBlank(cookieToken)) {
        LOGGER.info("Token from Cookie: " + cookieToken);
        username = AuthUtil.getUserNameFromCookie(cookieToken);
        redirectTo = REDIRECT + "/";
      } else {
        LOGGER.info("No token from cookie or url found");
        redirectTo = REDIRECT + urlLogout;
      }

      LOGGER.info("Logging in: " + username);
      if (username != null) {
        UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken userToken = new UsernamePasswordAuthenticationToken(userDetails,
            userDetails.getPassword(), userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(userToken);
      }
    } catch (RuntimeException e) {
      LOGGER.error("Login failed. Redirecting to logout url", e);
      redirectTo = REDIRECT + urlLogout;
    }

    return redirectTo;
  }

  /**
   * Gets the roles.
   *
   * @param request the request
   * @return the roles
   */
  @ResponseBody
  @GetMapping("/auth/roles")
  public JsonResponse<List<String>> getRoles() {
    LOGGER.info("Getting user roles");

    final List<String> response = new ArrayList<>();
    IteratorUtils.forEach(Utils.getLoggedInAuthorities().iterator(), new Closure<GrantedAuthority>() {

      @Override
      public void execute(GrantedAuthority auth) {
        response.add(auth.getAuthority());
      }
    });
    LOGGER.debug("getRoles: " + response);
    return new JsonResponse<>(response);
  }

  /**
   * Gets the heartbeat.
   *
   * @param request the request
   * @return the heartbeat
   */
  @ResponseBody
  @GetMapping("/rfraccs")
  public ResponseEntity<Void> getHeartbeat() {
    LOGGER.info("Getting heartbeat");
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Logout.
   *
   * @return the string
   */
  @GetMapping("/exit")
  public String logout() {
    return REDIRECT + urlLogout;
  }

  /**
   * set CustomUserDetailsService to test
   * 
   * @param customUserDetailsService
   */
  public void setCustomUserDetailsService(UserDetailsService customUserDetailsService) {
    this.customUserDetailsService = customUserDetailsService;
  }
}
