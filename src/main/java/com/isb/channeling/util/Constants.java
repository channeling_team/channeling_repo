package com.isb.channeling.util;

/**
 * The Class Constants.
 */
public class Constants {

  /**
   * Instantiates a new constants.
   */
  private Constants() {
  }

  /** The Constant sZERO. */
  public static final String ZERO = "0";

  /** The Constant sONE. */
  public static final String ONE = "1";

  /** The Constant OK. */
  public static final String OK = "OK";

}
