package com.isb.channeling.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.isb.channeling.domain.ChannelingException;
import com.isb.channeling.domain.Cookie;
import com.isb.channeling.domain.TokenDefinition;

/**
 * The Class AuthUtil.
 */
public class AuthUtil {

  /**
   * Instantiates a new auth util.
   */
  private AuthUtil() {
  }

  /** The Constant SHA_RSA. */
  public static final String SHA_RSA = "SHA1withRSA";

  /** The Constant RSA. */
  public static final String RSA = "RSA";

  /** The Constant HASH. */
  public static final String HASH = "#";

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(AuthUtil.class.getName());

  /**
   * Checks for expired.
   *
   * @param token
   *          the token
   * @return true, if successful
   */
  public static boolean hasExpired(String token) {
    final String[] tParts = token.split(HASH);
    long expiry = Long.valueOf(tParts[2]);
    boolean expired = System.currentTimeMillis() > expiry;
    LOGGER.info("Token expired: " + expired);
    return expired ;
  }

  /**
   * Gets the ldap search key from token.
   *
   * @param token
   *          the token
   * @return the ldap search key from token
   */
  public static String getLdapSearchKeyFromToken(String token) {
    if (token == null) {
      return null;
    }

    final String[] tParts = token.split(HASH);
    return tParts[6].concat("_").concat(tParts[8]);
  }

  /**
   * Gets the ldap search key from cookie.
   *
   * @param token
   *          the token
   * @return the ldap search key from cookie
   */
  public static String getLdapSearchKeyFromCookie(String token) {
    if (token == null) {
      return null;
    }

    final String[] tParts = token.split(HASH);
    return tParts[9].concat("_").concat(tParts[11]);
  }

  /**
   * Gets the user name from token.
   *
   * @param token
   *          the token
   * @return the user name from token
   */
  public static String getUserNameFromToken(String token) {
    String username = null;
    if (token == null) {
      return null;
    }

    final String[] tParts = token.split(HASH);
    byte[] xmlInfo = Base64.decodeBase64(tParts[3]);

    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(TokenDefinition.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      InputStream is = new ByteArrayInputStream(xmlInfo);
      TokenDefinition td = (TokenDefinition) jaxbUnmarshaller.unmarshal(is);
      username = td.getUserID();
    } catch (JAXBException e) {
      LOGGER.error(e);
    }
    return username;
  }

  /**
   * Gets the user name from cookie.
   *
   * @param data
   *          the data
   * @return the user name from cookie
   */
  public static String getUserNameFromCookie(String data) {
    String username = null;
    if (data == null) {
      return null;
    }

    final String[] tParts = data.split(HASH);
    byte[] xmlInfo = Base64.decodeBase64(tParts[6]);

    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Cookie.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      InputStream is = new ByteArrayInputStream(xmlInfo);
      Cookie cookie = (Cookie) jaxbUnmarshaller.unmarshal(is);
      username = cookie.getUserID();
    } catch (JAXBException e) {
      LOGGER.error(e);
    }

    return username;
  }

  /**
   * Token is valid.
   *
   * @param token
   *          the token
   * @param keyBytes
   *          the key bytes
   * @return true, if successful
   * @throws NoSuchAlgorithmException
   *           the no such algorithm exception
   * @throws InvalidKeySpecException
   *           the invalid key spec exception
   * @throws InvalidKeyException
   *           the invalid key exception
   * @throws SignatureException
   *           the signature exception
   */
  public static boolean tokenIsValid(String token, byte[] keyBytes) {

    boolean valid = false;
    byte[] firma;
    Signature firmador;
    try {
      int k = StringUtils.lastIndexOf(token, HASH);
      firma = Base64.decodeBase64(StringUtils.substring(token, k + 1));

      firmador = Signature.getInstance(SHA_RSA);

      X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(keyBytes);
      KeyFactory keyFactory = KeyFactory.getInstance(RSA);
      RSAPublicKey clavePublica = (RSAPublicKey) keyFactory.generatePublic(pubKeySpec);

      firmador.initVerify(clavePublica);
      firmador.update(StringUtils.substring(token, 0, k + 1).getBytes());
      valid = firmador.verify(firma);
    } catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | SignatureException e) {
      LOGGER.error("Error validating token", e);
      throw new ChannelingException(e.getMessage(), e);
    }

    return valid;
  }

  /**
   * Token is in valid.
   *
   * @param authToken
   *          the auth token
   * @param keyBytes
   *          the key bytes
   * @return true, if successful
   * @throws InvalidKeyException
   *           the invalid key exception
   * @throws NoSuchAlgorithmException
   *           the no such algorithm exception
   * @throws InvalidKeySpecException
   *           the invalid key spec exception
   * @throws SignatureException
   *           the signature exception
   */
  public static boolean tokenIsInValid(String authToken, byte[] keyBytes) {
    return !tokenIsValid(authToken, keyBytes);
  }
}
