package com.isb.channeling.util.logging;

import ch.qos.logback.classic.PatternLayout;

/**
 * The Class PatternLayoutWithUserContext.
 */
public class PatternLayoutWithUserContext extends PatternLayout {
  static {
    PatternLayout.defaultConverterMap.put("user", UserConverter.class.getName());
    PatternLayout.defaultConverterMap.put("session", SessionConverter.class.getName());
  }
}
