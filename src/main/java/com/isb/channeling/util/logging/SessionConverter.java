package com.isb.channeling.util.logging;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * The Class SessionConverter.
 */
public class SessionConverter extends ClassicConverter {

  /*
   * (non-Javadoc)
   * 
   * @see ch.qos.logback.core.pattern.Converter#convert(java.lang.Object)
   */
  @Override
  public String convert(ILoggingEvent event) {
    RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
    if (attrs != null) {
      return attrs.getSessionId();
    }
    return "NO_SESSION";
  }
}
