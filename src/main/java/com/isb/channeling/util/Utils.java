package com.isb.channeling.util;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * The Class Utils.
 */
public class Utils {

  /**
   * Instantiates a new utils.
   */
  private Utils() {
  }

  /**
   * Gets the logged in user.
   *
   * @return the logged in user
   */
  public static String getLoggedInUser() {
    return SecurityContextHolder.getContext().getAuthentication().getName();
  }

  /**
   * Gets the logged in authorities.
   *
   * @return the authorities
   */
  @SuppressWarnings("unchecked")
  public static Collection<GrantedAuthority> getLoggedInAuthorities() {
    return (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
  }

}
