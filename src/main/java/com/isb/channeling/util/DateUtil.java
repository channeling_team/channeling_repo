package com.isb.channeling.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * The Class DateUtil.
 */
public class DateUtil {

  /**
   * Instantiates a new date util.
   */
  private DateUtil() {
  }

  /** The Constant DEFAULT_TIME_ZONE. */
  public static final String DEFAULT_TIME_ZONE = "CET";

  /** The Constant GMT_TIME_ZONE. */
  public static final String GMT_TIME_ZONE = "GMT";

  /** The Constant NG_TIME_FORMAT. */
  public static final String NG_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

  /** The Constant DEFAULT_TIME_FORMAT. */
  public static final String DEFAULT_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(DateUtil.class.getName());

  /**
   * Convert GM tto CET.
   *
   * @param input
   *          the input
   * @return the string
   */
  public static String convertGMTtoCET(String input) {

    if (StringUtils.isEmpty(input)) {
      return null;
    }

    SimpleDateFormat sdfgmt = new SimpleDateFormat(NG_TIME_FORMAT);
    sdfgmt.setTimeZone(TimeZone.getTimeZone(GMT_TIME_ZONE));

    SimpleDateFormat sdfmad = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
    sdfmad.setTimeZone(TimeZone.getTimeZone(DEFAULT_TIME_ZONE));

    Date inptdate = null;
    try {
      inptdate = sdfgmt.parse(input);
    } catch (ParseException e) {
      LOGGER.error(e);
      return null;
    }
    return sdfmad.format(inptdate);
  }

}
