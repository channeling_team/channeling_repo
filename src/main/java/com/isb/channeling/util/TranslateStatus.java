package com.isb.channeling.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class TranslateStatus.
 */
public class TranslateStatus {

  /**
   * Instantiates a new translate status.
   */
  private TranslateStatus() {
  }

  /**
   * Convert id to status.
   *
   * @param input
   *          the input
   * @return the string
   */
  public static String convertIdtoStatus(String input) {

    Map<String, String> output = new HashMap<String, String>();
    output.put("1", "STATE_RECEIVED");
    output.put("2", "STATE_PGP_OK");
    output.put("3", "STATE_PGP_KO");
    output.put("4", "STATE_CYPHER_OK");
    output.put("5", "STATE_CYPHER_KO");
    output.put("6", "STATE_AUTH_OK");
    output.put("7", "STATE_AUTH_KO");
    output.put("8", "STATE_SIGNED_OK");
    output.put("9", "STATE_SIGNED_KO");
    output.put("10", "STATE_BANKSPHERE_OK");
    output.put("11", "STATE_BANKSPHERE_KO");
    output.put("12", "STATE_3SKEY_OK");
    output.put("13", "STATE_BANKSPHERE_RESP_KO");
    output.put("14", "STATE_FILE_OK");
    output.put("15", "STATE_FILE_KO");
    output.put("16", "STATE_RENAMED_OK");
    output.put("17", "STATE_RENAMED_KO");
    output.put("18", "STATE_GEMC_OK");
    output.put("19", "STATE_GEMC_KO");
    output.put("20", "STATE_IPLA_OK");
    output.put("21", "STATE_IPLA_KO");
    output.put("22", "STATE_MFT_OK");
    output.put("23", "STATE_MFT_KO");
    output.put("98", "STATE_CUSTOMER_NOTFOUND_KO");
    output.put("99", "STATE_UNEXPECTED_ERROR_KO");

    String salida = output.get(input);
    if (StringUtils.isEmpty(salida)) {
      salida = " ";
    }
    return salida;
  }
}
