package com.isb.channeling.util;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;

import com.isb.channeling.service.impl.SoapService;

/**
 * The Class ChannX509TrustManager.
 */
public class ChannX509TrustManager implements X509TrustManager {

  /** The Constant CERTIFICATE_NOT_VALID_OR_TRUSTED. */
  private static final String CERTIFICATE_NOT_VALID_OR_TRUSTED = "Certificate not valid or trusted.";
  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(SoapService.class.getName());

  /**
   * Gets the accepted issuers.
   *
   * @return the accepted issuers
   */
  public X509Certificate[] getAcceptedIssuers() {
    // Empty arrays and collections should be returned instead of null
    return new X509Certificate[0];
  }

  /**
   * Check server trusted.
   *
   * @param certs the certs
   * @param authType the auth type
   * @throws CertificateException the certificate exception
   */
  public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
    // No empty methods
    try {
      certs[0].checkValidity();
    } catch (Exception e) {
      LOGGER.info(CERTIFICATE_NOT_VALID_OR_TRUSTED, e);
      throw new CertificateException(CERTIFICATE_NOT_VALID_OR_TRUSTED);
    }
  }

  /**
   * Check client trusted.
   *
   * @param certs the certs
   * @param authType the auth type
   * @throws CertificateException the certificate exception
   */
  public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
    // No empty methods
    try {
      certs[0].checkValidity();
    } catch (Exception e) {
      LOGGER.info(CERTIFICATE_NOT_VALID_OR_TRUSTED, e);
      throw new CertificateException(CERTIFICATE_NOT_VALID_OR_TRUSTED);
    }
  }
}
