package com.isb.channeling.service;

import java.util.List;

import User.EditarUsuariosRequest;
import User.ListarUsuariosResponse.User;
import User.NuevoUsuarioRequest;

/**
 * The Interface UserService.
 */
public interface UserService {

  /**
   * Find all users.
   *
   * @return the list
   */
  List<User> findAllUsers();

  /**
   * Find by name.
   *
   * @param username
   *          the username
   * @return the user
   */
  User findByName(String username);
  
  /**
   * Log In user.
   *
   * @param username
   *          the username
   * @return the user
   */
  User logInUser(String username);

  /**
   * Update user.
   *
   * @param currentUser
   *          the current user
   * @return the string
   */
  String updateUser(EditarUsuariosRequest currentUser);

  /**
   * Creates the user.
   *
   * @param currentUser
   *          the current user
   * @return the string
   */
  String createUser(NuevoUsuarioRequest currentUser);

  /**
   * Delete user by id.
   *
   * @param id
   *          the id
   * @return the string
   */
  String deleteUserById(Number id);
}
