package com.isb.channeling.service;

import java.util.List;

import FileMonitor.BuscarFicheroRequest;
import FileMonitor.BuscarFicheroResponse.Fichero;

/**
 * The Interface FileMonitorService.
 */
public interface FileMonitorService {

  /**
   * Search.
   *
   * @param file
   *          the file
   * @return the list
   */
  List<Fichero> search(BuscarFicheroRequest file);

}
