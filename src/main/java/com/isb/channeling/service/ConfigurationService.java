package com.isb.channeling.service;

import Configuration.AltaConfiguracionRequest;
import Configuration.AltaListaConfiguracionesRequest;
import Configuration.AltaListaConfiguracionesResponse;
import Configuration.EditarConfiguracionRequest;
import Configuration.ListarConfiguracionResponse;

/**
 * The Interface ConfigurationService.
 */
public interface ConfigurationService {

  /**
   * Find by id.
   *
   * @param id
   *          the id
   * @return the listar configuracion response
   */
  ListarConfiguracionResponse findById(final Number id);

  /**
   * Creates the configuration.
   *
   * @param currentConfiguration
   *          the current configuration
   * @return the string
   */
  String createConfiguration(AltaConfiguracionRequest currentConfiguration);

  /**
   * Update configuration.
   *
   * @param currentConfiguration
   *          the current configuration
   * @return the string
   */
  String updateConfiguration(EditarConfiguracionRequest currentConfiguration);

  /**
   * Delete configuration by id.
   *
   * @param id
   *          the id
   * @return the string
   */
  String deleteConfigurationById(final Number id);

  /**
   * Creates the configuration list.
   *
   * @param altaConf
   *          the alta conf
   * @return the alta lista configuraciones response
   */
  AltaListaConfiguracionesResponse createConfigurationList(AltaListaConfiguracionesRequest altaConf);

}
