package com.isb.channeling.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.isb.channeling.domain.ChannelingException;
import com.isb.channeling.util.ChannX509TrustManager;

/**
 * The Class SoapService.
 */
public class SoapService {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(SoapService.class.getName());

  /**
   * Constructor
   */
  private SoapService() {
    super();
  }

  /**
   * Method to initate connection.
   *
   * @param urlval the urlval
   * @param message the message
   * @return the SOAP message
   */
  public static SOAPMessage talk(String urlval, SOAPMessage message) {
    long time_start, time_end;
    time_start = System.currentTimeMillis();
    SOAPMessage rp = null;
    LOGGER.info("url = " + urlval);
    // View input
    LOGGER.debug("Soap request: " + toString(message));

    // Trust to certificates
    doTrustToCertificates();
    rp = sendMessage(message, urlval);

    // View the output
    LOGGER.debug("XML response: " + toString(rp));
    time_end = System.currentTimeMillis();
    LOGGER.info("----> Tiempo llamada WS " + (time_end - time_start) + " milisegundos");
    return rp;

  }

  /**
   * Send message to Sterling.
   *
   * @param message the message
   * @param endPoint the end point
   * @return the SOAP message
   * @throws Exception
   * @throws UnsupportedOperationException
   * @throws MalformedURLException the malformed URL exception
   * @throws SOAPException the SOAP exception
   */
  public static SOAPMessage sendMessage(SOAPMessage message, String endPoint) {
    SOAPMessage result = null;
    if (endPoint != null && message != null) {
      SOAPConnection connection = null;
      try {
        URL url = new URL(endPoint);
        SOAPConnectionFactory scf = SOAPConnectionFactory.newInstance();
        connection = scf.createConnection(); // point-to-point
        result = connection.call(message, url); // connection
      } catch (UnsupportedOperationException | SOAPException | MalformedURLException e) {
        LOGGER.error("SOAP Error:", e);
        throw new ChannelingException(e.getMessage(), e);
      } finally {
        try {
          connection.close();
        } catch (SOAPException soape) {
          LOGGER.info("Can't close SOAPConnection:" + soape);
        }
      }
    }
    return result;
  }

  /**
   * Method to create connection SSL.
   * 
   * @throws NoSuchAlgorithmException
   * @throws KeyManagementException
   * @throws Exception the exception
   */
  public static void doTrustToCertificates() {
    TrustManager[] trustAllCerts = new TrustManager[] { new ChannX509TrustManager() };

    SSLContext sc;
    try {
      sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new SecureRandom());

      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
      HostnameVerifier hv = new HostnameVerifier() {

        public boolean verify(String urlHostName, SSLSession session) {
          if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
            LOGGER.info("Warning: URL host '" + urlHostName + "' is different to SSLSession host '"
                + session.getPeerHost() + "'.");
          }
          return true;
        }
      };
      HttpsURLConnection.setDefaultHostnameVerifier(hv);
    } catch (NoSuchAlgorithmException | KeyManagementException e) {
      LOGGER.error("HTTPs Error:", e);
    }

  }
  
  
  /**
   * To string.
   *
   * @param msg the msg
   * @return the string
   */
  public static String toString(SOAPMessage msg){
    if(msg == null){
      return StringUtils.EMPTY;
    }
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    try {
      msg.writeTo(out);
      
    } catch (SOAPException | IOException e) {
      LOGGER.error("Soap message print error:", e);
    }
    return new String(out.toByteArray());
    
  }

}
