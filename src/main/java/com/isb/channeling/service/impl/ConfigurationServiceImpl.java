package com.isb.channeling.service.impl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.isb.channeling.service.ConfigurationService;
import com.isb.channeling.util.Utils;

import Configuration.AltaConfiguracionRequest;
import Configuration.AltaConfiguracionResponse;
import Configuration.AltaListaConfiguracionesRequest;
import Configuration.AltaListaConfiguracionesResponse;
import Configuration.EditarConfiguracionRequest;
import Configuration.EditarConfiguracionResponse;
import Configuration.EliminarConfiguracionRequest;
import Configuration.EliminarConfiguracionResponse;
import Configuration.ListarConfiguracionRequest;
import Configuration.ListarConfiguracionResponse;

/**
 * The Class ConfigurationServiceImpl.
 */
@Service(value = "configurationService")
public class ConfigurationServiceImpl implements ConfigurationService {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(ConfigurationServiceImpl.class.getName());

  /** The s response. */
  private SOAPMessage sResponse = null;

  /** The url config. */
  @Value("${url.config.list}")
  private String urlConfig;

  /** The url configuration new. */
  @Value("${url.config.create}")
  private String urlConfigurationNew;

  /** The url configuration delete. */
  @Value("${url.config.delete}")
  private String urlConfigurationDelete;

  /** The url configuration edit. */
  @Value("${url.config.edit}")
  private String urlConfigurationEdit;

  /** The url configuration list new. */
  @Value("${url.config.create.list}")
  private String urlConfigurationListNew;

  /**
   * Metodo que realiza la llamada a los servicios ws y que puede utilizarse
   * desde los mocks.
   *
   * @param sRequest
   *          the s request
   * @param urlConfig
   *          the url config
   * @return the SOAP message
   */
  protected SOAPMessage callService(SOAPMessage sRequest, String urlConfig) {
    return SoapService.talk(urlConfig, sRequest);
  }

  /**
   * Busca una configuración por ID.
   *
   * @param id
   *          the id
   * @return the listar configuracion response
   */
  public ListarConfiguracionResponse findById(final Number id) {
    LOGGER.info("Searching for config");
    ListarConfiguracionResponse response = null;

    ListarConfiguracionRequest request = new ListarConfiguracionRequest();

    Document document;
    try {
      request.setId(id.toString());
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(ListarConfiguracionRequest.class).createMarshaller();
      marshaller.marshal(request, document);

      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlConfig);
      JAXBContext jc = JAXBContext.newInstance(ListarConfiguracionResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();

      response = (ListarConfiguracionResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error findById: ", e);
    }

    return response;
  }

  /**
   * Método para crear una configuración a través de WS.
   *
   * @param currentConfiguration
   *          the current configuration
   * @return the string
   */
  public String createConfiguration(AltaConfiguracionRequest currentConfiguration) {
    String id = StringUtils.EMPTY;
    try {
      LOGGER.info("Tipo renombrado altaConfig " + currentConfiguration.getRenombrado().getTipo());
      currentConfiguration
          .setSubalias(StringUtils.stripAccents(StringUtils.upperCase(currentConfiguration.getSubalias())));
      currentConfiguration
          .setNameConf(StringUtils.stripAccents(StringUtils.upperCase(currentConfiguration.getNameConf())));
      currentConfiguration.setDescripcion(StringUtils.stripAccents(currentConfiguration.getDescripcion()));
      currentConfiguration.setAuduserConf(Utils.getLoggedInUser());
      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(AltaConfiguracionRequest.class).createMarshaller();
      marshaller.marshal(currentConfiguration, document);

      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlConfigurationNew);

      JAXBContext jc = JAXBContext.newInstance(AltaConfiguracionResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      AltaConfiguracionResponse response = (AltaConfiguracionResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());
      id = response.getRsp();
      LOGGER.info("createConfiguration response" + response.toString());
    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error createConfiguration: ", e);
    }
    return id;
  }

  /**
   * Método para actualizar una configuración a través de WS.
   *
   * @param currentConfiguration
   *          the current configuration
   * @return the string
   */
  public String updateConfiguration(EditarConfiguracionRequest currentConfiguration) {
    String id = StringUtils.EMPTY;
    try {
      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(EditarConfiguracionRequest.class).createMarshaller();
      currentConfiguration
          .setSubalias(StringUtils.stripAccents(StringUtils.upperCase(currentConfiguration.getSubalias())));
      currentConfiguration
          .setNameConf(StringUtils.stripAccents(StringUtils.upperCase(currentConfiguration.getNameConf())));
      currentConfiguration.setDescripcion(StringUtils.stripAccents(currentConfiguration.getDescripcion()));
      currentConfiguration.setAuduserConf(Utils.getLoggedInUser());
      marshaller.marshal(currentConfiguration, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlConfigurationEdit);

      JAXBContext jc = JAXBContext.newInstance(EditarConfiguracionResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      EditarConfiguracionResponse response = (EditarConfiguracionResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      id = response.getRsp();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error updateConfiguration: ", e);
    }
    return id;
  }

  /**
   * Método para borrar una configuración por su ID.
   *
   * @param id
   *          the id
   * @return the string
   */
  public String deleteConfigurationById(Number id) {
    String rsp = StringUtils.EMPTY;
    try {
      EliminarConfiguracionRequest request = new EliminarConfiguracionRequest();
      request.setId(String.valueOf(id));
      request.setAuduserConf(Utils.getLoggedInUser());

      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(EliminarConfiguracionRequest.class).createMarshaller();
      marshaller.marshal(request, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlConfigurationDelete);

      JAXBContext jc = JAXBContext.newInstance(EliminarConfiguracionResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      EliminarConfiguracionResponse response = (EliminarConfiguracionResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      rsp = response.getRsp();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error deleteConfigurationById: ", e);
    }
    return rsp;
  }

  /**
   * Metodo AltaListaConfiguracionesResponse.
   *
   * @param altaConf
   *          the alta conf
   * @return the alta lista configuraciones response
   */
  @Override
  public AltaListaConfiguracionesResponse createConfigurationList(AltaListaConfiguracionesRequest altaConf) {
    AltaListaConfiguracionesResponse rsp = null;
    try {

      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(AltaListaConfiguracionesRequest.class).createMarshaller();
      marshaller.marshal(altaConf, document);

      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlConfigurationListNew);

      JAXBContext jc = JAXBContext.newInstance(AltaListaConfiguracionesResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      rsp = (AltaListaConfiguracionesResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());
      LOGGER.info("createConfiguration response" + rsp.getRsp());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error createConfigurationList: ", e);
    }
    return rsp;
  }

}
