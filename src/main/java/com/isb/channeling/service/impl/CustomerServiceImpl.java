package com.isb.channeling.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.isb.channeling.service.CustomerService;
import com.isb.channeling.util.Utils;

import Customer.EditarClienteCompleteConfigRequest;
import Customer.EditarClienteCompleteConfigResponse;
import Customer.EditarClienteRequest;
import Customer.EditarClienteResponse;
import Customer.EliminarClienteRequest;
import Customer.EliminarClienteResponse;
import Customer.ListarClienteCompleteConfigRequest;
import Customer.ListarClienteCompleteConfigResponse;
import Customer.ListarClienteConfigRequest;
import Customer.ListarClienteConfigResponse;
import Customer.ListarClientesRequest;
import Customer.ListarClientesResponse;
import Customer.ListarClientesResponse.Customer;
import Customer.NuevoClienteRequest;
import Customer.NuevoClienteResponse;

/**
 * The Class CustomerServiceImpl.
 */
@Service(value = "customerService")
public class CustomerServiceImpl implements CustomerService {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(CustomerServiceImpl.class.getName());

  /** The url customer list. */
  @Value("${url.customer.list}")
  private String urlCustomerList;

  /** The url customer new. */
  @Value("${url.customer.create}")
  private String urlCustomerNew;

  /** The url customer edit. */
  @Value("${url.customer.edit}")
  private String urlCustomerEdit;

  /** The url customer configs. */
  @Value("${url.customer.configs}")
  private String urlCustomerConfigs;

  /** The url customer complete configs. */
  @Value("${url.customer.complete.configs}")
  private String urlCustomerCompleteConfigs;

  /** The url customer delete. */
  @Value("${url.customer.delete}")
  private String urlCustomerDelete;

  /** The url customer edit configs. */
  @Value("${url.customer.editConfigs}")
  private String urlCustomerEditConfigs;

  /**
   * Metodo que realiza la llamada a los servicios ws y que puede utilizarse
   * desde los mocks.
   *
   * @param sRequest
   *          the s request
   * @param urlConfig
   *          the url config
   * @return the SOAP message
   */
  protected SOAPMessage callService(SOAPMessage sRequest, String urlConfig) {
    return SoapService.talk(urlConfig, sRequest);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.isb.channeling.service.CustomerService#findAllCustomers()
   */
  @Override
  public List<Customer> findAllCustomers() {
    List<Customer> customerList = new ArrayList<>();
    try {

      ListarClientesRequest request = new ListarClientesRequest();

      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

      Marshaller marshaller = JAXBContext.newInstance(ListarClientesRequest.class).createMarshaller();
      marshaller.marshal(request, document);

      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      SOAPMessage sResponse = callService(sRequest, urlCustomerList);

      JAXBContext jc = JAXBContext.newInstance(ListarClientesResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      ListarClientesResponse response = (ListarClientesResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());
      customerList = response.getCustomer();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error findAllCustomers: ", e);
    }

    return customerList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.isb.channeling.service.CustomerService#createCustomer(Customer.
   * NuevoClienteRequest)
   */
  @Override
  public String createCustomer(NuevoClienteRequest currentCustomer) {
    Document document;
    String id = StringUtils.EMPTY;
    try {
      currentCustomer.setAudUser(Utils.getLoggedInUser());
      currentCustomer.setBic(StringUtils.EMPTY);
      currentCustomer.setName(StringUtils.stripAccents(StringUtils.upperCase(currentCustomer.getName())));
      currentCustomer.setAlias(StringUtils.stripAccents(StringUtils.upperCase(currentCustomer.getAlias())));
      currentCustomer.setDescripcion(StringUtils.stripAccents(currentCustomer.getDescripcion()));
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(NuevoClienteRequest.class).createMarshaller();
      marshaller.marshal(currentCustomer, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      SOAPMessage sResponse = callService(sRequest, urlCustomerNew);

      JAXBContext jc = JAXBContext.newInstance(NuevoClienteResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      NuevoClienteResponse response = (NuevoClienteResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      id = response.getRsp();
      LOGGER.info(response.toString());
    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error createCustomer: ", e);
    }

    return id;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.isb.channeling.service.CustomerService#findById(java.lang.Number)
   */
  @Override
  public ListarClienteConfigResponse.Customer findById(final Number id) {
    ListarClienteConfigResponse.Customer customer = null;
    try {
      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      final ListarClienteConfigRequest currentCustomer = new ListarClienteConfigRequest();
      currentCustomer.setId(String.valueOf(id));

      Marshaller marshaller = JAXBContext.newInstance(ListarClienteConfigRequest.class).createMarshaller();
      marshaller.marshal(currentCustomer, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      SOAPMessage sResponse = callService(sRequest, urlCustomerConfigs);

      JAXBContext jc = JAXBContext.newInstance(ListarClienteConfigResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      ListarClienteConfigResponse response = (ListarClienteConfigResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      customer = response.getCustomer();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error findById: ", e);
    }
    return customer;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.isb.channeling.service.CustomerService#deleteCustomerById(java.lang.
   * Number)
   */
  @Override
  public String deleteCustomerById(Number id) {
    String rsp = StringUtils.EMPTY;

    try {
      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      EliminarClienteRequest request = new EliminarClienteRequest();
      request.setId(String.valueOf(id));
      request.setAudUser(Utils.getLoggedInUser());

      Marshaller marshaller = JAXBContext.newInstance(EliminarClienteRequest.class).createMarshaller();
      marshaller.marshal(request, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      SOAPMessage sResponse = callService(sRequest, urlCustomerDelete);

      JAXBContext jc = JAXBContext.newInstance(EliminarClienteResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      EliminarClienteResponse response = (EliminarClienteResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      rsp = response.getRsp();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error deleteCustomerById: ", e);
    }
    return rsp;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.isb.channeling.service.CustomerService#updateCustomer(Customer.
   * EditarClienteRequest)
   */
  @Override
  public String updateCustomer(EditarClienteRequest currentCustomer) {
    Document document;
    String id = StringUtils.EMPTY;
    try {
      currentCustomer.setName(StringUtils.stripAccents(StringUtils.upperCase(currentCustomer.getName())));
      currentCustomer.setAlias(StringUtils.stripAccents(StringUtils.upperCase(currentCustomer.getAlias())));
      currentCustomer.setDescripcion(StringUtils.strip(currentCustomer.getDescripcion()));
      currentCustomer.setAudUser(Utils.getLoggedInUser());
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(EditarClienteRequest.class).createMarshaller();
      marshaller.marshal(currentCustomer, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      SOAPMessage sResponse = callService(sRequest, urlCustomerEdit);

      JAXBContext jc = JAXBContext.newInstance(EditarClienteResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      EditarClienteResponse response = (EditarClienteResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      id = response.getRsp();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error updateCustomer: ", e);
    }

    return id;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.isb.channeling.service.CustomerService#editCustomerCompleteConfig(
   * Customer.EditarClienteCompleteConfigRequest)
   */
  @Override
  public String editCustomerCompleteConfig(EditarClienteCompleteConfigRequest currentCustomer) {
    Document document;
    String id = StringUtils.EMPTY;
    try {
      currentCustomer.setName(StringUtils.stripAccents(StringUtils.upperCase(currentCustomer.getName())));
      currentCustomer.setAlias(StringUtils.stripAccents(StringUtils.upperCase(currentCustomer.getAlias())));
      currentCustomer.setDescripcion(StringUtils.strip(currentCustomer.getDescripcion()));
      currentCustomer.setAuduser(Utils.getLoggedInUser());
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(EditarClienteCompleteConfigRequest.class).createMarshaller();
      marshaller.marshal(currentCustomer, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      SOAPMessage sResponse = callService(sRequest, urlCustomerEditConfigs);

      JAXBContext jc = JAXBContext.newInstance(EditarClienteCompleteConfigResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      EditarClienteCompleteConfigResponse response = (EditarClienteCompleteConfigResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      id = response.getId();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error editCustomerCompleteConfig: ", e);
    }

    return id;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.isb.channeling.service.CustomerService#findCompleteById(java.lang.
   * Number)
   */
  @Override
  public ListarClienteCompleteConfigResponse.Customer findCompleteById(Number id) {
    ListarClienteCompleteConfigResponse.Customer customer = null;
    try {
      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      final ListarClienteCompleteConfigRequest currentCustomer = new ListarClienteCompleteConfigRequest();
      currentCustomer.setId(String.valueOf(id));

      Marshaller marshaller = JAXBContext.newInstance(ListarClienteCompleteConfigRequest.class).createMarshaller();
      marshaller.marshal(currentCustomer, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      SOAPMessage sResponse = callService(sRequest, urlCustomerCompleteConfigs);

      JAXBContext jc = JAXBContext.newInstance(ListarClienteCompleteConfigResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      ListarClienteCompleteConfigResponse response = (ListarClienteCompleteConfigResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      customer = response.getCustomer();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error findCompleteById: ", e);
    }
    return customer;
  }
}
