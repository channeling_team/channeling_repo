package com.isb.channeling.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.isb.channeling.service.UserService;
import com.isb.channeling.util.Utils;

import User.EditarUsuariosRequest;
import User.EditarUsuariosResponse;
import User.EliminarUsuarioRequest;
import User.EliminarUsuarioResponse;
import User.ListarUsuariosRequest;
import User.ListarUsuariosResponse;
import User.ListarUsuariosResponse.User;
import User.NuevoUsuarioRequest;
import User.NuevoUsuarioResponse;

/**
 * The Class UserServiceImpl.
 */
@Service(value = "userService")
public class UserServiceImpl implements UserService {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class.getName());

  /** The s response. */
  private SOAPMessage sResponse = null;

  /** The url user list. */
  @Value("${url.user.list}")
  private String urlUserList;

  /** The url user create. */
  @Value("${url.user.create}")
  private String urlUserCreate;

  /** The url user delete. */
  @Value("${url.user.delete}")
  private String urlUserDelete;

  /** The url edit user. */
  @Value("${url.user.edit}")
  private String urlEditUser;

  /**
   * Metodo que realiza la llamada a los servicios ws y que puede utilizarse
   * desde los mocks.
   *
   * @param sRequest
   *          the s request
   * @param urlConfig
   *          the url config
   * @return the SOAP message
   */
  protected SOAPMessage callService(SOAPMessage sRequest, String urlConfig) {
    return SoapService.talk(urlConfig, sRequest);
  }

  /**
   * Method to obtain all USERS.
   *
   * @return the list
   */
  @Override
  public List<User> findAllUsers() {
    LOGGER.info("Searching for all users");
    LOGGER.info("UsersList" + urlUserList);
    List<User> userList = new ArrayList<>();

    ListarUsuariosRequest request = new ListarUsuariosRequest();

    Document document;
    try {
      request.setId("ALL");
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(ListarUsuariosRequest.class).createMarshaller();
      marshaller.marshal(request, document);

      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlUserList);

      JAXBContext jc = JAXBContext.newInstance(ListarUsuariosResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      ListarUsuariosResponse response = (ListarUsuariosResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());

      userList = response.getUser();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error findAllUsers: ", e);
    }

    return userList;
  }

  /**
   * Get a user by name.
   *
   * @param username
   *          the username
   * @return the user
   */
  @Override
  public User findByName(String username) {
    LOGGER.info("Searching for user: " + username);
    User user = null;
    LOGGER.info("UsersList" + urlUserList);
    ListarUsuariosRequest request = new ListarUsuariosRequest();

    Document document;
    try {
      request.setId(username);
      request.setLogin("0");
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(ListarUsuariosRequest.class).createMarshaller();
      marshaller.marshal(request, document);

      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlUserList);
      JAXBContext jc = JAXBContext.newInstance(ListarUsuariosResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();

      ListarUsuariosResponse response = (ListarUsuariosResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());
      LOGGER.info(response.getUser());
      if (CollectionUtils.isNotEmpty(response.getUser())) {
        user = response.getUser().get(0);
      }
    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error findByName: ", e);
    }
    return user;
  }

  @Override
  public User logInUser(String username) {
    LOGGER.info("Searching for user: " + username);
    User user = null;
    LOGGER.info("UsersList" + urlUserList);
    ListarUsuariosRequest request = new ListarUsuariosRequest();

    Document document;
    try {
      request.setId(username);
      request.setLogin("1");
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(ListarUsuariosRequest.class).createMarshaller();
      marshaller.marshal(request, document);

      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlUserList);
      JAXBContext jc = JAXBContext.newInstance(ListarUsuariosResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();

      ListarUsuariosResponse response = (ListarUsuariosResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());
      LOGGER.info(response.getUser());
      if (CollectionUtils.isNotEmpty(response.getUser())) {
        user = response.getUser().get(0);
      }
    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error logInUser: ", e);
    }
    return user;
  }
  
  /**
   * Update a user.
   *
   * @param request
   *          the request
   * @return the string
   */
  @Override
  public String updateUser(EditarUsuariosRequest request) {
    String respuesta = StringUtils.EMPTY;
    LOGGER.info("Edit user: " + request.getId());

    Document document;
    try {
      request.setName(request.getName().toUpperCase());
      request.setAudUser(Utils.getLoggedInUser());

      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(EditarUsuariosRequest.class).createMarshaller();
      marshaller.marshal(request, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlEditUser);

      JAXBContext jc = JAXBContext.newInstance(EditarUsuariosResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();

      EditarUsuariosResponse response = (EditarUsuariosResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());
      respuesta = response.getRsp();
      LOGGER.info(response.toString());

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error updateUser: ", e);
    }

    return respuesta;
  }

  /**
   * Create a user.
   *
   * @param currentUser
   *          the current user
   * @return the string
   */
  @Override
  public String createUser(NuevoUsuarioRequest currentUser) {
    String id = StringUtils.EMPTY;
    NuevoUsuarioRequest request = new NuevoUsuarioRequest();

    Document document;
    try {
      request.setEmail(currentUser.getEmail());
      request.setName(currentUser.getName().toUpperCase());
      request.setRole(currentUser.getRole());
      request.setBirthName(currentUser.getBirthName().toUpperCase());
      request.setLastName(currentUser.getLastName().toUpperCase());
      request.setAudUser(Utils.getLoggedInUser());

      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(NuevoUsuarioRequest.class).createMarshaller();
      marshaller.marshal(request, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlUserCreate);

      JAXBContext jc = JAXBContext.newInstance(NuevoUsuarioResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();

      NuevoUsuarioResponse response = (NuevoUsuarioResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());
      id = response.getRsp();
      LOGGER.info(response.toString());
    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error createUser: ", e);
    }

    return id;
  }

  /**
   * Delete user by id.
   *
   * @param id
   *          the id
   * @return the string
   */
  @Override
  public String deleteUserById(Number id) {
    EliminarUsuarioResponse response = null;
    String deleteResponse = StringUtils.EMPTY;
    try {
      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      EliminarUsuarioRequest request = new EliminarUsuarioRequest();
      request.setId(String.valueOf(id));
      request.setAudUser(Utils.getLoggedInUser());

      Marshaller marshaller = JAXBContext.newInstance(EliminarUsuarioRequest.class).createMarshaller();
      marshaller.marshal(request, document);
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      sResponse = callService(sRequest, urlUserDelete);

      JAXBContext jc = JAXBContext.newInstance(EliminarUsuarioResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();

      response = (EliminarUsuarioResponse) unmarshaller.unmarshal(sResponse.getSOAPBody().extractContentAsDocument());
      deleteResponse = response.getRsp();
      LOGGER.info(deleteResponse);

    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.error("WS error deleteUserById: ", e);
    }
    return deleteResponse;
  }
}
