package com.isb.channeling.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.isb.channeling.service.FileMonitorService;
import com.isb.channeling.util.Constants;
import com.isb.channeling.util.TranslateStatus;

import FileMonitor.BuscarFicheroRequest;
import FileMonitor.BuscarFicheroResponse;
import FileMonitor.BuscarFicheroResponse.Fichero;

/**
 * The Class FileMonitorServiceImpl.
 */
@Service(value = "fileMonitorService")
public class FileMonitorServiceImpl implements FileMonitorService {

  /** The urlfiles. */
  @Value("${url.file.list}")
  private String urlfiles;

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(FileMonitorServiceImpl.class.getName());

  /**
   * Metodo que realiza la llamada a los servicios ws y que puede utilizarse desde
   * los mocks.
   *
   * @param sRequest
   *          the s request
   * @param urlConfig
   *          the url config
   * @return the SOAP message
   */
  protected SOAPMessage callService(SOAPMessage sRequest, String urlConfig) {
    return SoapService.talk(urlConfig, sRequest);
  }

  /**
   * Check if file is empty.
   *
   * @param list
   *          the list
   * @return the list
   */
  public List<Fichero> isEmpty(List<Fichero> list) {
    for (int i = 0; i < list.size(); i++) {
      Fichero fich = list.get(i);
      list.get(i).setEstado(TranslateStatus.convertIdtoStatus(fich.getEstado()));
      if (fich.getId().compareTo(Constants.ZERO) == 0 && fich.getFormato().compareTo(Constants.ZERO) == 0
          && fich.getNombreOriginal().compareTo(Constants.ZERO) == 0) {
        list.remove(fich);
      }
    }
    return list;
  }

  /**
   * Metodo para obtener el listado de fichero que coincida con determinados
   * parámetros de búsqueda.
   *
   * @param file
   *          the file
   * @return the list
   */
  public List<Fichero> search(BuscarFicheroRequest file) {

    LOGGER.info("Búsqueda fichero" + file);
    List<Fichero> fileList = new ArrayList<>();
    Document document;

    try {
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      Marshaller marshaller = JAXBContext.newInstance(BuscarFicheroRequest.class).createMarshaller();
      marshaller.marshal(file, document);
      LOGGER.info("Request File Search");
      SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
      sRequest.getSOAPBody().addDocument(document);

      SOAPMessage sResponse = callService(sRequest, urlfiles);
      JAXBContext jc = JAXBContext.newInstance(BuscarFicheroResponse.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      BuscarFicheroResponse response = (BuscarFicheroResponse) unmarshaller
          .unmarshal(sResponse.getSOAPBody().extractContentAsDocument());
      fileList = response.getFichero();
      fileList = this.isEmpty(fileList);
      LOGGER.info("Files found: " + fileList.size());
      LOGGER.info(response.toString());
    } catch (ParserConfigurationException | JAXBException | SOAPException e) {
      LOGGER.info("WS error: ", e);
    }
    return fileList;
  }

}
