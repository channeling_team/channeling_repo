package com.isb.channeling.service;

import java.util.List;

import Customer.EditarClienteCompleteConfigRequest;
import Customer.EditarClienteRequest;
import Customer.ListarClienteCompleteConfigResponse;
import Customer.ListarClienteConfigResponse;
import Customer.ListarClientesResponse.Customer;
import Customer.NuevoClienteRequest;

/**
 * The Interface CustomerService.
 */
public interface CustomerService {

  /**
   * Creates the customer.
   *
   * @param currentCustomer
   *          the current customer
   * @return the string
   */
  String createCustomer(NuevoClienteRequest currentCustomer);

  /**
   * Find all customers.
   *
   * @return the list
   */
  List<Customer> findAllCustomers();

  /**
   * Find by id.
   *
   * @param id
   *          the id
   * @return the listar cliente config response. customer
   */
  ListarClienteConfigResponse.Customer findById(Number id);

  /**
   * Find complete by id.
   *
   * @param id
   *          the id
   * @return the listar cliente complete config response. customer
   */
  ListarClienteCompleteConfigResponse.Customer findCompleteById(Number id);

  /**
   * Delete customer by id.
   *
   * @param id
   *          the id
   * @return the string
   */
  String deleteCustomerById(Number id);

  /**
   * Update customer.
   *
   * @param currentCustomer
   *          the current customer
   * @return the string
   */
  String updateCustomer(EditarClienteRequest currentCustomer);

  /**
   * Edits the customer complete config.
   *
   * @param currentCustomer
   *          the current customer
   * @return the string
   */
  String editCustomerCompleteConfig(EditarClienteCompleteConfigRequest currentCustomer);

}
