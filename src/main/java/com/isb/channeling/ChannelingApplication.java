package com.isb.channeling;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

/**
 * The Class ChannelingApplication.
 */
@SpringBootApplication
public class ChannelingApplication extends SpringBootServletInitializer {

  /** The env. */
  @Autowired
  private Environment env;

  /**
   * The main method.
   *
   * @param args
   *          the arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(ChannelingApplication.class, args);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.springframework.boot.web.support.SpringBootServletInitializer#
   * configure(org.springframework.boot.builder.SpringApplicationBuilder)
   */
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(ChannelingApplication.class);
  }

  /**
   * Context source.
   *
   * @return the ldap context source
   */
  @Bean
  public LdapContextSource contextSource() {
    LdapContextSource contextSource = new LdapContextSource();
    contextSource.setUrl(env.getRequiredProperty("ldap.url"));
    contextSource.setBase(env.getRequiredProperty("ldap.publicKeySearchBase"));
    return contextSource;
  }

  /**
   * Ldap template.
   *
   * @return the ldap template
   */
  @Bean
  public LdapTemplate ldapTemplate() {
    return new LdapTemplate(contextSource());
  }
}
