package com.isb.channeling.domain;

import javax.xml.bind.annotation.XmlElement;

/**
 * The Class UserDefinition.
 */
public abstract class UserDefinition {

  /** The name. */
  private String name;

  /** The user ID. */
  private String userID;

  /** The alias. */
  private String alias;

  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name
   *          the new name
   */
  @XmlElement
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Gets the user ID.
   *
   * @return the user ID
   */
  public String getUserID() {
    return userID;
  }

  /**
   * Sets the user ID.
   *
   * @param userID
   *          the new user ID
   */
  @XmlElement
  public void setUserID(String userID) {
    this.userID = userID;
  }

  /**
   * Gets the alias.
   *
   * @return the alias
   */
  public String getAlias() {
    return alias;
  }

  /**
   * Sets the alias.
   *
   * @param alias
   *          the new alias
   */
  @XmlElement
  public void setAlias(String alias) {
    this.alias = alias;
  }

}
