package com.isb.channeling.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class TokenDefinition.
 */
@XmlRootElement
public class TokenDefinition extends UserDefinition {

}
