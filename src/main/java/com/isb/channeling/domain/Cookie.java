package com.isb.channeling.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class Cookie.
 */
@XmlRootElement
public class Cookie extends UserDefinition {

  /** The definition name. */
  private String definitionName;

  /**
   * Gets the definition name.
   * 
   * @return the definition name
   */
  public String getDefinitionName() {
    return definitionName;
  }

  /**
   * Sets the definition name.
   * 
   * @param definitionName
   *          the new definition name
   */
  @XmlElement
  public void setDefinitionName(String definitionName) {
    this.definitionName = definitionName;
  }

}
