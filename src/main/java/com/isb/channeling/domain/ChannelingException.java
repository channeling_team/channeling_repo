package com.isb.channeling.domain;

/**
 * The Class ChannelingException.
 */
public class ChannelingException extends RuntimeException {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2807630675809587366L;

  // ~ Constructors
  // ===================================================================================================

  /**
   * Instantiates a new channeling exception.
   *
   * @param msg
   *          the msg
   */
  public ChannelingException(String msg) {
    super(msg);
  }

  /**
   * Instantiates a new channeling exception.
   *
   * @param msg
   *          the msg
   * @param t
   *          the t
   */
  public ChannelingException(String msg, Throwable t) {
    super(msg, t);
  }

}
