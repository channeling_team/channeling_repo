package com.isb.channeling.domain;

import java.io.Serializable;

/**
 * The Class JsonResponse.
 *
 * @param <T> the generic type
 */
public class JsonResponse<T> implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5446594287871352193L;

  /** The data. */
  private T data;

  /** The error. */
  private String error;

  /** The message. */
  private String message;

  /**
   * Instantiates a new json response.
   */
  public JsonResponse() {
    super();
  }

  /**
   * Instantiates a new json response.
   *
   * @param object the object
   */
  public JsonResponse(T object) {
    this.data = object;
  }

  /**
   * Gets the data.
   *
   * @return the data
   */
  public T getData() {
    return data;
  }

  /**
   * Sets the data.
   *
   * @param data the new data
   */
  public void setData(T data) {
    this.data = data;
  }

  /**
   * Gets the error.
   *
   * @return the error
   */
  public String getError() {
    return error;
  }

  /**
   * Sets the error.
   *
   * @param error the new error
   */
  public void setError(String error) {
    this.error = error;
  }

  /**
   * Gets the message.
   *
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Sets the message.
   *
   * @param message the new message
   */
  public void setMessage(String message) {
    this.message = message;
  }

}
