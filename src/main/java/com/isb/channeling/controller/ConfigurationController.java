package com.isb.channeling.controller;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.service.ConfigurationService;

import Configuration.AltaConfiguracionRequest;
import Configuration.EditarConfiguracionRequest;
import Configuration.ListarConfiguracionResponse;

/**
 * The Class ConfigurationController.
 */
@RestController
@RequestMapping("/api/configuration")
public class ConfigurationController {

  private static final String MILISEGUNDOS = " milisegundos";

  /** The configuration service. */
  @Resource(name = "configurationService")
  private ConfigurationService configurationService;

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(ConfigurationController.class.getName());

  /**
   * Gets the configuration.
   *
   * @param id the id
   * @return the configuration
   */
  @GetMapping("/{id}")
  public ResponseEntity<JsonResponse<ListarConfiguracionResponse>> getConfiguration(@PathVariable("id") Number id) {
    LOGGER.info("Fetching Configuration with id " + id);
    long time_start, time_end;
    time_start = System.currentTimeMillis();

    final ListarConfiguracionResponse configuration = configurationService.findById(id);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time getConfiguration " + (time_end - time_start) + MILISEGUNDOS);

    if (configuration == null) {
      LOGGER.info("Configuration with id " + id + " not found");
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<>(new JsonResponse<>(configuration), HttpStatus.OK);
  }

  /**
   * Creates the configuration.
   *
   * @param configuration the configuration
   * @return the response entity
   */
  // Create a Configuration
  @PostMapping
  @PreAuthorize("hasRole('ROLE_OPERATOR')")
  public ResponseEntity<JsonResponse<String>> createConfiguration(@RequestBody AltaConfiguracionRequest configuration) {
    LOGGER.info("Creating Configuration: " + configuration.getNameConf());
    long time_start, time_end;
    time_start = System.currentTimeMillis();

    String response = configurationService.createConfiguration(configuration);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time createConfiguration " + (time_end - time_start) + MILISEGUNDOS);

    if (!StringUtils.isNumeric(response)) {
      final JsonResponse<String> jsonResponse = new JsonResponse<>();
      jsonResponse
          .setError(StringUtils.equalsIgnoreCase(response, "DUPLICADO") ? "DUPLICADO_CONFIG" : "KO_CREATE_CONFIG");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    final JsonResponse<String> jsonResponse = new JsonResponse<>("OK_CREATE_CONFIG");
    return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
  }

  /**
   * Update configuration.
   *
   * @param id the id
   * @param configuration the configuration
   * @return the response entity
   */
  @PutMapping("/{id}")
  public ResponseEntity<JsonResponse<String>> updateConfiguration(@PathVariable("id") Number id,
      @RequestBody EditarConfiguracionRequest configuration) {
    LOGGER.info("Updating Configuration " + id);
    long time_start, time_end;
    time_start = System.currentTimeMillis();

    String response = configurationService.updateConfiguration(configuration);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time updateConfiguration " + (time_end - time_start) + MILISEGUNDOS);

    if (!StringUtils.equalsIgnoreCase(response, "OK")) {
      final JsonResponse<String> jsonResponse = new JsonResponse<>();
      jsonResponse
          .setError(StringUtils.equalsIgnoreCase(response, "DUPLICADO") ? "DUPLICADO_CONFIG" : "KO_UPDATE_CONFIG");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    final JsonResponse<String> jsonResponse = new JsonResponse<>("OK_UPDATE_CONFIG");
    return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
  }

  /**
   * Delete configuration.
   *
   * @param id the id
   * @return the response entity
   */
  @DeleteMapping("/{id}")
  public ResponseEntity<JsonResponse<String>> deleteConfiguration(@PathVariable("id") Number id) {
    long time_start, time_end;
    time_start = System.currentTimeMillis();
    LOGGER.info("Fetching & Deleting Configuration with id " + id);

    String response = configurationService.deleteConfigurationById(id);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time deleteConfiguration " + (time_end - time_start) + MILISEGUNDOS);

    if (!StringUtils.equalsIgnoreCase(response, "OK")) {
      final JsonResponse<String> jsonResponse = new JsonResponse<>();
      jsonResponse.setError("KO_DELETE_CONFIG");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    final JsonResponse<String> jsonResponse = new JsonResponse<>("OK_DELETE_CONFIG");
    return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
  }

  /**
   * Sets the configuration service.
   *
   * @param service the new configuration service
   */
  // FOR TESTS
  public void setConfigurationService(ConfigurationService service) {
    this.configurationService = service;
  }

}
