package com.isb.channeling.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.Closure;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.service.ConfigurationService;
import com.isb.channeling.service.CustomerService;
import com.isb.channeling.util.Utils;

import Configuration.AltaListaConfiguracionesRequest;
import Configuration.AltaListaConfiguracionesRequest.Config.PGP;
import Configuration.AltaListaConfiguracionesRequest.Config.Renombrado;
import Configuration.AltaListaConfiguracionesResponse;
import Customer.EditarClienteCompleteConfigRequest;
import Customer.EditarClienteCompleteConfigRequest.Config;
import Customer.EditarClienteCompleteConfigRequest.Config.Renombrado.Value;
import Customer.EditarClienteRequest;
import Customer.ListarClienteCompleteConfigResponse;
import Customer.ListarClienteConfigResponse;
import Customer.ListarClientesResponse.Customer;
import Customer.NuevoClienteRequest;

/**
 * The Class CustomerController.
 */
@RestController
@RequestMapping("/api/customer")
public class CustomerController {

  private static final String CREATE_CUSTOMER = "_CREATE_CUSTOMER";

  private static final String MILISEGUNDOS = " milisegundos";

  private static final String CUSTOMER_WITH_ID = "Customer with id ";

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(CustomerController.class.getName());

  /** The customer service. */
  @Resource(name = "customerService")
  private CustomerService customerService;

  /** The configuration service. */
  @Resource(name = "configurationService")
  private ConfigurationService configurationService;

  /**
   * Retrieve All Customers.
   *
   * @return the json response
   */
  @GetMapping
  public JsonResponse<List<Customer>> listAllCustomers() {
    LOGGER.info("Find all Customers() ");
    long time_start, time_end;
    time_start = System.currentTimeMillis();
    JsonResponse<List<Customer>> response = new JsonResponse<>(customerService.findAllCustomers());
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time listAllCustomers " + (time_end - time_start) + MILISEGUNDOS);
    return response;
  }

  /**
   * Retrieve Single Customer.
   *
   * @param id the id
   * @return the customer
   */
  @GetMapping("/{id}")
  public ResponseEntity<JsonResponse<ListarClienteConfigResponse.Customer>> getCustomer(@PathVariable("id") Number id) {
    LOGGER.info("Fetching Customer with id " + id);
    long time_start, time_end;
    time_start = System.currentTimeMillis();
    final ListarClienteConfigResponse.Customer customer = customerService.findById(id);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time getCustomer " + (time_end - time_start) + MILISEGUNDOS);

    if (customer == null) {
      LOGGER.info(CUSTOMER_WITH_ID + id + " not found");
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    if (StringUtils.isEmpty(customer.getId())) {
      LOGGER.info(CUSTOMER_WITH_ID + id + " doesn't exist");
      final JsonResponse<ListarClienteConfigResponse.Customer> jsonResponse = new JsonResponse<>();
      jsonResponse.setError("CUSTOMER_NOT_EXIST");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    // Remove empty configs
    CollectionUtils.filter(customer.getConfig(), new Predicate<ListarClienteConfigResponse.Customer.Config>() {

      @Override
      public boolean evaluate(ListarClienteConfigResponse.Customer.Config config) {
        return StringUtils.isNotEmpty(config.getIdConfig());
      }
    });

    return new ResponseEntity<>(new JsonResponse<>(customer), HttpStatus.OK);
  }

  /**
   * Retrieve Single Customer All Info.
   *
   * @param id the id
   * @return the customer complete info
   */
  @GetMapping("/{id}/info")
  public ResponseEntity<JsonResponse<ListarClienteCompleteConfigResponse.Customer>> getCustomerCompleteInfo(
      @PathVariable("id") Number id) {
    LOGGER.info("Fetching Customer with id " + id);
    long time_start, time_end;
    time_start = System.currentTimeMillis();
    final ListarClienteCompleteConfigResponse.Customer customer = customerService.findCompleteById(id);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time getCustomerCompleteInfo " + (time_end - time_start) + MILISEGUNDOS);

    if (customer == null) {
      LOGGER.info(CUSTOMER_WITH_ID + id + " not found");
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // Remove empty configs
    CollectionUtils.filter(customer.getConfig(), new Predicate<ListarClienteCompleteConfigResponse.Customer.Config>() {

      @Override
      public boolean evaluate(ListarClienteCompleteConfigResponse.Customer.Config config) {
        return StringUtils.isNotEmpty(config.getIdConfig());
      }
    });

    return new ResponseEntity<>(new JsonResponse<>(customer), HttpStatus.OK);
  }

  /**
   * Create a Customer.
   *
   * @param customer the customer
   * @return the response entity
   */
  @PostMapping
  @PreAuthorize("hasRole('ROLE_OPERATOR')")
  public ResponseEntity<JsonResponse<String>> createCustomer(@RequestBody NuevoClienteRequest customer) {

    LOGGER.info("Creating Customer " + customer.getName());
    long time_start, time_end;
    time_start = System.currentTimeMillis();
    final String response = customerService.createCustomer(customer);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time createCustomer " + (time_end - time_start) + MILISEGUNDOS);

    if (!StringUtils.isNumeric(response)) {
      final JsonResponse<String> jsonResponse = new JsonResponse<>();
      jsonResponse.setError(response + CREATE_CUSTOMER);
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    final JsonResponse<String> jsonResponse = new JsonResponse<>(response);
    jsonResponse.setMessage("OK_CREATE_CUSTOMER");
    return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
  }

  /**
   * Update a Customer.
   *
   * @param id the id
   * @param customer the customer
   * @return the response entity
   */
  @PutMapping("/{id}")
  @PreAuthorize("hasRole('ROLE_OPERATOR')")
  public ResponseEntity<JsonResponse<String>> updateCustomer(@PathVariable("id") Number id,
      @RequestBody EditarClienteRequest customer) {
    LOGGER.info("Updating Customer " + id);
    long time_start, time_end;
    time_start = System.currentTimeMillis();
    final String response = customerService.updateCustomer(customer);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time updateCustomer " + (time_end - time_start) + MILISEGUNDOS);

    if (!StringUtils.equalsIgnoreCase(response, "OK")) {
      final JsonResponse<String> jsonResponse = new JsonResponse<>();
      jsonResponse
          .setError(StringUtils.contains(response, "DUPLICADO") ? response + CREATE_CUSTOMER : "KO_UPDATE_CUSTOMER");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    final JsonResponse<String> jsonResponse = new JsonResponse<>(response);
    jsonResponse.setMessage("OK_UPDATE_CUSTOMER");
    return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
  }

  /**
   * Delete a Customer.
   *
   * @param id the id
   * @return the response entity
   */
  @DeleteMapping("/{id}")
  @PreAuthorize("hasRole('ROLE_OPERATOR')")
  public ResponseEntity<JsonResponse<String>> deleteCustomer(@PathVariable("id") Number id) {
    LOGGER.info("Deleting Customer with id " + id);
    long time_start, time_end;
    time_start = System.currentTimeMillis();
    final String response = customerService.deleteCustomerById(id);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time deleteCustomer " + (time_end - time_start) + MILISEGUNDOS);

    if (!StringUtils.equalsIgnoreCase(response, "OK")) {
      final JsonResponse<String> jsonResponse = new JsonResponse<>();
      jsonResponse.setError("KO_DELETE_CUSTOMER");
      jsonResponse.setMessage(response);
      return new ResponseEntity<>(jsonResponse, HttpStatus.NO_CONTENT);
    }

    final JsonResponse<String> jsonResponse = new JsonResponse<>(response);
    jsonResponse.setMessage("OK_DELETE_CUSTOMER");
    return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
  }

  /**
   * Duplicate a Customer.
   *
   * @param customer the customer
   * @return the response entity
   */
  @PostMapping("/duplicate")
  @PreAuthorize("hasRole('ROLE_OPERATOR')")
  public ResponseEntity<JsonResponse<String>> duplicateCustomer(
      @RequestBody EditarClienteCompleteConfigRequest customer) {
    LOGGER.info("Creating Customer " + customer.getName());
    final JsonResponse<String> jsonResponse = new JsonResponse<>();

    final String response = customerService.createCustomer(mapCliente(customer));
    if (!StringUtils.isNumeric(response)) {
      jsonResponse.setError(response + CREATE_CUSTOMER);
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    final List<Config> listconf = customer.getConfig();
    if (CollectionUtils.isEmpty(listconf)) {
      jsonResponse.setData(response);
      jsonResponse.setMessage("OK_DUPLICATE_CUSTOMER");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    final AltaListaConfiguracionesRequest altaConf = new AltaListaConfiguracionesRequest();
    altaConf.setId(response);
    IteratorUtils.forEach(listconf.iterator(), new Closure<Config>() {

      @Override
      public void execute(Config origin) {
        altaConf.getConfig().add(mapConfigurations(origin));
      }
    });

    final AltaListaConfiguracionesResponse responseConf = configurationService.createConfigurationList(altaConf);
    if (!StringUtils.equalsIgnoreCase(responseConf.getRsp(), "OK")) {
      jsonResponse.setError("KO_DUPLICATE_ALL_CONFIG");
    } else if (responseConf.getConfigDup() != null && StringUtils.isNotEmpty(responseConf.getConfigDup().getDup())) {
      jsonResponse.setError("KO_DUPLICATE_CONFIG");
    } else {
      jsonResponse.setData(responseConf.getRsp());
      jsonResponse.setMessage("OK_DUPLICATE_CUSTOMER");
    }
    return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
  }

  /**
   * Map Cliente.
   *
   * @param customer the customer
   * @return the nuevo cliente request
   */
  private NuevoClienteRequest mapCliente(EditarClienteCompleteConfigRequest customer) {
    // Nos creamos un usuario nuevo del tipo necesario NuevoClienteRequest y
    // asignamos los campos
    NuevoClienteRequest nuevocliente = new NuevoClienteRequest();
    nuevocliente.setAlias(customer.getAlias());
    nuevocliente.setAudUser(customer.getAuduser());
    nuevocliente.setName(customer.getName());
    nuevocliente.setDescripcion(customer.getDescripcion());
    return nuevocliente;
  }

  /**
   * Map configurations.
   *
   * @param config the config
   * @return the alta lista configuraciones request. config
   */
  private AltaListaConfiguracionesRequest.Config mapConfigurations(Config config) {
    // Mapeamos la vista a la entrada del WS
    final AltaListaConfiguracionesRequest.Config duplicateConf = new AltaListaConfiguracionesRequest.Config();
    duplicateConf.setNameConf(StringUtils.stripAccents(StringUtils.upperCase(config.getNameConf())));
    duplicateConf.setSubalias(StringUtils.stripAccents(StringUtils.upperCase(config.getSubalias())));
    duplicateConf.setDescripcion(StringUtils.stripAccents(config.getDescripcion()));
    duplicateConf.setDestino(config.getDestino());
    duplicateConf.setFormato(config.getFormato());
    duplicateConf.setOrigen(config.getOrigen());
    duplicateConf.setPGPX(config.getPGPX());
    duplicateConf.setS3Key(config.getS3Key());
    duplicateConf.setTipoDestino(config.getTipoDestino());
    duplicateConf.setAudUser(Utils.getLoggedInUser());

    if (config.getPGP() != null) {
      PGP dPgp = new PGP();
      dPgp.setAscii(config.getPGP().getAscii());
      dPgp.setCompress(config.getPGP().getCompress());
      dPgp.setKey(config.getPGP().getKey());
      dPgp.setTextMode(config.getPGP().getTextMode());
      dPgp.setTipoCifrado(config.getPGP().getTipoCifrado());
      dPgp.setTipoFirma(config.getPGP().getTipoFirma());
      duplicateConf.setPGP(dPgp);
    }

    if (config.getRenombrado() != null) {
      final Renombrado dRenom = new Renombrado();
      dRenom.setTipo(config.getRenombrado().getTipo());
      IteratorUtils.forEach(config.getRenombrado().getValue().iterator(), new Closure<Value>() {

        @Override
        public void execute(Value val) {
          AltaListaConfiguracionesRequest.Config.Renombrado.Value value;
          value = new AltaListaConfiguracionesRequest.Config.Renombrado.Value();
          value.setOrden(val.getOrden());
          value.setParametro(val.getParametro());
          value.setSeparador(val.getSeparador());
          value.setValor(val.getValor());
          dRenom.getValue().add(value);
        }
      });
      duplicateConf.setRenombrado(dRenom);
    }

    return duplicateConf;
  }

  /**
   * only for test.
   *
   * @param service the new customer service
   */
  public void setCustomerService(CustomerService service) {
    this.customerService = service;
  }

  /**
   * only for test.
   *
   * @param service the new configuration service
   */
  public void setConfigurationService(ConfigurationService service) {
    this.configurationService = service;
  }

}
