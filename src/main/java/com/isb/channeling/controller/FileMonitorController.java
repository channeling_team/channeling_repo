package com.isb.channeling.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.service.FileMonitorService;
import com.isb.channeling.util.DateUtil;

import FileMonitor.BuscarFicheroRequest;
import FileMonitor.BuscarFicheroResponse.Fichero;

/**
 * The Class FileMonitorController.
 */
@RestController
@RequestMapping("/api/filemonitor")
public class FileMonitorController {

  private static final String MILISEGUNDOS = " milisegundos";

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(FileMonitorController.class.getName());

  /** The file monitor service. */
  @Resource(name = "fileMonitorService")
  private FileMonitorService fileMonitorService;

  /**
   * Method to search files in history.
   *
   * @param file the file
   * @return the json response
   */
  @PostMapping
  public JsonResponse<List<Fichero>> search(@RequestBody BuscarFicheroRequest file) {
    LOGGER.info("searching files" + file);
    long time_start, time_end;
    time_start = System.currentTimeMillis();

    file.setFechaIni(DateUtil.convertGMTtoCET(file.getFechaIni()));
    file.setFechaFin(DateUtil.convertGMTtoCET(file.getFechaFin()));
    file.setCliente(StringUtils.upperCase(file.getCliente()));
    file.setNombreOriginal(StringUtils.upperCase(file.getNombreOriginal()));
    file.setRenombrado(StringUtils.upperCase(file.getRenombrado()));

    JsonResponse<List<Fichero>> response = new JsonResponse<>(fileMonitorService.search(file));
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time search " + (time_end - time_start) + MILISEGUNDOS);
    return response;
  }

  /**
   * Sets the file monitor service.
   *
   * @param service the new file monitor service
   */
  // FOR TESTS
  public void setFileMonitorService(FileMonitorService service) {
    this.fileMonitorService = service;
  }

}
