package com.isb.channeling.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.service.UserService;
import com.isb.channeling.util.Constants;

import User.EditarUsuariosRequest;
import User.ListarUsuariosResponse.User;
import User.NuevoUsuarioRequest;

/**
 * The Class UserController.
 */
@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class UserController {

  private static final String MILISEGUNDOS = " milisegundos";

  /** The Constant LOGGER. */
  private static final Logger LOGGER = Logger.getLogger(UserController.class.getName());

  /** The user service. */
  @Resource(name = "userService")
  private UserService userService;

  /**
   * List all users.
   *
   * @return the json response
   */
  // Retrieve All Users
  @GetMapping
  public JsonResponse<List<User>> listAllUsers() {
    long time_start, time_end;
    time_start = System.currentTimeMillis();
    JsonResponse<List<User>> response = new JsonResponse<>(userService.findAllUsers());
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time listAllUsers " + (time_end - time_start) + MILISEGUNDOS);
    return response;
  }

  /**
   * Gets the user.
   *
   * @param name the name
   * @return the user
   */
  // Retrieve Single User
  @GetMapping("/{name}")
  public ResponseEntity<JsonResponse<User>> getUser(@PathVariable("name") String name) {
    LOGGER.debug("Fetching User with name " + name);
    long time_start, time_end;
    time_start = System.currentTimeMillis();

    final User user = userService.findByName(name);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time getUser " + (time_end - time_start) + MILISEGUNDOS);

    if (user == null) {
      LOGGER.info("User with name " + name + " not found");
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    if (StringUtils.isEmpty(user.getId())) {
      LOGGER.info("User with name " + name + " not found");
      final JsonResponse<User> jsonResponse = new JsonResponse<>();
      jsonResponse.setError("USER_NOT_EXIST");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }
    return new ResponseEntity<>(new JsonResponse<>(user), HttpStatus.OK);
  }

  /**
   * Creates the user.
   *
   * @param user the user
   * @return the response entity
   */
  // Create a User
  @PostMapping
  public ResponseEntity<JsonResponse<String>> createUser(@RequestBody NuevoUsuarioRequest user) {
    LOGGER.info("Creating User " + user.getName());
    long time_start, time_end;
    time_start = System.currentTimeMillis();

    final String response = userService.createUser(user);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time createUser " + (time_end - time_start) + MILISEGUNDOS);

    LOGGER.info("createUser_response: " + response);
    if (!StringUtils.isNumeric(response)) {
      final JsonResponse<String> jsonResponse = new JsonResponse<>();
      jsonResponse.setError(StringUtils.equalsIgnoreCase(response, "DUPLICADO") ? "DUPLICADO_USER" : "KO_CREATE_USER");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }
    return new ResponseEntity<>(new JsonResponse<>("OK_CREATE_USER"), HttpStatus.OK);
  }

  /**
   * Update user.
   *
   * @param id the id
   * @param user the user
   * @return the response entity
   */
  // Update a User
  @PutMapping("/{id}")
  public ResponseEntity<JsonResponse<String>> updateUser(@PathVariable("id") Number id,
      @RequestBody EditarUsuariosRequest user) {
    LOGGER.info("Updating User " + id);
    long time_start, time_end;
    time_start = System.currentTimeMillis();

    final String response = userService.updateUser(user);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time updateUser " + (time_end - time_start) + MILISEGUNDOS);

    LOGGER.info("updateUser_response: " + response);
    if (!StringUtils.equalsIgnoreCase(Constants.OK, response)) {
      final JsonResponse<String> jsonResponse = new JsonResponse<>();
      jsonResponse.setError(StringUtils.equalsIgnoreCase(response, "DUPLICADO") ? "DUPLICADO_USER" : "KO_UPDATE_USER");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }
    return new ResponseEntity<>(new JsonResponse<>("OK_UPDATE_USER"), HttpStatus.OK);
  }

  /**
   * Delete user.
   *
   * @param id the id
   * @return the response entity
   */
  // Delete a User
  @DeleteMapping("/{id}")
  public ResponseEntity<JsonResponse<String>> deleteUser(@PathVariable("id") Number id) {
    LOGGER.info("Fetching & Deleting User with id " + id);
    long time_start, time_end;
    time_start = System.currentTimeMillis();

    final String response = userService.deleteUserById(id);
    time_end = System.currentTimeMillis();
    LOGGER.debug("----> Time deleteUser " + (time_end - time_start) + MILISEGUNDOS);

    LOGGER.info("deleteUser_Response: " + response);
    if (!StringUtils.equalsIgnoreCase(response, Constants.OK)) {
      final JsonResponse<String> jsonResponse = new JsonResponse<>();
      jsonResponse.setError("KO_DELETE_USER");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    return new ResponseEntity<>(new JsonResponse<>("OK_DELETE_USER"), HttpStatus.OK);
  }

  /**
   * Sets the user service.
   *
   * @param service the new user service
   */
  // FOR TESTS
  public void setUserService(UserService service) {
    this.userService = service;
  }

}
