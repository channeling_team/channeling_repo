module.exports = {
  extends: [
    'angular'
  ],
  rules: {
    "linebreak-style": 0,
    'angular/no-service-method': 0,
    'prefer-arrow-callback': 0,
    'angular/log': 0,
    'no-unneeded-ternary': 0,
    'max-params': 0,
    'no-return-assign': 0,
    'no-negated-condition': 0
  }
}
