export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(false);
  $urlRouterProvider.otherwise('/customers');

  $stateProvider
    .state('customers', {
      url: '/customers',
      component: 'customer',
      name: 'customers',
      data: {
        permissions: {
          except: ['ADMIN'],
          redirectTo: 'users'
        }
      }
    })
    .state('filesMonitor', {
      url: '/file-monitor',
      component: 'fileMonitor',
      data: {
        permissions: {
          except: ['ADMIN'],
          redirectTo: 'users'
        }
      }
    })
    .state('users', {
      url: '/users',
      component: 'users',
      data: {
        permissions: {
          only: ['ADMIN'],
          redirectTo: 'customers'
        }
      }
    });

  // Prevent router from automatic state resolving
  $urlRouterProvider.deferIntercept();
}
