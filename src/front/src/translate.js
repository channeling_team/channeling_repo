export default translateConfig;

/** @ngInject */
function translateConfig($translateProvider) {
  $translateProvider.fallbackLanguage('en');
  $translateProvider.registerAvailableLanguageKeys(['en', 'es'], {
    'en_*': 'en',
    'es_*': 'es'
  });

  $translateProvider.useStaticFilesLoader({
    prefix: 'channeling/locale-',
    suffix: '.json'
  });
  $translateProvider.useSanitizeValueStrategy('escape');
  $translateProvider.preferredLanguage('en');
}
