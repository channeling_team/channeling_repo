import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import uiBootstrap from 'angular-ui-bootstrap';
import ngPagination from 'angular-utils-pagination';
import ngAnimate from 'angular-animate';
import ngAria from 'angular-aria';
import ngChkModel from 'checklist-model';
import ngTranslate from 'angular-translate';
import ngTranslateLoader from 'angular-translate-loader-static-files';
import ngIdle from 'ng-idle';
import ngMaterialDatePicker from 'ng-material-datetimepicker';
import ngMaterial from 'angular-material';
import ngMessages from 'angular-messages';
import {permission, uiPermission} from 'angular-permission';

import routesConfig from './routes';
import authConfig from './app/auth/auth';
import translateConfig from './translate';
import heartbeatConfig from './app/auth/heartbeat';
import idle from './app/auth/idle';
import accessInterceptor from './app/auth/access-interceptor';
import common from './app/common/common';
import components from './app/components/components';
import {AuthService} from './app/auth/auth.service';
import {App} from './app/app';
import 'bootstrap-css-only';
import 'angular-material/angular-material.css';
import 'ng-material-datetimepicker/css/material-datetimepicker.css';
import 'font-awesome/css/font-awesome.css';
import './style/app.css';

angular
  .module('app', [uiRouter, permission, uiPermission, ngTranslate, ngTranslateLoader, ngPagination,
    ngAnimate, ngAria, ngMaterial, uiBootstrap, ngChkModel, ngMessages, ngMaterialDatePicker, common, components, ngIdle])
  .component('app', App)
  .service('authService', AuthService)
  .factory('accessInterceptor', accessInterceptor)
  .config(routesConfig)
  .config(translateConfig)
  .config(heartbeatConfig)
  .run(authConfig)
  .run(idle);
