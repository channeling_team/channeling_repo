class AppController {
  /** @ngInject */
  constructor($translate) {
    this.$translate = $translate;
  }

  changeLanguage(key) {
    this.$translate.use(key);
  }

}

export const App = {
  template: require('./app.html'),
  controller: AppController
};
