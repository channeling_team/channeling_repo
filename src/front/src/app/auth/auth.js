export default authConfig;

/** @ngInject */
function authConfig(PermPermissionStore, PermRoleStore, authService, $urlRouter) {
  authService.getPermissions().then(response => {
    const checkRole = /** @ngInject */ function (roleName) {
      roleName = 'ROLE_' + roleName;
      return response.indexOf(roleName) !== -1;
    };
    PermRoleStore.defineManyRoles({
      VIEWER: checkRole,
      OPERATOR: checkRole,
      ADMIN: checkRole
    });
  }).then(() => {
    // Once permissions are set-up
    // kick-off router and start the application rendering
    $urlRouter.sync();
    // Also enable router to listen to url changes
    $urlRouter.listen();
  });
}
