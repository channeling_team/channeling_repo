import {ROLE_URL} from '../common/constants.js';

export class AuthService {
  /** @ngInject */
  constructor($http) {
    this.$http = $http;
  }

  getPermissions() {
    return this.$http.get(ROLE_URL).then(response => response.data.data);
  }
}
