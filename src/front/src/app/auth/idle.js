export default idle;

/** @ngInject */
function idle(Idle) {
  // start watching when the app runs. also starts the Keepalive service by default.
  Idle.watch();
}
