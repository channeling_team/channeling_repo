export default accessInterceptor;

/** @ngInject */
function accessInterceptor($q, $window) {
  return {
    responseError(response) {
      if (response.status === 403) {
        $window.location.href = $window.location.origin + '/channeling/logout';
      }
      return $q.reject(response);
    }
  };
}
