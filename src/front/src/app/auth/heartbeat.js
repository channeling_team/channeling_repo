export default heartbeatConfig;

/** @ngInject */
function heartbeatConfig(IdleProvider, KeepaliveProvider, $httpProvider) {// configure Idle settings
  // IdleProvider.idle(5); // in seconds
  // IdleProvider.timeout(5); // in seconds
  KeepaliveProvider.interval(10 * 60); // in seconds
  KeepaliveProvider.http('channeling/rfraccs'); // URL that makes sure session is alive
  $httpProvider.interceptors.push('accessInterceptor');
}
