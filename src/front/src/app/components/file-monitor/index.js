import angular from 'angular';
import {FileMonitorComponent} from './file-monitor.component';
import {FileMonitorService} from './file-monitor.service';

const filesMonitor = angular
    .module('fileMonitor', [])
    .component('fileMonitor', FileMonitorComponent)
    .service('fileMonitorService', FileMonitorService)
    .name;

export default filesMonitor;
