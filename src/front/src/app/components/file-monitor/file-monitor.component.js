import {TYPESTATE, NOTIFICATION} from '../../common/constants.js';

class FileMonitorController {
  /** @ngInject */
  constructor(fileMonitorService, mdcDateTimeDialog) {
    this.fileMonitorService = fileMonitorService;
    this.mdcDateTimeDialog = mdcDateTimeDialog;
    this.opt = {
      type: 'select',
      name: 'Service',
      value: '10',
      values: ['10', '20', '40']
    };
    this.typestate = TYPESTATE;
  }

  displayInitDate() {
    this.mdcDateTimeDialog.show({
      currentDate: this.myDate,
      maxDate: this.maxDate,
      minDate: this.minDate,
      showTodaysDate: '',
      time: true
    }).then(date => {
      this.myDate = date;
      this.calcDatesEnd();
    }, () => {});
  }

  displayEndDate() {
    this.mdcDateTimeDialog.show({
      currentDate: this.myDate,
      maxDate: this.maxDateEnd,
      minDate: this.minDateEnd,
      showTodaysDate: '',
      time: true
    }).then(date => {
      this.myDateEnd = date;
    }, () => {});
  }

  calcDates() {
    this.maxDate = new Date();
    this.minDate = new Date(
      this.maxDate.getFullYear(),
      this.maxDate.getMonth() - 6,
      this.maxDate.getDate()
    );
  }

  calcDatesEnd() {
    this.myDateEnd = '';
    this.maxDateEnd = new Date(
      this.myDate.getFullYear(),
      this.myDate.getMonth(),
      this.myDate.getDate() + 31
    );
    this.minDateEnd = new Date(
      this.myDate.getFullYear(),
      this.myDate.getMonth(),
      this.myDate.getDate());
  }

  calcRange() {
    this.dateRangeError = false;
    if (this.myDate < this.minDate) {
      this.dateRangeError = true;
    }
    if (this.myDateEnd > this.maxDateEnd) {
      this.dateRangeError = true;
    }
  }

  updateFormatStatus() {
    this.selectedStatus = this.selectedType.state[0];
    this.selectedFormat = this.selectedType.format[0];
  }

  $onInit() {
    this.waiting = false;
    this.files = [];
    this.previewMode = false;
    this.reverse = false;
    this.searched = false;
    this.selectedType = this.typestate[0];
    this.selectedStatus = this.typestate[0].state[0];
    this.selectedFormat = this.typestate[0].format[0];
    this.myDate = new Date();
    this.myDate = new Date(this.myDate.getFullYear(),
      this.myDate.getMonth(),
      this.myDate.getDate());
    this.myDateEnd = '';
    this.dateRangeError = false;
    this.calcDates();
    this.calcDatesEnd();
    this.file = {
      cliente: '',
      fechaIni: this.myDate,
      fechaFin: '',
      tipo: '',
      nombreOriginal: '',
      renombrado: '',
      id: '',
      formato: '',
      estado: '',
      fecha: ''
    };
  }

  showMe() {
    this.previewMode = !this.previewMode;
  }

  sort(keyname) {
    this.sortKey = keyname;
    if (keyname === 'estado') {
      this.sortKey += '| translate';
    }
    if (keyname === 'tipo') {
      this.sortKey += '| translate';
    }
    this.reverse = !this.reverse;
  }

  hideAlert() {
    this.notification.show = false;
  }

  handleError(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'red';
    notice.message = message;
    this.notification = notice;
  }

  search() {
    this.files = [];
    this.calcRange();
    if (this.dateRangeError === false) {
      this.waiting = true;
      this.file.tipo = this.selectedType.value;
      this.file.estado = this.selectedStatus ? this.selectedStatus.id : '';
      this.file.formato = this.selectedFormat ? this.selectedFormat.name : '';
      this.file.fechaIni = this.myDate;
      this.file.fechaFin = this.myDateEnd;
      this.fileMonitorService.getFiles(this.file).then(response => {
        this.files = response;
        this.searched = true;
        this.waiting = false;
      }).catch(() => {
        this.handleError('KO_FILE_SEARCH');
        this.waiting = false;
        // this.message = error.message;
      });
    }
  }
}

export const FileMonitorComponent = {
  template: require('./file-monitor.html'),
  controller: FileMonitorController
};
