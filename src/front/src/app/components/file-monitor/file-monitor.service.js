import {FILES_URL} from '../../common/constants.js';

export class FileMonitorService {
  /** @ngInject */
  constructor($http) {
    this.$http = $http;
  }

  getFiles(file) {
    return this.$http.post(
      FILES_URL, angular.toJson(file)
    ).then(response => {
      const resp = response ? response.data.data : [];
      angular.forEach(resp, file => {
        file.id = parseFloat(file.id);
      });
      return resp;
    });
  }
}
