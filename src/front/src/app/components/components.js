import angular from 'angular';

import users from './users';
import customers from './customers';
import filesMonitor from './file-monitor';

const components = angular
  .module('app.components', [users,
    customers, filesMonitor])
  .name;

export default components;
