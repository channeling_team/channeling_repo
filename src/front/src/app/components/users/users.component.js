import {NOTIFICATION} from '../../common/constants.js';
class UserController {
  /** @ngInject */
  constructor($uibModal, userService, $filter) {
    this.$uibModal = $uibModal;
    this.userService = userService;
    this.selectedUsers = [];
    this.$filter = $filter;
  }

  $onInit() {
    this.getUsers();
  }

  getUsers() {
    this.waiting = true;
    this.userService.getUsers().then(response => {
      this.users = response;
      this.waiting = false;
    }).catch(() => {
      this.waiting = false;
      this.handleError('Error retrieving users');
    });
  }

  deleteUser(id) {
    this.userService.deleteUserById(id).then(response => {
      if (response.data) {
        this.handleSuccess(response.data);
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_DELETE_USER'));
  }

  handleSuccess(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'green';
    notice.message = message;
    this.notification = notice;
    this.selectedUsers = [];
    this.getUsers();
  }

  handleError(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'red';
    notice.message = message;
    this.notification = notice;
    this.selectedUsers = [];
  }

  openUserModal(name) {
    this.$uibModal.open({
      component: 'userDetailCo',
      ariaLabelledBy: 'modal-title-top',
      ariaDescribedBy: 'modal-body-top',
      windowClass: 'modal right fade',
      backdrop: 'static',
      resolve: {
        title: () => name ? 'EDIT_USER' : 'NEW_USER',
        userName: () => name,
        userService: 'userService'
      }
    }).result.then(result => {
      this.notification = result;
      this.selectedUsers = [];
      this.getUsers();
    },
    cancel => console.debug('modal closed: ' + cancel));
  }

  openDeleteModal(selected) {
    this.$uibModal.open({
      component: 'warningCo',
      ariaLabelledBy: 'modal-title-top',
      ariaDescribedBy: 'modal-body-top',
      windowClass: 'modal right fade',
      backdrop: 'static',
      resolve: {
        info: () => {
          return {
            title: this.$filter('translate')('DELETE_USER') + ' ' + selected.name,
            type: 'window__tit_delete',
            message: 'MESSAGE_SURE'
          };
        }
      }
    }).result.then(
      () => this.deleteUser(selected.id),
      cancel => console.debug('modal closed: ' + cancel)
    );
  }

}

export const UserComponent = {
  template: require('./users.html'),
  controller: UserController
};
