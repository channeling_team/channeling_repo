import {NOTIFICATION, ROLE} from '../../../common/constants.js';
class UserDetailController {
  constructor() {
    this.role = ROLE;
  }

  getUser(name) {
    this.waiting = true;
    this.userService.getUserByName(name).then(response => {
      if (response.data) {
        this.selectedUser = response.data;
      } else {
        this.handleError(response.error);
      }
      this.waiting = false;
    }).catch(() => this.handleError('USER_NOT_EXIST'));
  }

  createUser(user) {
    this.userService.createUser(user).then(response => {
      if (response.data) {
        this.handleSuccess(response.data);
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_CREATE_USER'));
  }

  saveUser(user) {
    this.userService.saveUser(user).then(response => {
      if (response.data) {
        this.handleSuccess(response.data);
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_UPDATE_USER'));
  }

  delete() {
    this.userService.deleteUserById(this.selectedUser.id).then(response => {
      if (response.data) {
        this.handleSuccess(response.data);
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_DELETE_USER'));
  }

  handleSuccess(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'green';
    notice.message = message;
    this.close({$value: notice});
  }

  handleError(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'red';
    notice.message = message;
    this.close({$value: notice});
  }

  ok() {
    if (this.selectedUser.id) {
      this.saveUser(this.selectedUser);
    } else {
      this.createUser(this.selectedUser);
    }
  }

  cancel() {
    this.dismiss({$value: 'cancel'});
  }

  $onInit() {
    this.userService = this.resolve.userService;
    this.title = this.resolve.title;
    this.warning = true;
    if (this.resolve.userName) {
      this.getUser(this.resolve.userName);
    }
  }
}

export const UserDetailComponent = {
  template: require('./user-detail.html'),
  controller: UserDetailController,
  bindings: {
    close: '&',
    dismiss: '&',
    resolve: '<',
    greeting: '<'
  }
};
