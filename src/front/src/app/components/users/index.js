import angular from 'angular';
import {UserComponent} from './users.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {UserService} from './user.service';

const users = angular
    .module('users', [])
    .component('users', UserComponent)
    .component('userListCo', UserListComponent)
    .component('userDetailCo', UserDetailComponent)
    .service('userService', UserService)
    .name;

export default users;
