import {USERS_URL} from '../../common/constants.js';

export class UserService {
  /** @ngInject */
  constructor($http) {
    this.$http = $http;
  }
  // getUsers() {
  //   return this.$http.get(USERS_URL)
  //     .then(response => response ? response.data.data : []);
  // }

  getUsers() {
    return this.$http.get(USERS_URL)
      .then(response => {
        const resp = response ? response.data.data : [];
        angular.forEach(resp, user => {
          user.id = parseFloat(user.id);
        });
        return resp;
      });
  }

  getUserByName(name) {
    const url = `${USERS_URL}/${name}`;
    return this.$http.get(url)
      .then(response => response ? response.data : {});
  }

  saveUser(user) {
    const url = `${USERS_URL}/${user.id}`;
    return this.$http.put(
      url, angular.toJson(user)
    ).then(response => response.data);
  }

  createUser(user) {
    return this.$http.post(
      USERS_URL, angular.toJson(user)
    ).then(response => response.data);
  }

  deleteUserById(id) {
    const url = `${USERS_URL}/${id}`;
    return this.$http.delete(url)
      .then(response => response.data);
  }
}
