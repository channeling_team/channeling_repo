import {RECORDS} from '../../../common/constants.js';

class UserListController {
  constructor() {
    this.records = RECORDS;
  }

  $onInit() {
    this.reverse = false;
    this.selectedRow = null;
  }

  sort(keyname) {
    this.sortKey = keyname;
    this.reverse = !this.reverse;
  }

  editUser(name) {
    this.chks = [];
    this.selectedRow = null;
    this.edit({name});
  }

  addOrRemove(item) {
    const isPresent = (this.chks.indexOf(item) > -1);
    if (!isPresent) {
      this.selectedRow = item.id;
      this.chks.splice(0, 1);
    } else {
      this.selectedRow = null;
      this.chks.splice(0, 1, item);
    }
  }
}

export const UserListComponent = {
  template: require('./user-list.html'),
  controller: UserListController,
  bindings: {
    users: '<',
    edit: '&',
    chks: '='
  }
};
