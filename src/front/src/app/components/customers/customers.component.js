import {NOTIFICATION} from '../../common/constants.js';
class CustomerController {
  /** @ngInject */
  constructor($uibModal, customerService, $state, authService, $filter) {
    this.$uibModal = $uibModal;
    this.$state = $state;
    this.customerService = customerService;
    this.selectedCustomers = [];
    this.authService = authService;
    this.$filter = $filter;
  }

  $onInit() {
    this.getCustomers();
  }

  getCustomers() {
    this.waiting = true;
    this.customerService.getCustomers().then(response => {
      this.customers = response.data;
      this.waiting = false;
    }).catch(() => {
      this.handleError('Error retrieving customers');
      this.waiting = false;
    });
  }

  deleteUser(id) {
    this.userService.deleteUserById(id).then(() => {
      this.handleSuccess('User with id: ' + id + ' deleted successfully');
    }).catch(() => this.handleError('Error deleting user'));
  }

  deleteCustomer(id) {
    this.customerService.deleteCustomerById(id).then(response => {
      if (response.data) {
        this.handleSuccess(response.message);
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_DELETE_CUSTOMER'));
  }

  handleSuccess(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'green';
    notice.message = message;
    this.notification = notice;
    this.selectedCustomers = [];
    this.getCustomers();
  }

  handleError(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'red';
    notice.message = message;
    this.notification = notice;
    this.selectedCustomers = [];
  }

  openDeleteModal(selected) {
    console.info(selected);
    this.$uibModal.open({
      component: 'warningCo',
      ariaLabelledBy: 'modal-title-top',
      ariaDescribedBy: 'modal-body-top',
      windowClass: 'modal right fade',
      backdrop: 'static',
      resolve: {
        info: () => {
          return {
            title: this.$filter('translate')('DELETE') + ' ' + selected.name,
            type: 'window__tit_delete',
            message: 'MESSAGE_SURE'
          };
        }
      }
    }).result.then(
      () => this.deleteCustomer(selected.id),
      cancel => console.debug('modal closed: ' + cancel));
  }

  openCustomerModal(id) {
    this.$state.go('detail');
    this.$uibModal.open({
      component: 'customerDetailCo',
      ariaLabelledBy: 'modal-title-top',
      ariaDescribedBy: 'modal-body-top',
      windowClass: 'modal right fade',
      backdrop: 'static',
      resolve: {
        title: () => id ? 'EDIT' : 'NEW',
        customerId: () => id
      }
    }).result.then(result => {
      this.notification = result;
      this.selectedCustomers = [];
      this.getCustomers();
    }, created => {
      if (created) {
        this.getCustomers();
      }
    }
    ).finally(() => this.$state.go('customers'));
  }

  openDuplicateModal(id) {
    this.$state.go('detail');
    this.$uibModal.open({
      component: 'customerDuplicateCo',
      ariaLabelledBy: 'modal-title-top',
      ariaDescribedBy: 'modal-body-top',
      windowClass: 'modal right fade',
      backdrop: 'static',
      resolve: {
        title: () => 'NEW',
        customerId: () => id
      }
    }).result.then(result => {
      this.notification = result;
      this.selectedCustomers = [];
      this.getCustomers();
    }, cancel => console.debug('modal closed: ' + cancel)
    ).finally(() => this.$state.go('customers'));
  }
}

export const CustomerComponent = {
  template: require('./customers.html'),
  controller: CustomerController
};
