export default customerRoutesConfig;
/** @ngInject */
function customerRoutesConfig($stateProvider) {
  $stateProvider
  .state('detail', {
    url: '/detail',
    parent: 'customers',
    views: {
      'customers@': {
        template: require('./customer-detail/partial-customer.html')
      }
    }
  })
  .state('config0', {
    url: '/config0',
    parent: 'customers',
    views: {
      'customers@': {
        template: require('./customer-detail/partial-configuration0.html')
      }
    }
  })
  .state('config1', {
    url: '/config1',
    parent: 'customers',
    views: {
      'customers@': {
        template: require('./customer-detail/partial-configuration1.html')
      }
    }
  })
  .state('config2', {
    url: '/config2',
    parent: 'customers',
    views: {
      'customers@': {
        template: require('./customer-detail/partial-configuration2.html')
      }
    }
  });
}
