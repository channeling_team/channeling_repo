import angular from 'angular';
import {CustomerComponent} from './customers.component';
import {CustomerService} from './customer.service';
import {CustomerListComponent} from './customer-list/customer-list.component';
import {CustomerDetailComponent} from './customer-detail/customer-detail.component';
import {CustomerDuplicateComponent} from './customer-duplicate/customer-duplicate.component';
import {ConfigurationListComponent} from './configuration-list/configuration-list.component';
import {ConfigurationService} from './configuration-list/configuration.service';
import customerRoutesConfig from './customer.routes.js';

const customers = angular
    .module('customer', [])
    .component('customer', CustomerComponent)
    .component('customerListCo', CustomerListComponent)
    .component('customerDetailCo', CustomerDetailComponent)
    .component('customerDuplicateCo', CustomerDuplicateComponent)
    .component('configurationListCo', ConfigurationListComponent)
    .service('customerService', CustomerService)
    .service('configurationService', ConfigurationService)
    .config(customerRoutesConfig)
    .name;

export default customers;
