import {RECORDS} from '../../../common/constants.js';

class CustomerListController {
  constructor() {
    this.records = RECORDS;
  }

  $onInit() {
    this.reverse = false;
    this.selectedRow = null;
  }

  sort(keyname) {
    this.sortKey = keyname;
    this.reverse = !this.reverse;
  }

  editCustomer(id) {
    this.chks = [];
    this.selectedRow = null;
    this.edit({id});
  }

  addOrRemove(item) {
    const isPresent = (this.chks.indexOf(item) > -1);
    if (!isPresent) {
      this.selectedRow = item.id;
      this.chks.splice(0, 1);
    } else {
      this.selectedRow = null;
      this.chks.splice(0, 1, item);
    }
  }
}

export const CustomerListComponent = {
  template: require('./customer-list.html'),
  controller: CustomerListController,
  bindings: {
    customers: '<',
    edit: '&',
    delete: '&',
    chks: '='
  }
};
