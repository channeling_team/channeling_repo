import {CUSTOMERS_URL} from '../../common/constants.js';

export class CustomerService {
  /** @ngInject */
  constructor($http) {
    this.$http = $http;
  }

  getCustomers() {
    return this.$http.get(CUSTOMERS_URL)
      .then(response => {
        const resp = response ? response.data : [];
        angular.forEach(resp.data, customer => {
          customer.id = parseFloat(customer.id);
          delete customer.audate;
          delete customer.auduser;
        });
        return resp;
      });
  }

  getCustomerById(id) {
    const url = `${CUSTOMERS_URL}/${id}`;
    return this.$http.get(url)
      .then(response => response ? response.data : {});
  }

  getCustomerCompleteById(id) {
    const url = `${CUSTOMERS_URL}/${id}/info`;
    return this.$http.get(url)
      .then(response => response ? response.data : {});
  }

  saveCustomer(customer) {
    const url = `${CUSTOMERS_URL}/${customer.id}`;
    return this.$http.put(
      url, angular.toJson(customer)
    ).then(response => response.data);
  }

  createCustomer(customer) {
    return this.$http.post(
      CUSTOMERS_URL, angular.toJson(customer)
    ).then(response => {
      return response.data;
    });
  }

  duplicateCustomer(customer) {
    const url = `${CUSTOMERS_URL}/duplicate`;
    return this.$http.post(url, angular.toJson(customer))
    .then(response => {
      return response.data;
    });
  }

  deleteCustomerById(id) {
    const url = `${CUSTOMERS_URL}/${id}`;
    return this.$http.delete(url)
      .then(response => response.data);
  }
}
