class ConfigurationListController {
  constructor() {
    this.conftype = [
      {text: 'CUSTOMER_BANK', value: 'config0'},
      {text: 'BANK_CUSTOMER_S', value: 'config1'},
      {text: 'BANK_CUSTOMER_R', value: 'config2'}
    ];
  }

  hasConfigurations() {
    return this.configurations.length > 0;
  }

  oneSelected() {
    return this.confChks.length === 1;
  }

  $onInit() {
    this.reverse = false;
    this.action = true;
    this.selectedRow = null;
  }

  sort(keyname) {
    this.sortKey = keyname;
    this.reverse = !this.reverse;
  }

  delete(id) {
    this.action = !this.action;
    this.deleteConfiguration(id);
  }

  editConfig(id) {
    this.confChks = [];
    this.selectedRow = null;
    this.edit(id);
  }

  addOrRemove(item) {
    const isPresent = (this.confChks.indexOf(item.idConfig) > -1);
    if (!isPresent) {
      this.selectedRow = item.idConfig;
      this.confChks.splice(0, 1);
    } else {
      this.selectedRow = null;
      this.confChks.splice(0, 1, item.idConfig);
    }
  }
}
export const ConfigurationListComponent = {
  template: require('./configuration-list.html'),
  controller: ConfigurationListController,
  bindings: {
    configurations: '<',
    action: '<',
    nbAction: '&',
    edit: '&',
    duplicateConfig: '&',
    deleteConfiguration: '&',
    isViewer: '&',
    confChks: '='
  }
};
