import {CONFIGURATIONS_URL} from '../../../common/constants.js';

export class ConfigurationService {
  /** @ngInject */
  constructor($http) {
    this.$http = $http;
  }

  getConfigurations() {
    return this.$http.get(CONFIGURATIONS_URL)
      .then(response => response ? response.data.data : []);
  }

  createConfiguration(configuration) {
    return this.$http.post(
      CONFIGURATIONS_URL, angular.toJson(configuration)
    ).then(response => {
      return response.data;
    });
  }

  saveConfiguration(configuration) {
    const url = `${CONFIGURATIONS_URL}/${configuration.idConfig}`;
    return this.$http.put(
      url, angular.toJson(configuration)
    ).then(response => response.data);
  }

  deleteConfigurationById(id) {
    const url = `${CONFIGURATIONS_URL}/${id}`;
    return this.$http.delete(url)
      .then(response => response.data);
  }

  getConfigById(id) {
    const url = `${CONFIGURATIONS_URL}/${id}`;
    return this.$http.get(url)
      .then(response => response ? response.data.data : {});
  }
}
