import {NOTIFICATION, CONFIG, ORIGEN0, ORIGEN1, FORMATO0, FORMATO1, FORMATO2, PARAMETER1,
   PARAMETER2, SEPARATOR, RENAME_ROW, ACCOUNT, TYPE_DATES} from '../../../common/constants.js';

class CustomerDuplicateController {
  /** @ngInject */
  constructor(customerService, $state, PermRoleStore, configurationService) {
    this.PermRoleStore = PermRoleStore;
    this.$state = $state;
    this.customerService = customerService;
    this.configurationService = configurationService;
    this.confChks = [];
    this.origen0 = ORIGEN0;
    this.origen1 = ORIGEN1;
    this.formato0 = FORMATO0;
    this.formato1 = FORMATO1;
    this.formato2 = FORMATO2;
    this.parameter1 = PARAMETER1;
    this.parameter2 = PARAMETER2;
    this.separator = SEPARATOR;
    this.element = RENAME_ROW;
    this.account = ACCOUNT;
    this.typeDates = TYPE_DATES;
    this.destino1 = false;
    this.destino2 = false;
    this.element.parametro = PARAMETER2[0].value;
    this.element.separador = SEPARATOR[0].value;
  }

  $onInit() {
    this.getCustomerDetail();
    this.title = this.resolve.title;
    this.selectedConfig = angular.copy(CONFIG);
    this.isCollapsed = true;
    this.warning = true;
    this.action = true;
    this.errorFormConfig = false;
    this.texto = true;
  }

  copy() {
    this.selectedCustomer.name = this.selectedCustomer.name + '_copy';
    this.selectedCustomer.alias = this.selectedCustomer.alias + '_copy';
    this.selectedCustomer.originalId = this.selectedCustomer.id;
    this.selectedCustomer.id = null;
    angular.forEach(this.selectedCustomer.config, config => {
      config.nameConf += '_copy';
      config.subalias = config.idConfig + 'copy';
      config.subalias = config.subalias.length < 6 ? '0' + config.subalias : config.subalias;
      config.subalias = config.subalias.length > 8 ? config.subalias.substring(0, 8) : config.subalias;
    });
  }

  getCustomerDetail() {
    this.waiting = true;
    this.customerService.getCustomerCompleteById(this.resolve.customerId).then(response => {
      if (response.data) {
        this.selectedCustomer = response.data;
        this.copy();
        this.selectedCustomer.config.forEach((conf, i) => conf.idConfig = i);
        this.configurations = this.selectedCustomer.config;
      } else {
        this.handleError(response.error);
      }
      this.waiting = false;
    }).catch(() => {
      this.waiting = false;
      this.handleError('CUSTOMER_NOT_EXIST');
    });
  }

  addField() {
    this.selectedConfig.renombrado.value.push({
      idExpr: '0',
      parametro: this.element.parametro,
      valor: this.element.valor,
      separador: this.element.separador
    });
    this.resetRename();
  }

  changeAscii() {
    this.selectedConfig.pgp.textMode = this.selectedConfig.pgp.textMode === '0' ? '1' : '0';
  }

  changeTextMode() {
    this.selectedConfig.pgp.ascii = this.selectedConfig.pgp.ascii === '0' ? '1' : '0';
  }

  validateSelect() {
    if (this.element.parametro === 'FIJO' || this.element.parametro === 'FORMATO') {
      this.texto = true;
      this.fecha = false;
      this.desplegable = false;
      this.element.valor = '';
    }
    if (this.element.parametro === 'FECHA') {
      this.texto = false;
      this.fecha = true;
      this.desplegable = false;
      this.element.valor = this.typeDates[0];
    }
    if (this.element.parametro === 'CUENTA') {
      this.desplegable = true;
      this.texto = false;
      this.fecha = false;
      this.element.valor = this.account[0].value;
    }
    if (this.element.parametro === 'SECUENCIAL') {
      this.texto = true;
      this.fecha = false;
      this.desplegable = false;
      this.element.valor = '-';
    }
  }

  isViewer() {
    return this.PermRoleStore.getRoleDefinition('VIEWER').validationFunction[1]('VIEWER');
  }

  ok() {
    this.waiting = true;
    this.createCustomer(this.selectedCustomer);
  }

  validateFormConfiguration() {
    this.errorFormConfig = false;
    if (!this.selectedConfig.nameConf || !this.selectedConfig.subalias ||
      !this.selectedConfig.formato || !this.selectedConfig.origen || !this.selectedConfig.destino) {
      this.errorFormConfig = true;
    }
    if (this.typeConfig !== 'CB' && this.selectedConfig.renombrado.tipo === '1' && this.selectedConfig.renombrado.value.length === 0) {
      this.errorFormConfig = true;
    }
    if (this.selectedConfig.pgpx === '1' && this.selectedConfig.pgp.tipoCifrado === '0' &&
      this.selectedConfig.pgp.tipoFirma === '0') {
      this.errorFormConfig = true;
    }
    if (this.typeConfig === 'CB' && this.selectedConfig.pgp.tipoFirma === '1' && !this.selectedConfig.pgp.key) {
      this.errorFormConfig = true;
    }
    if ((this.typeConfig === 'STATEMENT' || this.typeConfig === 'RESPONSE') && this.selectedConfig.pgp.tipoCifrado === '1' && !this.selectedConfig.pgp.key) {
      this.errorFormConfig = true;
    }
  }
  resetRename() {
    this.element.parametro = PARAMETER2[0].value;
    this.element.separador = SEPARATOR[0].value;
    this.element.valor = '';
    this.texto = true;
    this.fecha = false;
    this.desplegable = false;
  }
  resetPGP() {
    if (this.selectedConfig.pgpx === '0') {
      this.selectedConfig.pgp.tipoCifrado = '0';
      this.selectedConfig.pgp.tipoFirma = '0';
      this.selectedConfig.pgp.key = '';
      this.selectedConfig.pgp.compress = '0';
      this.selectedConfig.pgp.ascii = '0';
      this.selectedConfig.pgp.textMode = '1';
    }
  }

  okConfiguration() {
    this.resetPGP();
    if (this.typeConfig === 'STATEMENT' || this.typeConfig === 'RESPONSE') {
      this.selectedConfig.tipoDestino = this.destino1 && this.destino2 ? 1 : 0;
      if (this.destino1 && this.destino2) {
        this.selectedConfig.destino = 'MFT_EXTERNO|IPLA';
      } else if (this.destino2) {
        this.selectedConfig.destino = 'IPLA';
      } else if (this.destino1) {
        this.selectedConfig.destino = 'MFT_EXTERNO';
      }
    }

    this.validateFormConfiguration();
    if (this.errorFormConfig === false) {
      angular.forEach(this.selectedConfig.renombrado.value, (val, i) => val.orden = i + 1);
      if (this.selectedConfig.idConfig !== '') {
        this.saveConfiguration();
      } else {
        this.createConfiguration();
      }
    }
  }

  save() {
    if (this.selectedConfig.idConfig) {
      this.saveConfiguration();
    } else {
      this.createConfiguration();
    }
  }

  okCustomer() {
    this.waiting = true;
    if (this.selectedCustomer.id) {
      this.saveCustomer(this.selectedCustomer);
    } else {
      this.createCustomer(this.selectedCustomer);
    }
  }

  createCustomer(customer) {
    customer.config = this.configurations;
    this.customerService.duplicateCustomer(customer).then(response => {
      if (response.data) {
        this.handleSuccess(response.message);
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_CREATE_CUSTOMER'));
  }

  saveCustomer(customer) {
    this.customerService.saveCustomer(customer).then(response => {
      if (response.data) {
        this.handleSuccess(response.message);
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_UPDATE_CUSTOMER'));
  }

  handleSuccess(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'green';
    notice.message = message;
    this.waiting = false;
    this.close({$value: notice});
  }

  handleError(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'red';
    notice.message = message;
    this.waiting = false;
    this.close({$value: notice});
  }

  delete() {
    console.info('No deleting customer in duplicate');
  }

  cancel() {
    this.dismiss();
  }

  cancelConfig() {
    this.$state.go('detail');
  }

  hideAlert() {
    this.notification.show = false;
  }

  edit(id) {
    this.destino1 = false;
    this.destino2 = false;
    this.resetRename();
    this.selectedConfig = angular.copy(this.configurations.filter(conf => conf.idConfig === id)[0]);
    if (this.selectedConfig) {
      if (this.selectedConfig.destino.indexOf('MFT_EXTERNO') !== -1) {
        this.destino1 = true;
      }
      if (this.selectedConfig.destino.indexOf('IPLA') !== -1) {
        this.destino2 = true;
      }
      if (this.selectedConfig.destino === 'GEMC') {
        this.typeConfig = 'CB';
        this.contentUrl = 'config0';
      }
      if (this.selectedConfig.origen === 'SIC' || this.selectedConfig.origen === 'REPAL' || this.selectedConfig.origen === 'IC') {
        this.typeConfig = 'STATEMENT';
        this.contentUrl = 'config1';
        this.selectedConfig.destino = '';
      }
      if (this.selectedConfig.origen === 'GEMC') {
        this.typeConfig = 'RESPONSE';
        this.contentUrl = 'config2';
        this.selectedConfig.destino = '';
      }
      this.$state.go(this.contentUrl);
    } else {
      this.handleError('KO_GET_CONFIG');
    }
  }

  duplicateConfig(id) {
    this.destino1 = false;
    this.destino2 = false;
    this.resetRename();
    this.selectedConfig = angular.copy(this.configurations.filter(conf => conf.idConfig === id)[0]);
    if (this.selectedConfig) {
      this.selectedConfig.idConfig = '';
      this.selectedConfig.nameConf = this.selectedConfig.nameConf + '_copy';
      this.selectedConfig.subalias = this.selectedConfig.subalias + '_copy';
      if (this.selectedConfig.destino.indexOf('MFT_EXTERNO') !== -1) {
        this.destino1 = true;
      }
      if (this.selectedConfig.destino.indexOf('IPLA') !== -1) {
        this.destino2 = true;
      }
      if (this.selectedConfig.destino === 'GEMC') {
        this.typeConfig = 'CB';
        this.contentUrl = 'config0';
      }
      if (this.selectedConfig.origen === 'SIC' || this.selectedConfig.origen === 'REPAL' || this.selectedConfig.origen === 'IC') {
        this.typeConfig = 'STATEMENT';
        this.contentUrl = 'config1';
      }
      if (this.selectedConfig.origen === 'GEMC') {
        this.typeConfig = 'RESPONSE';
        this.contentUrl = 'config2';
      }
    } else {
      this.handleError('KO_GET_CONFIG');
    }
    this.$state.go(this.contentUrl);
  }

  /** CONFIGURATION METHODS */
  newConfiguration(confType) {
    angular.copy(CONFIG, this.selectedConfig);
    this.destino1 = false;
    this.destino2 = false;
    this.resetRename();
    if (confType === 'config0') {
      this.typeConfig = 'CB';
      this.selectedConfig.destino = 'GEMC';
      this.selectedConfig.tipoDestino = '0';
      this.selectedConfig.idCustomer = this.selectedCustomer.id;
    }
    if (confType === 'config1') {
      this.typeConfig = 'STATEMENT';
      this.selectedConfig.idCustomer = this.selectedCustomer.id;
    }
    if (confType === 'config2') {
      this.typeConfig = 'RESPONSE';
      this.selectedConfig.origen = 'GEMC';
      this.selectedConfig.idCustomer = this.selectedCustomer.id;
    }
    this.$state.go(confType);
  }

  createConfiguration() {
    this.selectedConfig.tipoDestino = this.destino1 && this.destino2 ? 1 : 0;
    this.configurations.push({
      idConfig: this.configurations.length === 0 ? 0 : parseInt(this.configurations[this.configurations.length - 1].idConfig, 10) + 1,
      nameConf: this.selectedConfig.nameConf,
      subalias: this.selectedConfig.subalias,
      formato: this.selectedConfig.formato,
      origen: this.selectedConfig.origen,
      destino: this.selectedConfig.destino,
      tipoDestino: this.selectedConfig.tipoDestino,
      pgpx: this.selectedConfig.pgpx,
      auduserConf: this.selectedConfig.auduserConf,
      renombrado: this.selectedConfig.renombrado,
      pgp: this.selectedConfig.pgp,
      audateConf: new Date().toISOString().slice(0, 10)
    });
    this.$state.go('detail');
  }

  saveConfiguration() {
    const index = this.configurations.findIndex(conf => conf.idConfig === this.selectedConfig.idConfig);
    this.configurations[index] = this.selectedConfig;
    console.info(this.selectedConfig);
    this.$state.go('detail');
  }

  deleteConfiguration(id) {
    for (let i = 0; i <= this.configurations.length - 1; i++) {
      if (parseInt(this.configurations[i].idConfig, 10) === parseInt(id, 10)) {
        this.configurations.splice(i, 1);
      }
    }
    this.confChks = [];
  }
}

export const CustomerDuplicateComponent = {
  template: '<div ui-view="customers"></div>',
  controller: CustomerDuplicateController,
  bindings: {
    close: '&',
    dismiss: '&',
    resolve: '<'
  }
};
