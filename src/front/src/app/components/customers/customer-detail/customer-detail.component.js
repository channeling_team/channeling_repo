import {NOTIFICATION, CONFIG, ORIGEN0, ORIGEN1, FORMATO0, FORMATO1,
  FORMATO2, PARAMETER1, PARAMETER2, SEPARATOR, RENAME_ROW, ACCOUNT, TYPE_DATES, SEQUENTIAL_LENGHT} from '../../../common/constants.js';

class CustomerDetailController {
  /** @ngInject */
  constructor(customerService, $state, PermRoleStore, configurationService) {
    this.PermRoleStore = PermRoleStore;
    this.$state = $state;
    this.customerService = customerService;
    this.configurationService = configurationService;
    this.confChks = [];
    this.origen0 = ORIGEN0;
    this.origen1 = ORIGEN1;
    this.formato0 = FORMATO0;
    this.formato1 = FORMATO1;
    this.formato2 = FORMATO2;
    this.parameter1 = PARAMETER1;
    this.parameter2 = PARAMETER2;
    this.separator = SEPARATOR;
    this.element = RENAME_ROW;
    this.account = ACCOUNT;
    this.typeDates = TYPE_DATES;
    this.sequentialLenght = SEQUENTIAL_LENGHT;
    this.resetRename();
  }

  $onInit() {
    if (this.resolve.customerId) {
      this.getCustomerDetail(this.resolve.customerId);
    }
    this.title = this.resolve.title;
    this.selectedConfig = angular.copy(CONFIG);
    this.isCollapsed = true;
    this.warning = true;
    this.action = true;
    this.texto = true;
    this.texto2 = false;
    this.errorFormConfig = false;
    this.destino1 = false;
    this.destino2 = false;
    this.destino3 = false;
  }

  getCustomerDetail(customerId) {
    this.waiting = true;
    this.customerService.getCustomerById(customerId).then(response => {
      if (response.data) {
        this.selectedCustomer = response.data;
      } else {
        this.handleError(response.error);
      }
      this.waiting = false;
    }).catch(() => {
      this.waiting = false;
      this.handleError('CUSTOMER_NOT_EXIST');
    });
  }

  addField() {
    this.selectedConfig.renombrado.value.push({
      idExpr: '0',
      parametro: this.element.parametro,
      // valor2 solo vale para mostrar en web, en realidad se pasa a channeling concatenado.
      // valor: this.elemeng.valor.split('\\/:[^\\[A-Z]*'),
      valor: this.element.valor + ':' + this.element.valor2,
      valor2: this.element.valor2,
      separador: this.element.separador
    });

    this.resetRename();
  }

  changeAscii() {
    this.selectedConfig.pgp.textMode = this.selectedConfig.pgp.textMode === '0' ? '1' : '0';
  }

  changeTextMode() {
    this.selectedConfig.pgp.ascii = this.selectedConfig.pgp.ascii === '0' ? '1' : '0';
  }

  validateSelect() {
    if (this.element.parametro === 'FIJO' || this.element.parametro === 'FORMATO') {
      this.texto = true;
      this.texto2 = false;
      this.fecha = false;
      this.secuencial = false;
      this.desplegable = false;
      this.element.valor = '';
    }
    if (this.element.parametro === 'FECHA') {
      this.texto = false;
      this.texto2 = false;
      this.fecha = true;
      this.secuencial = false;
      this.desplegable = false;
      this.element.valor = this.typeDates[0];
    }
    if (this.element.parametro === 'CUENTA') {
      this.desplegable = true;
      this.texto2 = false;
      this.texto = false;
      this.secuencial = false;
      this.fecha = false;
      this.element.valor = this.account[0].value;
    }
    if (this.element.parametro === 'SECUENCIAL') {
      this.texto = false;
      this.texto2 = false;
      this.fecha = false;
      this.secuencial = true;
      this.desplegable = false;
      this.element.valor = this.sequentialLenght[0];
    }
    if (this.element.parametro === 'CROSSREF') {
      this.texto = true;
      this.texto2 = true;
      this.fecha = false;
      this.secuencial = false;
      this.desplegable = false;
      this.element.valor2 = '';
      this.element.valor = '';
    }
  }

  isViewer() {
    return this.PermRoleStore.getRoleDefinition('VIEWER').validationFunction[1]('VIEWER');
  }

  okCustomer() {
    this.validateFormCustomer();
    if (this.errorFormCustomer === false) {
      if (this.selectedCustomer.id) {
        this.saveCustomer(this.selectedCustomer);
      } else {
        this.createCustomer(this.selectedCustomer);
      }
    }
  }

  createCustomer(customer) {
    this.customerService.createCustomer(customer).then(response => {
      if (response.data) {
        this.selectedCustomer.id = response.data;
        this.selectedCustomer.config = [];
        this.waiting = false;
        this.notification = NOTIFICATION;
        this.notification.show = true;
        this.notification.type = 'green';
        this.notification.message = response.message;
        this.clientCreated = true;
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_CREATE_CUSTOMER'));
  }

  saveCustomer(customer) {
    this.customerService.saveCustomer(customer).then(response => {
      if (response.data) {
        this.handleSuccess(response.message);
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_UPDATE_CUSTOMER'));
  }

  delete() {
    this.customerService.deleteCustomerById(this.selectedCustomer.id).then(response => {
      if (response.data) {
        this.handleSuccess(response.message);
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_DELETE_CUSTOMER'));
  }

  handleSuccess(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'green';
    notice.message = message;
    this.waiting = false;
    this.close({$value: notice});
  }

  handleError(message) {
    const notice = NOTIFICATION;
    notice.show = true;
    notice.type = 'red';
    notice.message = message;
    this.waiting = false;
    this.close({$value: notice});
  }

  cancel() {
    this.dismiss({$value: this.clientCreated});
  }

  cancelConfig() {
    this.$state.go('detail');
  }

  hideAlert() {
    this.notification.show = false;
  }

  edit(id) {
    this.waiting = true;
    this.destino1 = false;
    this.destino2 = false;
    this.destino3 = false;
    this.resetRename();
    this.configurationService.getConfigById(id).then(response => {
      if (response) {
        this.selectedConfig = response;
        this.errorFormConfig = false;
        if (this.selectedConfig.destino === 'MFT_EXTERNO') {
          this.destino1 = true;
        }
        if (this.selectedConfig.destino === 'IPLA') {
          this.destino2 = true;
        }
        if (this.selectedConfig.destino === 'IPLA_CAM') {
          this.destino3 = true;
        }
        if (response.destino === 'GEMC') {
          this.typeConfig = 'CB';
          this.contentUrl = 'config0';
        }
        if (response.origen === 'SIC' || response.origen === 'REPAL' || response.origen === 'IC') {
          this.typeConfig = 'STATEMENT';
          this.contentUrl = 'config1';
          this.selectedConfig.destino = '';
        }
        if (response.origen === 'GEMC') {
          this.typeConfig = 'RESPONSE';
          this.contentUrl = 'config2';
          this.selectedConfig.destino = '';
        }
      } else {
        this.handleError('KO_GET_CONFIG');
      }
      this.$state.go(this.contentUrl);
      this.waiting = false;
    }).catch(() => this.handleError('KO_GET_CONFIG'));
  }

  duplicateConfig(id) {
    this.destino1 = false;
    this.destino2 = false;
    this.destino3 = false;
    this.resetRename();
    this.configurationService.getConfigById(id).then(response => {
      if (response) {
        this.selectedConfig = response;
        this.selectedConfig.idConfig = '';
        this.selectedConfig.nameConf = this.selectedConfig.nameConf + '_copy';
        this.selectedConfig.subalias = this.selectedConfig.subalias + '_copy';
        if (this.selectedConfig.destino.indexOf('MFT_EXTERNO') !== -1) {
          this.destino1 = true;
        }
        if (this.selectedConfig.destino.indexOf('IPLA') !== -1) {
          this.destino2 = true;
        }
        if (this.selectedConfig.destino.indexOf('IPLA_CAM') !== -1) {
          this.destino3 = true;
        }
        if (response.destino === 'GEMC') {
          this.typeConfig = 'CB';
          this.contentUrl = 'config0';
        }
        if (response.origen === 'SIC' || response.origen === 'REPAL' || response.origen === 'IC') {
          this.typeConfig = 'STATEMENT';
          this.contentUrl = 'config1';
        }
        if (response.origen === 'GEMC') {
          this.typeConfig = 'RESPONSE';
          this.contentUrl = 'config2';
        }
      } else {
        this.handleError('KO_GET_CONFIG');
      }
      this.$state.go(this.contentUrl);
    }).catch(() => this.handleError('KO_GET_CONFIG'));
  }

  /** CONFIGURATION METHODS */

  newConfiguration(confType) {
    angular.copy(CONFIG, this.selectedConfig);
    this.destino1 = false;
    this.destino2 = false;
    this.destino3 = false;
    this.errorFormConfig = false;
    this.selectedConfig.idCustomer = this.selectedCustomer.id;
    this.resetRename();
    if (confType === 'config0') {
      this.typeConfig = 'CB';
      this.selectedConfig.destino = 'GEMC';
      this.selectedConfig.tipoDestino = '0';
    }
    if (confType === 'config1') {
      this.typeConfig = 'STATEMENT';
    }
    if (confType === 'config2') {
      this.selectedConfig.origen = 'GEMC';
      this.typeConfig = 'RESPONSE';
    }
    this.$state.go(confType);
  }

  validateFormCustomer() {
    this.errorFormCustomer = false;
    if (!this.selectedCustomer.name || !this.selectedCustomer.alias) {
      this.errorFormCustomer = true;
    }
  }

  validateFormConfiguration() {
    this.errorFormConfig = false;
    if (!this.selectedConfig.nameConf || !this.selectedConfig.subalias ||
      !this.selectedConfig.formato || !this.selectedConfig.origen || !this.selectedConfig.destino) {
      this.errorFormConfig = true;
    }
    if (this.typeConfig !== 'CB' && this.selectedConfig.renombrado.tipo === '1' && this.selectedConfig.renombrado.value.length === 0) {
      this.errorFormConfig = true;
    }
    if (this.selectedConfig.pgpx === '1' && this.selectedConfig.pgp.tipoCifrado === '0' &&
      this.selectedConfig.pgp.tipoFirma === '0') {
      this.errorFormConfig = true;
    }
    if (this.typeConfig === 'CB' && this.selectedConfig.pgp.tipoFirma === '1' && !this.selectedConfig.pgp.key) {
      this.errorFormConfig = true;
    }
    if ((this.typeConfig === 'STATEMENT' || this.typeConfig === 'RESPONSE') && this.selectedConfig.pgp.tipoCifrado === '1' && !this.selectedConfig.pgp.key) {
      this.errorFormConfig = true;
    }
  }

  resetRename() {
    this.element.parametro = PARAMETER2[0].value;
    this.element.separador = SEPARATOR[0].value;
    this.element.valor = '';
    this.element.valor2 = '';
    this.texto = true;
    this.texto2 = false;
    this.fecha = false;
    this.desplegable = false;
    this.secuencial = false;
  }

  resetPGP() {
    if (this.selectedConfig.pgpx === '0') {
      this.selectedConfig.pgp.tipoCifrado = '0';
      this.selectedConfig.pgp.tipoFirma = '0';
      this.selectedConfig.pgp.key = '';
      this.selectedConfig.pgp.compress = '0';
      this.selectedConfig.pgp.ascii = '0';
      this.selectedConfig.pgp.textMode = '1';
    }
  }

  okConfiguration() {
    this.resetPGP();
    if (this.typeConfig === 'STATEMENT' || this.typeConfig === 'RESPONSE') {
      this.selectedConfig.tipoDestino = this.destino1 && this.destino2 ? 1 : 0;
      if (this.destino1 && this.destino2 && this.destino3) {
        this.selectedConfig.destino = 'MFT_EXTERNO|IPLA|IPLA_CAM';
      } else if (this.destino2) {
        this.selectedConfig.destino = 'IPLA';
      } else if (this.destino1) {
        this.selectedConfig.destino = 'MFT_EXTERNO';
      } else if (this.destino3) {
        this.selectedConfig.destino = 'IPLA_CAM';
      }
    }

    this.validateFormConfiguration();
    if (this.errorFormConfig === false) {
      angular.forEach(this.selectedConfig.renombrado.value, (val, i) => val.orden = i + 1);
      if (this.selectedConfig.idConfig !== '') {
        this.saveConfiguration();
      } else {
        this.createConfiguration();
      }
    }
  }

  createConfiguration() {
    this.configurationService.createConfiguration(this.selectedConfig).then(response => {
      if (response.data && response.data !== 'KO') {
        this.waiting = false;
        this.notification = NOTIFICATION;
        this.notification.show = true;
        this.notification.type = 'green';
        this.notification.message = 'OK_CREATE_CONFIG';
        this.customerService.getCustomerById(this.selectedCustomer.id).then(response => {
          this.selectedCustomer.config = response.data.config;
        });
        this.selectedConfig = {};
        this.confChks = [];
        this.$state.go('detail');
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_CREATE_CONFIG'));
  }

  saveConfiguration() {
    this.configurationService.saveConfiguration(this.selectedConfig).then(response => {
      if (response.data) {
        this.waiting = false;
        this.notification = NOTIFICATION;
        this.notification.show = true;
        this.notification.type = 'green';
        this.notification.message = 'OK_UPDATE_CONFIG';
        this.customerService.getCustomerById(this.selectedCustomer.id).then(response => {
          this.selectedCustomer.config = response.data.config;
        });
        this.selectedConfig = {};
        this.confChks = [];
        this.$state.go('detail');
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_UPDATE_CONFIG'));
  }

  deleteConfiguration(id) {
    this.configurationService.deleteConfigurationById(id).then(response => {
      if (response.data) {
        this.waiting = false;
        this.notification = NOTIFICATION;
        this.notification.show = true;
        this.notification.type = 'green';
        this.notification.message = 'OK_DELETE_CONFIG';
        this.customerService.getCustomerById(this.selectedCustomer.id).then(response => {
          this.selectedCustomer.config = response.data.config;
        });
        this.selectedConfig = {};
        this.confChks = [];
        this.$state.go('detail');
      } else {
        this.handleError(response.error);
      }
    }).catch(() => this.handleError('KO_DELETE_CONFIG'));
  }
}

export const CustomerDetailComponent = {
  template: '<div ui-view="customers"></div>',
  controller: CustomerDetailController,
  bindings: {
    close: '&',
    dismiss: '&',
    resolve: '<'
  }
};
