export const USERS_URL = 'channeling/api/user';
export const CUSTOMERS_URL = 'channeling/api/customer';
export const FILES_URL = 'channeling/api/filemonitor';
export const CONFIGURATIONS_URL = 'channeling/api/configuration';
export const ROLE_URL = 'channeling/auth/roles';
export const RECORDS = {
  value: '10',
  values: ['10', '20', '40']
};
export const NOTIFICATION = {
  show: false,
  message: '',
  type: 'green',
  time: '3500'
};
export const ORIGEN0 = {
  values: ['MFT_EXTERNO', 'IPLA', 'IPLA_CAM', 'PORTAL_GTB']
};
export const ORIGEN1 = {
  values: ['SIC', 'REPAL', 'IC']
};
export const FORMATO0 = {
  values: ['ALL', 'PAIN01', 'XMLV3I', 'PAIN08', 'PAIN07', 'PAYMUL', 'SPIDOC', 'MT101', 'COLRBR', 'CONPTG',
    'PS2PTG', 'ESPFAC', 'ESPI34', 'ESPICF', 'ESPN58', 'PTGICH', 'PTGICO', 'PTGIPS', 'IDOCXX']
};

export const FORMATO1 = {
  SIC: [
    'MT940',
    'FINSTA',
    'BAI',
    'XML',
    'N43'
  ],
  REPAL: [
    'MT942',
    'XML'
  ],
  IC: [
    'MT940',
    'FINSTA',
    'BAI',
    'XML',
    'N43',
    'MT941',
    'MT942',
    'CAMT052'
  ]
};

export const FORMATO2 = {
  values: ['PAIN02', 'XMLV3O', 'BANSTA', 'MT199', 'COLRBR', 'CONPTG', 'PS2PTG',
    'ESPNOT', 'PTGCHQ', 'PTGCON', 'PTGPS2', 'CONTRL']
};

export const PARAMETER1 = [{
  name: 'PARAM_FIJO',
  value: 'FIJO'
}, {
  name: 'PARAM_FECHA',
  value: 'FECHA'
}, {
  name: 'PARAM_FORMATO',
  value: 'FORMATO'
}, {
  name: 'PARAM_CUENTA',
  value: 'CUENTA'
}, {
  name: 'PARAM_CROSSREF',
  value: 'CROSSREF'
}, {
  name: 'PARAM_SECUENCIAL',
  value: 'SECUENCIAL'
}];
export const PARAMETER2 = [{
  name: 'PARAM_FIJO',
  value: 'FIJO'
}, {
  name: 'PARAM_FECHA',
  value: 'FECHA'
}, {
  name: 'PARAM_FORMATO',
  value: 'FORMATO'
}, {
  name: 'PARAM_SECUENCIAL',
  value: 'SECUENCIAL'
}];
export const SEPARATOR = [{
  name: 'PARAM_UNDERSCORE',
  value: '_'
}, {
  name: 'PARAM_DASH',
  value: '-'
}, {
  name: 'PARAM_DOT',
  value: '.'
}, {
  name: 'PARAM_SPACE',
  value: ' '
}, {
  name: 'PARAM_EMPTY',
  value: ''
}];

export const ACCOUNT = [{
  name: 'PARAM_conC',
  value: 'conC'
}, {
  name: 'PARAM_sinC',
  value: 'sinC'
}];

export const TYPE_DATES = ['Dyymmdd.Hhhmmss',
  'Dyymmdd.Thhmm',
  'Dyymmddhhmmss',
  'yyyymmddhhmmss',
  'yyyymmdd_hhmmss',
  'yymmdd_hhmmss',
  'yymmdd.hhmm',
  'yymmdd.hhmmss',
  'yymmdd-hhmm',
  'yyyymmddhhmmsssss',
  'yymmdd_hhmm',
  'yymmdd',
  'ddmmyyyyhhmmss',
  'Dyymmdd.Thhmmss',
  'Dyymmdd_Hhhmmss',
  'YYYYMMDD_hhmmss',
  'yyyy',
  'yy',
  'mm',
  'dd',
  'hh',
  'mm',
  'ss',
  'sss'];

export const RENAME_ROW = {
  idExpr: '',
  orden: '',
  parametro: '',
  valor: '',
  separador: ''
};

export const SEQUENTIAL_LENGHT = [
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9'];

export const TYPESTATE = [{
  name: '',
  value: '',
  state: [
    {name: '', id: '', ko: false},
    {name: 'STATE_RECEIVED', id: '1', ko: false},
    {name: 'STATE_PGP_OK', id: '2', ko: false},
    {name: 'STATE_PGP_KO', id: '3', ko: true},
    {name: 'STATE_CYPHER_OK', id: '4', ko: false},
    {name: 'STATE_CYPHER_KO', id: '5', ko: true},
    {name: 'STATE_AUTH_OK', id: '6', ko: false},
    {name: 'STATE_AUTH_KO', id: '7', ko: true},
    {name: 'STATE_SIGNED_OK', id: '8', ko: false},
    {name: 'STATE_SIGNED_KO', id: '9', ko: true},
    {name: 'STATE_BANKSPHERE_OK', id: '10', ko: false},
    {name: 'STATE_BANKSPHERE_KO', id: '11', ko: true},
    {name: 'STATE_3SKEY_OK', id: '12', ko: false},
    {name: 'STATE_BANKSPHERE_RESP_KO', id: '13', ko: true},
    {name: 'STATE_FILE_OK', id: '14', ko: false},
    {name: 'STATE_FILE_KO', id: '15', ko: true},
    {name: 'STATE_RENAMED_OK', id: '16', ko: false},
    {name: 'STATE_RENAMED_KO', id: '17', ko: true},
    {name: 'STATE_GEMC_OK', id: '18', ko: false},
    {name: 'STATE_GEMC_KO', id: '19', ko: true},
    {name: 'STATE_IPLA_OK', id: '20', ko: false},
    {name: 'STATE_IPLA_KO', id: '21', ko: true},
    {name: 'STATE_MFT_OK', id: '22', ko: false},
    {name: 'STATE_MFT_KO', id: '23', ko: true},
    {name: 'STATE_CUSTOMER_NOTFOUND_KO', id: '98', ko: true},
    {name: 'STATE_UNEXPECTED_ERROR_KO', id: '99', ko: true}],
  format: [
      {name: ''},
      {name: 'PAIN01'},
      {name: 'XMLV3I'},
      {name: 'PAIN08'},
      {name: 'PAIN07'},
      {name: 'PAYMUL'},
      {name: 'SPIDOC'},
      {name: 'MT101'},
      {name: 'COLRBR'},
      {name: 'CONPTG'},
      {name: 'PS2PTG'},
      {name: 'ESPFAC'},
      {name: 'ESPI34'},
      {name: 'ESPICF'},
      {name: 'ESPN58'},
      {name: 'PTGICH'},
      {name: 'PTGICO'},
      {name: 'PTGIPS'},
      {name: 'IDOCXX'},
      {name: 'PAIN02'},
      {name: 'XMLV3O'},
      {name: 'BANSTA'},
      {name: 'MT199'},
      {name: 'ESPNOT'},
      {name: 'PTGCHQ'},
      {name: 'PTGCON'},
      {name: 'PTGPS2'},
      {name: 'CONTRL'},
      {name: 'MT940'},
      {name: 'MT941'},
      {name: 'MT942'},
      {name: 'FINSTA'},
      {name: 'BAI'},
      {name: 'APPEN'},
      {name: 'XML'},
      {name: 'N43'},
      {name: 'CAMT052'}]
}, {
  name: 'FILE_PAYMENT',
  value: 'Pago',
  state: [
    {name: '', id: '', ko: false},
    {name: 'STATE_RECEIVED', id: '1', ko: false},
    {name: 'STATE_PGP_OK', id: '2', ko: false},
    {name: 'STATE_PGP_KO', id: '3', ko: true},
    {name: 'STATE_AUTH_OK', id: '6', ko: false},
    {name: 'STATE_AUTH_KO', id: '7', ko: true},
    {name: 'STATE_BANKSPHERE_OK', id: '10', ko: false},
    {name: 'STATE_BANKSPHERE_KO', id: '11', ko: true},
    {name: 'STATE_3SKEY_OK', id: '12', ko: false},
    {name: 'STATE_BANKSPHERE_RESP_KO', id: '13', ko: true},
    {name: 'STATE_FILE_OK', id: '14', ko: false},
    {name: 'STATE_FILE_KO', id: '15', ko: true},
    {name: 'STATE_RENAMED_OK', id: '16', ko: false},
    {name: 'STATE_RENAMED_KO', id: '17', ko: true},
    {name: 'STATE_GEMC_OK', id: '18', ko: false},
    {name: 'STATE_GEMC_KO', id: '19', ko: true},
    {name: 'STATE_CUSTOMER_NOTFOUND_KO', id: '98', ko: true},
    {name: 'STATE_UNEXPECTED_ERROR_KO', id: '99', ko: true}],
  format: [
      {name: ''},
      {name: 'PAIN01'},
      {name: 'XMLV3I'},
      {name: 'PAIN08'},
      {name: 'PAIN07'},
      {name: 'PAYMUL'},
      {name: 'SPIDOC'},
      {name: 'MT101'},
      {name: 'COLRBR'},
      {name: 'CONPTG'},
      {name: 'PS2PTG'},
      {name: 'ESPFAC'},
      {name: 'ESPI34'},
      {name: 'ESPICF'},
      {name: 'ESPN58'},
      {name: 'PTGICH'},
      {name: 'PTGICO'},
      {name: 'PTGIPS'},
      {name: 'IDOCXX'}]
}, {
  name: 'FILE_RESPONSE',
  value: 'Respuesta',
  state: [
    {name: '', id: '', ko: false},
    {name: 'STATE_RECEIVED', id: '1', ko: false},
    {name: 'STATE_CYPHER_OK', id: '4', ko: false},
    {name: 'STATE_CYPHER_KO', id: '5', ko: true},
    {name: 'STATE_SIGNED_OK', id: '8', ko: false},
    {name: 'STATE_SIGNED_KO', id: '9', ko: true},
    {name: 'STATE_RENAMED_OK', id: '16', ko: false},
    {name: 'STATE_RENAMED_KO', id: '17', ko: true},
    {name: 'STATE_IPLA_OK', id: '20', ko: false},
    {name: 'STATE_IPLA_KO', id: '21', ko: true},
    {name: 'STATE_MFT_OK', id: '22', ko: false},
    {name: 'STATE_MFT_KO', id: '23', ko: true},
    {name: 'STATE_CUSTOMER_NOTFOUND_KO', id: '98', ko: true},
    {name: 'STATE_UNEXPECTED_ERROR_KO', id: '99', ko: true}],
  format: [
      {name: ''},
      {name: 'PAIN02'},
      {name: 'XMLV3O'},
      {name: 'BANSTA'},
      {name: 'MT199'},
      {name: 'COLRBR'},
      {name: 'PS2PTG'},
      {name: 'ESPNOT'},
      {name: 'PTGCHQ'},
      {name: 'PTGCON'},
      {name: 'PTGPS2'},
      {name: 'CONTRL'}]
}, {
  name: 'FILE_STATEMENTS',
  value: 'Extracto',
  state: [
    {name: '', id: '', ko: false},
    {name: 'STATE_RECEIVED', id: '1', ko: false},
    {name: 'STATE_CYPHER_OK', id: '4', ko: false},
    {name: 'STATE_CYPHER_KO', id: '5', ko: true},
    {name: 'STATE_SIGNED_OK', id: '8', ko: false},
    {name: 'STATE_SIGNED_KO', id: '9', ko: true},
    {name: 'STATE_RENAMED_OK', id: '16', ko: false},
    {name: 'STATE_RENAMED_KO', id: '17', ko: true},
    {name: 'STATE_IPLA_OK', id: '20', ko: false},
    {name: 'STATE_IPLA_KO', id: '21', ko: true},
    {name: 'STATE_MFT_OK', id: '22', ko: false},
    {name: 'STATE_MFT_KO', id: '23', ko: true},
    {name: 'STATE_CUSTOMER_NOTFOUND_KO', id: '98', ko: true},
    {name: 'STATE_UNEXPECTED_ERROR_KO', id: '99', ko: true}],
  format: [
    {name: ''},
    {name: 'MT940'},
    {name: 'MT941'},
    {name: 'MT942'},
    {name: 'FINSTA'},
    {name: 'BAI'},
    {name: 'XML'},
    {name: 'N43'},
    {name: 'CAMT052'}]
}];
export const CONFIG = {
  idConfig: '',
  idCustomer: '',
  nameConf: '',
  subalias: '',
  formato: '',
  origen: '',
  destino: '',
  tipoDestino: '',
  s3Key: '0',
  pgpx: '0',
  descripcion: '',
  auduserConf: 'WEB',
  audateConf: '',
  renombrado: {
    tipo: 0,
    value: []
  },
  pgp: {
    tipoCifrado: '0',
    tipoFirma: '0',
    key: '',
    compress: '0',
    ascii: '0',
    textMode: '1'
  }
};

export const ROLE = {
  type: 'select',
  name: 'Roles',
  values: ['VIEWER', 'OPERATOR', 'ADMIN']
};
