import angular from 'angular';
import {NavComponent} from './nav/nav.component';
import {WarningComponent} from './warning/warning.component';
import {ControlAreaComponent} from './control-area/control-area.component';
import inputRegex from './directives/input-regex';
import stopCcp from './directives/stop-ccp';

const common = angular
  .module('app.common', [])
  .component('navCo', NavComponent)
  .component('areaCo', ControlAreaComponent)
  .component('warningCo', WarningComponent)
  .directive('regExInput', inputRegex)
  .directive('stopCcp', stopCcp)
  .name;

export default common;
