export default fileChange;

/** @ngInject */
function fileChange() {
  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      fileChange: '&'
    },
    link: (scope, element, attrs, ctrl) => {
      element.on('change', onChange);
      scope.$on('destroy', function () {
        element.off('change', onChange);
      });
      function onChange() {
        const reader = new FileReader();
        reader.onload = () => {
          const text = reader.result;
          const arrayLines = text.split('\n');
          ctrl.$setViewValue(arrayLines);
          // console.log('directiva array ---> ' + ctrl.arrayLines.length);
          scope.$apply(function () {
            scope.fileChange();
          });
        };
        const fileUp = element[0].files[0];
        // const fileUp = attrs.multiple ? element[0].files : element[0].files[0];
        reader.readAsText(fileUp);
      }
    }
  };
}
