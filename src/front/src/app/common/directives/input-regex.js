export default inputRegex;

/** @ngInject */
function inputRegex() {
  'use strict';
  return {
    restrict: 'A',
    require: '?regEx',
    scope: {},
    replace: false,
    link: (scope, element, attrs) => {
      element.bind('keypress', function (event) {
        const regex = new RegExp(attrs.regEx);
        const key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
          event.preventDefault();
          return false;
        }
      });
    }
  };
}
