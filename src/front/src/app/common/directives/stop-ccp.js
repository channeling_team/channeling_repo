export default stopCcp;

/** @ngInject */
function stopCcp() {
  return {
    link: (scope, element) => {
      element.on('cut copy paste', function (event) {
        event.preventDefault();
      });
    }
  };
}
