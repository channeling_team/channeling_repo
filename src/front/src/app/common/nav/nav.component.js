class NavController {
  /** @ngInject */
  constructor($state, $translate) {
    this.$state = $state;
    this.$translate = $translate;
    this.showBox = false;
  }

  navClass(page) {
    this.currentRoute = this.$state.current.name || 'customers';
    return page === this.currentRoute ? 'active' : '';
  }

  changeLang(lang) {
    this.$translate.use(lang);
  }
}

export const NavComponent = {
  template: require('./nav.html'),
  controller: NavController
};
