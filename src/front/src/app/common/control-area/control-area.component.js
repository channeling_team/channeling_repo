class ControlAreaController {

  selectedID() {
    return this.chks.length === 1 ? this.chks[0].id : '';
  }

  selectedName() {
    return this.chks.length === 1 ? this.chks[0].name : '';
  }

  toManySelected() {
    return this.chks.length > 1;
  }
  oneSelected() {
    return this.chks.length === 1;
  }

  isCustomer() {
    return this.nbType === 'CUSTOMER';
  }

  hideAlert() {
    this.notification.show = false;
  }
}

export const ControlAreaComponent = {
  template: require('./control-area.html'),
  controller: ControlAreaController,
  bindings: {
    nbLabel: '@',
    nbType: '@',
    nbAction: '&',
    edit: '&',
    duplicate: '&',
    delete: '&',
    chks: '=',
    notification: '='
  }
};
