
class WarningController {
  $onInit() {
    if (this.resolve) {
      this.title = this.resolve.info.title;
      this.type = this.resolve.info.type;
      this.message = this.resolve.info.message;
      this.confirm = this.close;
      this.cancel = this.dismiss;
    }
  }
}

export const WarningComponent = {
  template: require('./warning.html'),
  controller: WarningController,
  bindings: {
    title: '@',
    type: '@',
    message: '@',
    confirm: '&',
    cancel: '&',
    resolve: '<',
    close: '&',
    dismiss: '&'
  }
};
