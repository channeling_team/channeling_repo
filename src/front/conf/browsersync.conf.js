const conf = require('./gulp.conf');
const proxyMiddle = require('http-proxy-middleware');

module.exports = function () {
  const apiProxy = proxyMiddle('/channeling', {
    target: 'http://localhost:8080',
    changeOrigin: true});
  return {
    server: {
      baseDir: [
        conf.paths.tmp,
        conf.paths.src
      ],
      middleware: [apiProxy]
    },
    open: false
  };
};
