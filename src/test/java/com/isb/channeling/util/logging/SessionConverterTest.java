package com.isb.channeling.util.logging;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ch.qos.logback.classic.spi.ILoggingEvent;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RequestContextHolder.class)
public class SessionConverterTest {

	private SessionConverter utilSession;

	@Mock
	private ServletRequestAttributes attrs;

	ILoggingEvent event = null;

	@Before
	public void init() {
		utilSession = new SessionConverter();
	}

	@Test
	public void convertNull() {
		
		PowerMockito.mockStatic(RequestContextHolder.class);
		PowerMockito.when(RequestContextHolder.getRequestAttributes()).thenReturn(null);


		// invoca metodo a probar
		String result = utilSession.convert(event);

		// Comprobar resultados
		Assert.assertEquals("NO_SESSION", result);
	}

	@Test
	public void convert() {

		PowerMockito.mockStatic(RequestContextHolder.class);
		PowerMockito.when(RequestContextHolder.getRequestAttributes()).thenReturn(attrs);
		PowerMockito.when(attrs.getSessionId()).thenReturn(StringUtils.EMPTY);

		// invoca metodo a probar
		String result = utilSession.convert(event);

		// Comprobar resultados
		Assert.assertNotNull(result);
	}

}
