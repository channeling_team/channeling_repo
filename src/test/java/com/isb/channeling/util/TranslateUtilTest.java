package com.isb.channeling.util;

import org.junit.Assert;
import org.junit.Test;

public class TranslateUtilTest {
	

	@Test
	public void shouldReturnReceivedWhenTranslate() {

		// prepara el test
	  String input = "1";
	  
		// invoca metodo a probar
		String result = TranslateStatus.convertIdtoStatus(input);

		// Comprobar resultados
		Assert.assertNotNull(result);
		Assert.assertEquals("STATE_RECEIVED", result);
	}

	 @Test
	  public void shouldReturnPgpOkWhenTranslate() {

	    // prepara el test
	    String input = "2";
	    
	    // invoca metodo a probar
	    String result = TranslateStatus.convertIdtoStatus(input);

	    // Comprobar resultados
	    Assert.assertNotNull(result);
	    Assert.assertEquals("STATE_PGP_OK", result);
	  }
	 
	 @Test
   public void shouldReturnPgpKOWhenTranslate() {

     // prepara el test
     String input = "3";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_PGP_KO", result);
   }
	 
   @Test
   public void shouldReturnCypherOkWhenTranslate() {

     // prepara el test
     String input = "4";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_CYPHER_OK", result);
   }
  
   @Test
   public void shouldReturnCypherKOWhenTranslate() {

     // prepara el test
     String input = "5";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_CYPHER_KO", result);
   }
   
   @Test
   public void shouldReturnAuthOkWhenTranslate() {

     // prepara el test
     String input = "6";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_AUTH_OK", result);
   }
   
   @Test
   public void shouldReturnAuthKOWhenTranslate() {

     // prepara el test
     String input = "7";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_AUTH_KO", result);
   }
   
   @Test
   public void shouldReturnSignedOKWhenTranslate() {

     // prepara el test
     String input = "8";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_SIGNED_OK", result);
   }

   @Test
   public void shouldReturnSignedKOWhenTranslate() {

     // prepara el test
     String input = "9";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_SIGNED_KO", result);
   }
   
   @Test
   public void shouldReturn3SkeyOkWhenTranslate() {

     // prepara el test
     String input = "12";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_3SKEY_OK", result);
   }
   
   @Test
   public void shouldReturnBanksphereOkWhenTranslate() {

     // prepara el test
     String input = "10";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_BANKSPHERE_OK", result);
   }
   
   @Test
   public void shouldReturnBanksphereKOWhenTranslate() {

     // prepara el test
     String input = "11";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_BANKSPHERE_KO", result);
   }
   
   @Test
   public void shouldReturnBanksphereRespKOWhenTranslate() {

     // prepara el test
     String input = "13";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_BANKSPHERE_RESP_KO", result);
   }
   
   @Test
   public void shouldReturnFileFormatOkWhenTranslate() {

     // prepara el test
     String input = "14";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_FILE_OK", result);
   }
   
   @Test
   public void shouldReturnFileFormatKOWhenTranslate() {

     // prepara el test
     String input = "15";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_FILE_KO", result);
   }
   
   @Test
   public void shouldReturnRenamedOkWhenTranslate() {

     // prepara el test
     String input = "16";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_RENAMED_OK", result);
   }
   
   @Test
   public void shouldReturnRenamedKOWhenTranslate() {

     // prepara el test
     String input = "17";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_RENAMED_KO", result);
   }
   
   @Test
   public void shouldReturnGemcOKWhenTranslate() {

     // prepara el test
     String input = "18";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_GEMC_OK", result);
   }
   
   @Test
   public void shouldReturnGemcKOWhenTranslate() {

     // prepara el test
     String input = "19";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_GEMC_KO", result);
   }   
   
   @Test
   public void shouldReturnIplaOKWhenTranslate() {

     // prepara el test
     String input = "20";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_IPLA_OK", result);
   }
   
   @Test
   public void shouldReturnIplaKOWhenTranslate() {

     // prepara el test
     String input = "21";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_IPLA_KO", result);
   }
   
   @Test
   public void shouldReturnMftOKWhenTranslate() {

     // prepara el test
     String input = "22";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_MFT_OK", result);
   }
   
   @Test
   public void shouldReturnMftKOWhenTranslate() {

     // prepara el test
     String input = "23";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals("STATE_MFT_KO", result);
   }
   
   @Test
   public void shouldReturnAllWhenTranslate() {

     // prepara el test
     String input = "0";
     
     // invoca metodo a probar
     String result = TranslateStatus.convertIdtoStatus(input);

     // Comprobar resultados
     Assert.assertNotNull(result);
     Assert.assertEquals(" ", result);
   }
   
}
