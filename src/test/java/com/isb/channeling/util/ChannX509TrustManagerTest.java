package com.isb.channeling.util;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.junit.Assert;
import org.junit.Test;

public class ChannX509TrustManagerTest {

  ChannX509TrustManager manager = new ChannX509TrustManager();

  @Test
  public void shouldReturnNotEmptyIssuer() {

    // prepara el test

    // invoca metodo a probar
    X509Certificate[] result = manager.getAcceptedIssuers();

    // Comprobar resultados
    Assert.assertNotNull(result);
  }

  @Test(expected = CertificateException.class)
  public void checkClientTrustedKO() throws CertificateException {

    // prepara el test

    // invoca metodo a probar
    manager.checkClientTrusted(null, null);

    // Comprobar resultados
  }

  @Test(expected = CertificateException.class)
  public void checkServerTrustedKO() throws CertificateException {

    // prepara el test

    // invoca metodo a probar
    manager.checkServerTrusted(null, null);

    // Comprobar resultados
  }

}
