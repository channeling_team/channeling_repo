package com.isb.channeling.util;

import org.junit.Assert;
import org.junit.Test;

public class DateUtilTest {
	

	@Test
	public void convertGMTtoCET() {

		// prepara el test
	  String input = "2014-04-16T16:51:31.195Z";
	  
		// invoca metodo a probar
		String result = DateUtil.convertGMTtoCET(input);

		// Comprobar resultados
		Assert.assertNotNull(result);
	}

	

}
