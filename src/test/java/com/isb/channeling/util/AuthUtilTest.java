package com.isb.channeling.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;

import org.apache.commons.codec.binary.Base64;
import org.junit.Assert;
import org.junit.Test;

public class AuthUtilTest {

  private static final String TOKEN = "QjQ2NTc5NDJFNTM4RTQwMTMyMkI4M0M5IzE4MC4xMDEuMTE2LjkjMTUwNDA4NjMyMDkyOSNQRDk0Yld3Z2RtVnljMmx2YmowaU1TNHdJaUJsYm1OdlpHbHVaejBpU1ZOUExUZzROVGt0TVNJL1BqeDBiMnRsYmtSbFptbHVhWFJwYjI0K1BHeHZZMkZzUlcxcGRIUmxjajVUVTBORFFqd3ZiRzlqWVd4RmJXbDBkR1Z5UGp4MWMyVnlTVVErZUVsVE1USTNORGM4TDNWelpYSkpSRDQ4Ym1GdFpUNUZVMUJCVWxwQklFUkpRVm9zSUZOUFRFVkVRVVFnUWtWTVJVNDhMMjVoYldVK1BHRnNhV0Z6UG5oSlV6RXlOelEzUEM5aGJHbGhjejQ4ZFhObGNrTnZjbkErZUVsVE1USTNORGM4TDNWelpYSkRiM0p3UGp3dmRHOXJaVzVFWldacGJtbDBhVzl1UGc9PSNERVNlZGUvQ0JDL1BLQ1M1UGFkZGluZyN2MSNDb3JwSW50cmFuZXQjTk9UIFVTRUQjU0hBMXdpdGhSU0EjQ09pRkJHdkhOVnRGQ0pFRWN5blN1czNtTmh0enNLYmZsVkRmWUN5RkpuaW9GSFIxbmNRcVpCVEd0N1IxSkJhcStCc25ucGhGZFZYenFoWmdZN3d4N21wYTFDWmVDakVjcVViR2NHNUx4UEFKdHkyVXlqd1NmYkR2bE9QaWtxZXBwbzNFSnltbWg0THJ1NUJOdW1BSDQ5RHM3M3V1YXVhWFRjZGpEZUxrM0lJPQ==";

  private static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCWed2Kvl2NIllDbN99PYpizdg1Vxn3fcn2GWBEi+zcnebPQQwrxpb81uxs4/IkVzarubSu1uoI7nl/RAkud5B3jhA3styPGK8DN2UE+gYW72oziHC5IEnlMUEaFqrCduXHVc66/xLjDDzZ/fttijM9Wj6b9gMPE0RyZo4I5YBcEwIDAQAB";

  private static final String COOKIE = "B4657942E538470132814004#180.101.116.9#1504083476270#1680000#1504081860270#64000#PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iSVNPLTg4NTktMSI/Pjxjb29raWU+PGRlZmluaXRpb25OYW1lPk5ld1VzZXJQYXNzd29yZENvb2tpZTwvZGVmaW5pdGlvbk5hbWU+PGxvY2FsRW1pdHRlcj5TU0NDQjwvbG9jYWxFbWl0dGVyPjx1c2VySUQ+eElTMTI3NDc8L3VzZXJJRD48bmFtZT5FU1BBUlpBIERJQVosIFNPTEVEQUQgQkVMRU48L25hbWU+PGFsaWFzPnhJUzEyNzQ3PC9hbGlhcz48dXNlckNvcnA+eElTMTI3NDc8L3VzZXJDb3JwPjwvY29va2llPg==#DESede/CBC/PKCS5Padding#v1#CorpIntranet#NOT_USED#SHA1withRSA#f9K+QrmKFlDB7Rc7TIAHJag+ha/quY+xT6ompNFlnozpQuxZFPoJHqMOoDCmQZ7lhX6EVOMR50pDiU3rlJlEdMJcxkveNO02NRo5Y8YcBMxRsM22NZkw39gzqVknEWKjpKD4P97uHRMRTkkcTMuvXFNxMeib5rh71wyh2aWqukU=";

  @Test
  public void hasExpiredOK() {

    // prepara el test
    final String decodedToken = new String(Base64.decodeBase64(TOKEN));

    // invoca metodo a probar
    boolean result = AuthUtil.hasExpired(decodedToken);

    // Comprobar resultados
    Assert.assertNotNull(result);
  }

  @Test
  public void getLdapSearchKeyNull() {
    // prepara el test
    String token = null;

    // invoca metodo a probar
    String result1 = AuthUtil.getLdapSearchKeyFromToken(token);
    String result2 = AuthUtil.getLdapSearchKeyFromCookie(token);

    // Comprobar resultados
    Assert.assertNull(result1);
    Assert.assertNull(result2);
  }

  @Test
  public void getLdapSearchKeyOK() {
    // prepara el test
    final String decodedToken = new String(Base64.decodeBase64(TOKEN));

    // invoca metodo a probar
    String result1 = AuthUtil.getLdapSearchKeyFromToken(decodedToken);
    String result2 = AuthUtil.getLdapSearchKeyFromCookie(COOKIE);

    // Comprobar resultados
    Assert.assertNotNull(result1);
    Assert.assertNotNull(result2);
  }

  @Test
  public void tokenIsInValidOK() throws Exception {
    // prepara el test
    final String decodedToken = new String(Base64.decodeBase64(TOKEN));

    // invoca metodo a probar
    boolean result = false;
    result = AuthUtil.tokenIsInValid(decodedToken, Base64.decodeBase64(PUBLIC_KEY));

    // Comprobar resultados
    Assert.assertNotNull(result);
  }

  @Test
  public void getUserNameOK() {
    // prepara el test
    final String decodedToken = new String(Base64.decodeBase64(TOKEN));

    // invoca metodo a probar
    String result1 = AuthUtil.getUserNameFromToken(decodedToken);
    String result2 = AuthUtil.getUserNameFromCookie(COOKIE);

    // Comprobar resultados
    Assert.assertNotNull(result1);
    Assert.assertNotNull(result2);
  }
}
