package com.isb.channeling.service.impl;

import static org.mockito.Matchers.any;

import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.w3c.dom.Document;

import Customer.ListarClientesRequest;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ URL.class, SOAPConnectionFactory.class, SOAPConnection.class })
public class SoapServiceTest {

  private String sUrl = "http://somer.url";

  @Test
  public void shouldReturnSoapMessageWhenConnectToSterling() throws Exception {
    // prepara el test
    ListarClientesRequest request = new ListarClientesRequest();

    Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    Marshaller marshaller = JAXBContext.newInstance(ListarClientesRequest.class).createMarshaller();
    marshaller.marshal(request, document);
    SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
    sRequest.getSOAPBody().addDocument(document);

    URL mockURL = PowerMockito.mock(URL.class);
    SOAPConnection connection = PowerMockito.mock(SOAPConnection.class);
    PowerMockito.mockStatic(SOAPConnectionFactory.class);
    SOAPConnectionFactory soapInstance = PowerMockito.mock(SOAPConnectionFactory.class);
    PowerMockito.whenNew(URL.class).withArguments(sUrl).thenReturn(mockURL);
    PowerMockito.when(SOAPConnectionFactory.newInstance()).thenReturn(soapInstance);
    PowerMockito.when(soapInstance.createConnection()).thenReturn(connection);
    PowerMockito.when(connection.call(any(), any())).thenReturn(sRequest);

    // invoca metodo a probar
    SOAPMessage response = SoapService.talk(sUrl, sRequest);

    // Comprobar resultados
    Assert.assertNotNull(response);
  }

  @Test
  public void shouldNullWithNullRequest() throws Exception {
    // prepara el test

    // invoca metodo a probar
    SOAPMessage response = SoapService.talk(sUrl, null);

    // Comprobar resultados
    Assert.assertNull(response);
  }

  @Test
  public void shouldNullWithNullUrl() throws Exception {
    // prepara el test
    ListarClientesRequest request = new ListarClientesRequest();

    Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    Marshaller marshaller = JAXBContext.newInstance(ListarClientesRequest.class).createMarshaller();
    marshaller.marshal(request, document);
    SOAPMessage sRequest = MessageFactory.newInstance().createMessage();
    sRequest.getSOAPBody().addDocument(document);

    // invoca metodo a probar
    SOAPMessage response = SoapService.talk(null, sRequest);

    // Comprobar resultados
    Assert.assertNull(response);
  }

}
