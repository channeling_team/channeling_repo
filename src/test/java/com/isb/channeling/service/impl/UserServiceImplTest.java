package com.isb.channeling.service.impl;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import User.EditarUsuariosRequest;
import User.EliminarUsuarioRequest;
import User.ListarUsuariosRequest;
import User.ListarUsuariosResponse.User;
import User.NuevoUsuarioRequest;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ JAXBContext.class, DocumentBuilderFactory.class })
public class UserServiceImplTest {

  private SOAPMessage soapResponse = Mockito.mock(SOAPMessage.class);

  private SOAPBody soapBody = Mockito.mock(SOAPBody.class);

  private UserServiceImpl service;

  @Before
  public void init() throws SOAPException {

    service = Mockito.spy(new UserServiceImpl());

    Mockito.doReturn(soapBody).when(soapResponse).getSOAPBody();

    Mockito.doReturn(soapResponse).when(service).callService(Matchers.any(SOAPMessage.class),
        Matchers.any(String.class));

    Authentication authentication = Mockito.mock(Authentication.class);
    // Mockito.whens() for your authorization object
    SecurityContext securityContext = Mockito.mock(SecurityContext.class);
    Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);

  }

  private Document buildDocument(String response) throws ParserConfigurationException, SAXException, IOException {

    final DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    final InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(response));
    final Document document = documentBuilder.parse(is);

    return document;
  }

  @Test
  public void shouldReturnListUsersWhenSearch()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    List<User> userList = new ArrayList<>();
    Mockito
        .doReturn(buildDocument(
            "<ListarUsuariosResponse><User><Id>1</Id><Role>Admin</Role><Name>SP6EDU</Name><Email>ADMIN@email.com</Email><BirthName>Administrator</BirthName><LastName>SP6EDU</LastName><CreationDate>21/03/2018</CreationDate><LastLoginDate>21/03/2018</LastLoginDate></User><User><Id>12</Id><Role>User</Role><Name>User2</Name><Email>User2@email.com</Email><BirthName>Name User</BirthName><LastName>Last Name</LastName><CreationDate>21/03/2018</CreationDate><LastLoginDate>21/03/2018</LastLoginDate></User><User><Id>3</Id><Role>User</Role><Name>User3</Name><Email>User3@email.com</Email><BirthName>Name User</BirthName><LastName>Last Name</LastName><CreationDate>21/03/2018</CreationDate><LastLoginDate>21/03/2018</LastLoginDate></User></ListarUsuariosResponse>"))
        .when(soapBody).extractContentAsDocument();

    // invoca metodo a probar
    userList = service.findAllUsers();

    // Comprobar resultados
    Assert.assertNotNull(userList);
    Assert.assertNotNull(userList.get(0).getId());
    Assert.assertNotNull(userList.get(0).getRole());
    Assert.assertNotNull(userList.get(0).getName());
    Assert.assertNotNull(userList.get(0).getEmail());
    Assert.assertNotNull(userList.get(0).getBirthName());
    Assert.assertNotNull(userList.get(0).getLastName());
    Assert.assertNotNull(userList.get(0).getCreationDate());
    Assert.assertNotNull(userList.get(0).getLastLoginDate());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void shouldReturnSOAPExceptionWhenSearch() throws SOAPException {

    // prepara el test
    List<User> userList = new ArrayList<>();
    Mockito.when(soapBody.extractContentAsDocument()).thenThrow(SOAPException.class);

    // invoca metodo a probar
    service.findAllUsers();

    // Comprobar resultados
    Assert.assertNotNull(userList);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnParserConfigurationExceptionWhenSearch() throws ParserConfigurationException {

    // prepara el test
    List<User> userList = new ArrayList<>();
    PowerMockito.mockStatic(DocumentBuilderFactory.class);
    PowerMockito.when(DocumentBuilderFactory.newInstance()).thenThrow(ParserConfigurationException.class);

    // invoca metodo a probar
    service.findAllUsers();

    // Comprobar resultados
    Assert.assertNotNull(userList);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnJAXBExceptionWhenSearch() throws JAXBException {

    // prepara el test
    List<User> userList = new ArrayList<>();
    PowerMockito.mockStatic(JAXBContext.class);
    PowerMockito.when(JAXBContext.newInstance(ListarUsuariosRequest.class)).thenThrow(JAXBException.class);

    // invoca metodo a probar
    service.findAllUsers();

    // Comprobar resultados
    Assert.assertNotNull(userList);
  }

  @Test
  public void shouldReturnUsersWhenFindByName()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    User user = new User();
    String userName = "User1";
    Mockito
        .doReturn(buildDocument(
            "<ListarUsuariosResponse><User><Id>1</Id><Role>Admin</Role><Name>SP6EDU</Name><Email>ADMIN@email.com</Email><BirthName>Administrator</BirthName><LastName>SP6EDU</LastName><CreationDate>21/03/2018</CreationDate><LastLoginDate>21/03/2018</LastLoginDate></User></ListarUsuariosResponse>"))
        .when(soapBody).extractContentAsDocument();

    // invoca metodo a probar
    user = service.findByName(userName);

    // Comprobar resultados
    Assert.assertNotNull(user);
    Assert.assertNotNull(user.getId());
    Assert.assertNotNull(user.getName());
  }
  
  @Test
  public void shouldReturnEmptyWhenFindByName()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    User user = new User();
    String userName = "User1";
    Mockito
        .doReturn(buildDocument(
            "<ListarUsuariosResponse></ListarUsuariosResponse>"))
        .when(soapBody).extractContentAsDocument();

    // invoca metodo a probar
    user = service.findByName(userName);

    // Comprobar resultados
    Assert.assertNull(user);
//    Assert.assertNotNull(user.getId());
//    Assert.assertNotNull(user.getName());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void shouldReturnSOAPExceptionWhenFindByName() throws SOAPException {

    // prepara el test
    User user = new User();
    String userName = "User1";
    Mockito.when(soapBody.extractContentAsDocument()).thenThrow(SOAPException.class);

    // invoca metodo a probar
    service.findByName(userName);

    // Comprobar resultados
    Assert.assertNotNull(user);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnParserConfigurationExceptionWhenFindByName() throws ParserConfigurationException {

    // prepara el test
    User user = new User();
    String userName = "User1";
    PowerMockito.mockStatic(DocumentBuilderFactory.class);
    PowerMockito.when(DocumentBuilderFactory.newInstance()).thenThrow(ParserConfigurationException.class);

    // invoca metodo a probar
    service.findByName(userName);

    // Comprobar resultados
    Assert.assertNotNull(user);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnJAXBExceptionWhenFindByName() throws JAXBException {

    // prepara el test
    User user = new User();
    String userName = "User1";
    PowerMockito.mockStatic(JAXBContext.class);
    PowerMockito.when(JAXBContext.newInstance(ListarUsuariosRequest.class)).thenThrow(JAXBException.class);

    // invoca metodo a probar
    service.findByName(userName);

    // Comprobar resultados
    Assert.assertNotNull(user);
  }

  @Test
  public void shouldReturnIdWhenCreateUser()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    NuevoUsuarioRequest nuevoUsuario = new NuevoUsuarioRequest();
    nuevoUsuario.setAudUser("WEB");
    nuevoUsuario.setEmail("correo@gmail.com");
    nuevoUsuario.setName("nombre");
    nuevoUsuario.setRole("ADMIN");
    nuevoUsuario.setBirthName("birth name");
    nuevoUsuario.setLastName("last name");
    
    
    Mockito.doReturn(buildDocument("<NuevoUsuarioResponse><Rsp>10</Rsp></NuevoUsuarioResponse>")).when(soapBody)
        .extractContentAsDocument();

    // invoca metodo a probar
    String id = service.createUser(nuevoUsuario);

    // Comprobar resultados
    Assert.assertEquals("10", id);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void shouldReturnSOAPExceptionWhenCreateUser() throws SOAPException {

    // prepara el test
    NuevoUsuarioRequest nuevoUsuario = new NuevoUsuarioRequest();
    nuevoUsuario.setAudUser("WEB");
    nuevoUsuario.setEmail("correo@gmail.com");
    nuevoUsuario.setName("nombre");
    nuevoUsuario.setRole("ADMIN");
    nuevoUsuario.setBirthName("birth name");
    nuevoUsuario.setLastName("last name");
    Mockito.when(soapBody.extractContentAsDocument()).thenThrow(SOAPException.class);

    // invoca metodo a probar
    service.createUser(nuevoUsuario);

    // Comprobar resultados
    Assert.assertNotNull(nuevoUsuario);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnParserConfigurationExceptionWhenCreateUser() throws ParserConfigurationException {

    // prepara el test
    NuevoUsuarioRequest nuevoUsuario = new NuevoUsuarioRequest();
    nuevoUsuario.setAudUser("WEB");
    nuevoUsuario.setEmail("correo@gmail.com");
    nuevoUsuario.setName("nombre");
    nuevoUsuario.setRole("ADMIN");
    nuevoUsuario.setBirthName("birth name");
    nuevoUsuario.setLastName("last name");
    PowerMockito.mockStatic(DocumentBuilderFactory.class);
    PowerMockito.when(DocumentBuilderFactory.newInstance()).thenThrow(ParserConfigurationException.class);

    // invoca metodo a probar
    service.createUser(nuevoUsuario);

    // Comprobar resultados
    Assert.assertNotNull(nuevoUsuario);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnJAXBExceptionWhenCreateUser() throws JAXBException {

    // prepara el test
    NuevoUsuarioRequest nuevoUsuario = new NuevoUsuarioRequest();
    nuevoUsuario.setAudUser("WEB");
    nuevoUsuario.setEmail("correo@gmail.com");
    nuevoUsuario.setName("nombre");
    nuevoUsuario.setRole("ADMIN");
    nuevoUsuario.setBirthName("birth name");
    nuevoUsuario.setLastName("last name");
    PowerMockito.mockStatic(JAXBContext.class);
    PowerMockito.when(JAXBContext.newInstance(NuevoUsuarioRequest.class)).thenThrow(JAXBException.class);

    // invoca metodo a probar
    service.createUser(nuevoUsuario);

    // Comprobar resultados
    Assert.assertNotNull(nuevoUsuario);
  }

  @Test
  public void shouldReturnIdWhenUpdateUser()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    EditarUsuariosRequest usuarioEditado = new EditarUsuariosRequest();
    usuarioEditado.setAudUser("WEB");
    usuarioEditado.setEmail("correo@gmail.com");
    usuarioEditado.setName("nombre");
    usuarioEditado.setRole("ADMIN");
    usuarioEditado.setBirthName("birth name");
    usuarioEditado.setLastName("last name");

    Mockito.doReturn(buildDocument("<EditarUsuariosResponse><Rsp>Ok</Rsp></EditarUsuariosResponse>")).when(soapBody)
        .extractContentAsDocument();

    // invoca metodo a probar
    String id = service.updateUser(usuarioEditado);

    // Comprobar resultados
    Assert.assertEquals("Ok", id);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void shouldReturnSOAPExceptionWhenUpdateUser() throws SOAPException {

    // prepara el test
    EditarUsuariosRequest usuarioEditado = new EditarUsuariosRequest();
    usuarioEditado.setAudUser("WEB");
    usuarioEditado.setEmail("correo@gmail.com");
    usuarioEditado.setName("nombre");
    usuarioEditado.setRole("ADMIN");
    usuarioEditado.setBirthName("birth name");
    usuarioEditado.setLastName("last name");
    Mockito.when(soapBody.extractContentAsDocument()).thenThrow(SOAPException.class);

    // invoca metodo a probar
    service.updateUser(usuarioEditado);

    // Comprobar resultados
    Assert.assertNotNull(usuarioEditado);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnParserConfigurationExceptionWhenUpdateUser() throws ParserConfigurationException {

    // prepara el test
    EditarUsuariosRequest usuarioEditado = new EditarUsuariosRequest();
    usuarioEditado.setAudUser("WEB");
    usuarioEditado.setEmail("correo@gmail.com");
    usuarioEditado.setName("nombre");
    usuarioEditado.setRole("ADMIN");
    usuarioEditado.setBirthName("birth name");
    usuarioEditado.setLastName("last name");
    PowerMockito.mockStatic(DocumentBuilderFactory.class);
    PowerMockito.when(DocumentBuilderFactory.newInstance()).thenThrow(ParserConfigurationException.class);

    // invoca metodo a probar
    service.updateUser(usuarioEditado);

    // Comprobar resultados
    Assert.assertNotNull(usuarioEditado);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnJAXBExceptionWhenUpdateUser() throws JAXBException {

    // prepara el test
    EditarUsuariosRequest usuarioEditado = new EditarUsuariosRequest();
    usuarioEditado.setAudUser("WEB");
    usuarioEditado.setEmail("correo@gmail.com");
    usuarioEditado.setName("nombre");
    usuarioEditado.setRole("ADMIN");
    usuarioEditado.setBirthName("birth name");
    usuarioEditado.setLastName("last name");
    PowerMockito.mockStatic(JAXBContext.class);
    PowerMockito.when(JAXBContext.newInstance(EditarUsuariosRequest.class)).thenThrow(JAXBException.class);

    // invoca metodo a probar
    service.updateUser(usuarioEditado);

    // Comprobar resultados
    Assert.assertNotNull(usuarioEditado);
  }

  @Test
  public void shouldDeleteUserById() throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    Number id = 1;
    Mockito.doReturn(buildDocument("<EliminarUsuarioResponse><Rsp>1</Rsp></EliminarUsuarioResponse>")).when(soapBody)
        .extractContentAsDocument();

    // invoca metodo a probar
    String response = service.deleteUserById(id);

    // Comprobar resultados
    Assert.assertEquals("1", response);
  }
  
  @SuppressWarnings("unchecked")
  @Test
  public void shouldReturnSOAPExceptionWhenDeleteUserById() throws SOAPException {

    // prepara el test
    Number id = 1;
    Mockito.when(soapBody.extractContentAsDocument()).thenThrow(SOAPException.class);

    // invoca metodo a probar
    service.deleteUserById(id);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).deleteUserById(id);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnParserConfigurationExceptionWhenDeleteUserById() throws ParserConfigurationException {

    // prepara el test
    Number id = 1;
    PowerMockito.mockStatic(DocumentBuilderFactory.class);
    PowerMockito.when(DocumentBuilderFactory.newInstance()).thenThrow(ParserConfigurationException.class);

    // invoca metodo a probar
    service.deleteUserById(id);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).deleteUserById(id);
  }

  @SuppressWarnings({ "unchecked" })
  @Test
  public void shouldReturnJAXBExceptionWhenDeleteUserById() throws JAXBException {

    // prepara el test
    Number id = 1;
    PowerMockito.mockStatic(JAXBContext.class);
    PowerMockito.when(JAXBContext.newInstance(EliminarUsuarioRequest.class)).thenThrow(JAXBException.class);

    // invoca metodo a probar
    service.deleteUserById(id);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).deleteUserById(id);
  }

}
