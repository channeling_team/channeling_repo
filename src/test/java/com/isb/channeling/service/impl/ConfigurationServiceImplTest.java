package com.isb.channeling.service.impl;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import Configuration.AltaConfiguracionRequest;
import Configuration.AltaConfiguracionRequest.PGP;
import Configuration.AltaConfiguracionRequest.Renombrado;
import Configuration.EditarConfiguracionRequest;
import Configuration.ListarConfiguracionResponse;

public class ConfigurationServiceImplTest {

  private SOAPMessage soapResponse = Mockito.mock(SOAPMessage.class);

  private SOAPBody soapBody = Mockito.mock(SOAPBody.class);
  // private Document document = Mockito.mock(Document.class);

  private ConfigurationServiceImpl service;

  @Before
  public void init() throws SOAPException {

    service = Mockito.spy(new ConfigurationServiceImpl());
    Mockito.doReturn(soapBody).when(soapResponse).getSOAPBody();
    Mockito.doReturn(soapResponse).when(service).callService(Matchers.any(SOAPMessage.class),
        Matchers.any(String.class));
    Authentication authentication = Mockito.mock(Authentication.class);
    // Mockito.whens() for your authorization object
    SecurityContext securityContext = Mockito.mock(SecurityContext.class);
    Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);
  }

  private Document buildDocument(String response) throws ParserConfigurationException, SAXException, IOException {

    final DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    final InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(response));
    final Document document = documentBuilder.parse(is);

    return document;
  }

  @Test
  public void shouldReturnConfigurationWhenSearchById()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    int id = 1;
    Mockito
        .doReturn(buildDocument(
            "<ListarConfiguracionResponse> <IdConfig>2</IdConfig> <Id_customer>9</Id_customer> <NameConf>INN</NameConf>  <Origen>MFT</Origen> <Destino>GEMC</Destino> <Subalias>INNOXX</Subalias> <TipoDestino>1</TipoDestino> <Formato>XML</Formato> <PGPX>1</PGPX> <S3Key>0</S3Key> <Descripcion>Innovery MT101</Descripcion> <AuduserConf>User1</AuduserConf><Renombrado><Tipo>1</Tipo> <Value> <Orden>1</Orden> <Parametro>Fijo</Parametro> <Valor>abcdefg</Valor> <Separador>-</Separador> </Value>	<Value><Orden>2</Orden>	<Parametro>Secuencial</Parametro> <Valor>1234</Valor> <Separador>.</Separador> </Value> </Renombrado> <PGP> <Tipo_Cifrado>PGP</Tipo_Cifrado> <Tipo_Firma>PGX</Tipo_Firma> <Key>A1B2C3</Key> <Compress>0</Compress><Ascii>0</Ascii> <Text_Mode>0</Text_Mode>  </PGP> </ListarConfiguracionResponse>"))
        .when(soapBody).extractContentAsDocument();

    // invoca metodo a probar
    ListarConfiguracionResponse config = service.findById(id);

    // Comprobar resultados
    Assert.assertEquals("INNOXX", config.getSubalias());
    Assert.assertEquals("GEMC", config.getDestino());
    Assert.assertEquals("MFT", config.getOrigen());
    Assert.assertEquals("INN", config.getNameConf());
    Assert.assertEquals("User1", config.getAuduserConf());
    Assert.assertEquals("Innovery MT101", config.getDescripcion());
    Assert.assertEquals("XML", config.getFormato());
    Assert.assertEquals("2", config.getIdConfig());
    Assert.assertEquals("1", config.getTipoDestino());
    Assert.assertEquals("1", config.getPGPX());

  }

  @Test
  public void shouldReturnConfigurationKOWhenSearchById()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    int id = 1;
    Mockito
        .doReturn(buildDocument(
            "<ListarConfiguracionResponse> <IdConfig>2</IdConfig> <Id_customer>9</Id_customer> <NameConf>INN</NameConf>  <Origen>MFT</Origen> <Destino>GEMC</Destino> <Subalias>INNOXX</Subalias> <TipoDestino>1</TipoDestino> <Formato>XML</Formato> <PGPX>1</PGPX> <S3Key>0</S3Key> <Descripcion>Innovery MT101</Descripcion> <AuduserConf>User1</AuduserConf><Renombrado><Tipo>1</Tipo> <Value> <Orden>1</Orden> <Parametro>Fijo</Parametro> <Valor>abcdefg</Valor> <Separador>-</Separador> </Value>	<Value><Orden>2</Orden>	<Parametro>Secuencial</Parametro> <Valor>1234</Valor> <Separador>.</Separador> </Value> </Renombrado> <PGP> <Tipo_Cifrado>PGP</Tipo_Cifrado> <Tipo_Firma>PGX</Tipo_Firma> <Key>A1B2C3</Key> <Compress>0</Compress><Ascii>0</Ascii> <Text_Mode>0</Text_Mode>  </PGP> </ListarConfiguracionResponse>"))
        .when(soapBody).extractContentAsDocument();

    // invoca metodo a probar
    ListarConfiguracionResponse config = service.findById(id);

    // Comprobar resultados
    Assert.assertNotEquals("Alias2", config.getSubalias());
    Assert.assertNotEquals("MFT2", config.getDestino());
    Assert.assertNotEquals("IPLA2", config.getOrigen());
    Assert.assertNotEquals("Nombre2", config.getNameConf());
  }

  @Test
  public void shouldReturnIdWhenCreateConfig()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    AltaConfiguracionRequest nuevaConfig = new AltaConfiguracionRequest();
    nuevaConfig.setAuduserConf("WEB");
    nuevaConfig.setDescripcion("description");
    nuevaConfig.setDestino("MFT");
    nuevaConfig.setFormato("XML");
    nuevaConfig.setIdCustomer("9");
    nuevaConfig.setPGPX("0");
    nuevaConfig.setPGP(new PGP());
    nuevaConfig.setRenombrado(new Renombrado());
    nuevaConfig.getRenombrado().setTipo("0");

    Mockito.doReturn(buildDocument("<AltaConfiguracionResponse><Rsp>3</Rsp></AltaConfiguracionResponse>"))
        .when(soapBody).extractContentAsDocument();

    // invoca metodo a probar
    String id = service.createConfiguration(nuevaConfig);

    // Comprobar resultados
    Assert.assertEquals("3", id);
  }

  @Test
  public void shouldReturnKoWhenCreateConfig()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    AltaConfiguracionRequest nuevaConfig = new AltaConfiguracionRequest();
    nuevaConfig.setAuduserConf("WEB");
    nuevaConfig.setDescripcion("description");
    nuevaConfig.setDestino("MFT");
    nuevaConfig.setFormato("XML");
    nuevaConfig.setIdCustomer("9");
    nuevaConfig.setPGPX("0");
    nuevaConfig.setPGP(new PGP());
    nuevaConfig.setRenombrado(new Renombrado());
    nuevaConfig.getRenombrado().setTipo("0");

    Mockito.doReturn(buildDocument("<AltaConfiguracionResponse><Rsp>KO</Rsp></AltaConfiguracionResponse>"))
        .when(soapBody).extractContentAsDocument();

    // invoca metodo a probar
    String id = service.createConfiguration(nuevaConfig);

    // Comprobar resultados
    Assert.assertEquals("KO", id);
  }

  @Test
  public void shouldReturnIdWhenUpdateConfig()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    EditarConfiguracionRequest editConfig = new EditarConfiguracionRequest();
    editConfig.setAuduserConf("WEB");
    editConfig.setDescripcion("description");
    editConfig.setDestino("MFT");
    editConfig.setFormato("XML");
    editConfig.setIdCustomer("9");
    editConfig.setIdConfig("3");
    editConfig.setPGPX("0");
    editConfig.setRenombrado(new Configuration.EditarConfiguracionRequest.Renombrado());
    editConfig.getRenombrado().setTipo("0");

    Mockito.doReturn(buildDocument("<EditarConfiguracionResponse><Rsp>3</Rsp></EditarConfiguracionResponse>"))
        .when(soapBody).extractContentAsDocument();
    String id = service.updateConfiguration(editConfig);

    // Comprobar resultados
    Assert.assertEquals("3", id);
  }

  @Test
  public void shouldReturnKoWhenUpdateConfig()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    EditarConfiguracionRequest editConfig = new EditarConfiguracionRequest();
    editConfig.setAuduserConf("WEB");
    editConfig.setIdConfig("1");
    editConfig.setDescripcion("description");
    editConfig.setDestino("MFT");
    editConfig.setFormato("XML");
    editConfig.setIdCustomer("9");
    editConfig.setPGPX("0");
    editConfig.setRenombrado(new Configuration.EditarConfiguracionRequest.Renombrado());
    editConfig.getRenombrado().setTipo("1");
    editConfig.getRenombrado().getValue();

    Mockito.doReturn(buildDocument("<EditarConfiguracionResponse><Rsp>KO</Rsp></EditarConfiguracionResponse>"))
        .when(soapBody).extractContentAsDocument();
    String id = service.updateConfiguration(editConfig);

    // Comprobar resultados
    Assert.assertEquals("KO", id);
  }

  @Test
  public void shouldDeleteConfigurationById()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    int id = 1;
    Mockito.doReturn(buildDocument("<EliminarConfiguracionResponse><Rsp>Ok</Rsp></EliminarConfiguracionResponse>"))
        .when(soapBody).extractContentAsDocument();

    // invoca metodo a probar
    String response = service.deleteConfigurationById(id);

    // Comprobar resultados
    Assert.assertEquals("Ok", response);
  }

  @Test
  public void shouldDeleteKoConfigurationById()
      throws SOAPException, ParserConfigurationException, SAXException, IOException {

    // prepara el test
    int id = 1;
    Mockito.doReturn(buildDocument("<EliminarConfiguracionResponse><Rsp>KO</Rsp></EliminarConfiguracionResponse>"))
        .when(soapBody).extractContentAsDocument();

    // invoca metodo a probar
    String response = service.deleteConfigurationById(id);

    // Comprobar resultados
    Assert.assertEquals("KO", response);
  }
}
