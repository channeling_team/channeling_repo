package com.isb.channeling.service.impl;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import Customer.EditarClienteRequest;
import Customer.ListarClienteConfigResponse;
import Customer.ListarClientesResponse.Customer;
import Customer.NuevoClienteRequest;

public class CustomerServiceImplTest {

	private SOAPMessage soapResponse = Mockito.mock(SOAPMessage.class);

	private SOAPBody soapBody = Mockito.mock(SOAPBody.class);

	private CustomerServiceImpl service;

	@Before
	public void init() throws SOAPException {

		service = Mockito.spy(new CustomerServiceImpl());

		Mockito.doReturn(soapBody).when(soapResponse).getSOAPBody();

		Mockito.doReturn(soapResponse).when(service).callService(Matchers.any(SOAPMessage.class),
		    Matchers.any(String.class));
		
		Authentication authentication = Mockito.mock(Authentication.class);
		// Mockito.whens() for your authorization object
		SecurityContext securityContext = Mockito.mock(SecurityContext.class);
		Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
	}

	private Document buildDocument(String response) throws ParserConfigurationException, SAXException, IOException {

		final DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		final InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(response));
		final Document document = documentBuilder.parse(is);

		return document;
	}

	@Test
	public void shouldReturnListCustomerWhenSearch()
	    throws SOAPException, ParserConfigurationException, SAXException, IOException {

		// prepara el test
		Mockito
		    .doReturn(buildDocument(
		        "<ListarClientesResponse><Customer><Id>1</Id><Alias>SANTELEFONX</Alias><BIC>TELEFONICA1</BIC><Name>TELEFONICA</Name>            <Descripcion>CLIENTE TELEFONICA</Descripcion><Auduser>USER2</Auduser><Audate>2017-06-19</Audate></Customer><Customer><Id>1</Id><Alias>AAA</Alias><BIC>aaa</BIC><Name>AaAaAaA</Name><Descripcion>AAAAAAA</Descripcion><Auduser>User1</Auduser><Audate>2017-06-22</Audate></Customer><Customer><Id>1</Id><Alias>MERCADONAXX</Alias><BIC>MERCAD</BIC><Name>MERCADONA</Name><Descripcion>Cliente Mercadona</Descripcion> <Auduser>User1</Auduser><Audate>2017-06-22</Audate></Customer>  <Customer> <Id>1</Id> <Alias>MERCADONAXX</Alias> <BIC>ALCAMPOXXXX</BIC> <Name>ALCAMPO</Name> <Descripcion>Cliente Alcampo</Descripcion> <Auduser>User1</Auduser> <Audate>2017-06-23</Audate></Customer> <Customer><Id>1</Id> <Alias>ss</Alias> <BIC>ss</BIC> <Name>ss</Name><Descripcion>ss</Descripcion>  <Auduser>s</Auduser> <Audate>2017-06-23</Audate> </Customer></ListarClientesResponse>"))
		    .when(soapBody).extractContentAsDocument();

		// invoca metodo a probar
		List<Customer> config = service.findAllCustomers();

		// Comprobar resultados
		Assert.assertNotNull(config);
		Assert.assertNotNull(config.get(0).getId());
		Assert.assertNotNull(config.get(0).getAlias());
		Assert.assertNotNull(config.get(0).getName());

	}

	@Test
	public void shouldReturnIdWhenCreateCustomer()
	    throws SOAPException, ParserConfigurationException, SAXException, IOException {

		// prepara el test
		NuevoClienteRequest nuevoCliente = new NuevoClienteRequest();
		nuevoCliente.setAudUser("WEB");
		nuevoCliente.setBic("");
		nuevoCliente.setName("Prueba");
		nuevoCliente.setAlias("alias");
		nuevoCliente.setDescripcion("ejemplo de prueba");

		Mockito.doReturn(buildDocument("<NuevoClienteResponse><Rsp>7</Rsp></NuevoClienteResponse>")).when(soapBody)
		    .extractContentAsDocument();

		// invoca metodo a probar
		String id = service.createCustomer(nuevoCliente);

		// Comprobar resultados
		Assert.assertEquals("7", id);
	}

	@Test
	public void shouldReturnIdWhenUpdateCustomer()
	    throws SOAPException, ParserConfigurationException, SAXException, IOException {

		// prepara el test
		EditarClienteRequest editCliente = new EditarClienteRequest();
		editCliente.setAudUser("WEB");
		editCliente.setId("1");
		editCliente.setName("Prueba");
		editCliente.setAlias("alias");
		editCliente.setDescripcion("ejemplo de prueba");

		Mockito.doReturn(buildDocument("<EditarClienteResponse><Rsp>1</Rsp></EditarClienteResponse>")).when(soapBody)
		    .extractContentAsDocument();

		// invoca metodo a probar
		String id = service.updateCustomer(editCliente);

		// Comprobar resultados
		Assert.assertEquals("1", id);
	}

	@Test
	public void shouldDeleteConfigurationById()
	    throws SOAPException, ParserConfigurationException, SAXException, IOException {

		// prepara el test
		int id = 1;
		Mockito.doReturn(buildDocument("<EliminarClienteResponse><Rsp>1</Rsp></EliminarClienteResponse>")).when(soapBody)
		    .extractContentAsDocument();

		// invoca metodo a probar
		String response = service.deleteCustomerById(id);

		// Comprobar resultados
		Assert.assertEquals("1", response);
	}

	@Test
	public void shouldListCustomerConfigsByIdCustomer()
	    throws SOAPException, ParserConfigurationException, SAXException, IOException {

		// prepara el test
		int id = 9;
		Mockito
		    .doReturn(buildDocument(
		        "<ListarClienteConfigResponse><Customer><Id>9</Id><Alias>INNOVERY</Alias><Bic>INNOXXXX</Bic><Name>Innovery S.L.</Name><Descripcion>bla bla bla</Descripcion><Auduser>User1</Auduser><Audate>2017-06-23</Audate><Config><IdConfig>15</IdConfig><NameConf>INNOVERY</NameConf><Origen>MFTT</Origen><Destino>GEMC</Destino><SubAlias>INNOVX</SubAlias><Formato>PAIN08</Formato><AuduserConf>User2</AuduserConf><AudateConf>2017-06-27</AudateConf></Config><Config><IdConfig>16</IdConfig><NameConf>INN</NameConf><Origen>MFT</Origen><Destino>GEMC</Destino><SubAlias>INNOXX</SubAlias><Formato>XML</Formato><AuduserConf>User1</AuduserConf><AudateConf>2017-06-26</AudateConf></Config></Customer></ListarClienteConfigResponse>"))
		    .when(soapBody).extractContentAsDocument();

		// invoca metodo a probar
		ListarClienteConfigResponse.Customer listaConfiguraciones = service.findById(id);

		// Comprobar resultados
		Assert.assertNotNull(listaConfiguraciones);

	}
}
