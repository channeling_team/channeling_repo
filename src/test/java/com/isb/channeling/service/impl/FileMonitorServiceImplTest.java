package com.isb.channeling.service.impl;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import FileMonitor.BuscarFicheroRequest;
import FileMonitor.BuscarFicheroResponse.Fichero;


public class FileMonitorServiceImplTest {

	private SOAPMessage soapResponse = Mockito.mock(SOAPMessage.class);
	private SOAPBody soapBody = Mockito.mock(SOAPBody.class);

	private FileMonitorServiceImpl service;

	@Before
	public void init() throws SOAPException {

		service = Mockito.spy(new FileMonitorServiceImpl());

		Mockito.doReturn(soapBody).when(soapResponse).getSOAPBody();

		Mockito.doReturn(soapResponse).when(service).callService(Matchers.any(SOAPMessage.class),
				Matchers.any(String.class));

	}

	private Document buildDocument(String response) throws ParserConfigurationException, SAXException, IOException {

		final DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		final InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(response));
		final Document document = documentBuilder.parse(is);

		return document;
	}

	
	@Test
	public void shouldReturnListUsersWhenSearch()
			throws SOAPException, ParserConfigurationException, SAXException, IOException {

		// prepara el test
		List<Fichero> fileList = new ArrayList<>();
		BuscarFicheroRequest fileSearch = new BuscarFicheroRequest();
		Mockito.doReturn(buildDocument(
				"<BuscarFicheroResponse><Fichero><Id>1</Id><NombreOriginal>Nombre Original</NombreOriginal><Renombrado>file_renombrado</Renombrado><Fecha>28-06-2017</Fecha><Cliente>1</Cliente><Canal>1</Canal><Formato>1</Formato><Tipo>1</Tipo><Estado>1</Estado></Fichero><Fichero><Id>2</Id><NombreOriginal>Nombre Original</NombreOriginal><Renombrado>file_renombrado</Renombrado><Fecha>28-06-2017</Fecha><Cliente>1</Cliente><Canal>1</Canal><Formato>1</Formato><Tipo>1</Tipo><Estado>1</Estado></Fichero></BuscarFicheroResponse>"))
				.when(soapBody).extractContentAsDocument();

		// invoca metodo a probar
		fileList = service.search(fileSearch);

		// Comprobar resultados
		Assert.assertNotNull(fileList);
		Assert.assertEquals(2, fileList.size());
	}
	
	 @Test
	  public void shouldReturnListWhenCheckIsEmpty() {

	    // prepara el test
	    List<Fichero> fileList = new ArrayList<>();
	    Fichero fich = new Fichero();
	    fich.setId("0");
	    fich.setRenombrado("0");
	    fich.setNombreOriginal("0");
	    fich.setFormato("0");
	    fich.setEstado("0");
	    fileList.add(fich);
	    
	    Fichero fich1 = new Fichero();
      fich1.setId("1");
      fich1.setRenombrado("1");
      fich.setNombreOriginal("0");
      fich1.setFormato("1");
      fich1.setEstado("1");
      fileList.add(fich1);

	    // invoca metodo a probar
	    fileList = service.isEmpty(fileList);

	    // Comprobar resultados
	    Assert.assertEquals(1, fileList.size());
	  }
}
