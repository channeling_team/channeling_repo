package com.isb.channeling.controller;

import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.service.ConfigurationService;
import com.isb.channeling.service.CustomerService;
import com.isb.channeling.service.impl.ConfigurationServiceImpl;
import com.isb.channeling.service.impl.CustomerServiceImpl;

import Configuration.AltaListaConfiguracionesResponse;
import Configuration.AltaListaConfiguracionesResponse.ConfigDup;
import Customer.EditarClienteCompleteConfigRequest;
import Customer.EditarClienteCompleteConfigRequest.Config;
import Customer.EditarClienteCompleteConfigRequest.Config.PGP;
import Customer.EditarClienteCompleteConfigRequest.Config.Renombrado;
import Customer.EditarClienteCompleteConfigRequest.Config.Renombrado.Value;
import Customer.EditarClienteRequest;
import Customer.ListarClienteCompleteConfigResponse;
import Customer.ListarClienteConfigResponse;
import Customer.ListarClientesResponse.Customer;
import Customer.NuevoClienteRequest;

public class CustomerControllerTest {

  private CustomerController controller;

  private CustomerService service;

  private ConfigurationService cService;

  @Before
  public void init() {
    this.controller = new CustomerController();
    service = Mockito.mock(CustomerServiceImpl.class);
    cService = Mockito.mock(ConfigurationServiceImpl.class);
    controller.setCustomerService(service);
    controller.setConfigurationService(cService);
    
    Authentication authentication = Mockito.mock(Authentication.class);
    // Mockito.whens() for your authorization object
    SecurityContext securityContext = Mockito.mock(SecurityContext.class);
    Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);
  }

  @Test
  public void getCustomersWithResults() {

    // prepara el test
    List<Customer> listCustomer = new ArrayList<>();
    Customer customer = new Customer();
    customer.setAlias("vir");
    listCustomer.add(customer);
    Mockito.when(service.findAllCustomers()).thenReturn(listCustomer);

    // invoca metodo a probar
    final JsonResponse<List<Customer>> customers = this.controller.listAllCustomers();

    // Comprobar resultados
    Assert.assertNotNull(customers);
    Assert.assertEquals(1, listCustomer.size());
    Assert.assertEquals("vir", listCustomer.get(0).getAlias());
    Mockito.verify(this.service, Mockito.atLeastOnce()).findAllCustomers();
  }

  @Test
  public void shouldReturnCustomerOK() {

    int id = 1;
    // prepara el test
    ListarClienteConfigResponse.Customer customer = new ListarClienteConfigResponse.Customer();
    customer.setId("1");
    customer.setAlias("alias");
    Mockito.when(service.findById(id)).thenReturn(customer);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<ListarClienteConfigResponse.Customer>> customerAux = this.controller
        .getCustomer(id);

    // Comprobar resultados
    Assert.assertNotNull(customerAux);
    Assert.assertEquals("1", customer.getId());
    Assert.assertEquals(200, customerAux.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).findById(id);

  }

  @Test
  public void shouldReturnCustomerNotFound() {

    int id = 1;
    // prepara el test
    ListarClienteConfigResponse.Customer customer = new ListarClienteConfigResponse.Customer();
    customer.setId("");
    customer.setAlias("");
    Mockito.when(service.findById(id)).thenReturn(customer);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<ListarClienteConfigResponse.Customer>> customerAux = this.controller
        .getCustomer(id);

    // Comprobar resultados
    Assert.assertNotNull(customerAux);
    Assert.assertEquals("CUSTOMER_NOT_EXIST", customerAux.getBody().getError());
    Assert.assertEquals(200, customerAux.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).findById(id);

  }

  @Test
  public void shouldReturnCustomerKO() {

    int id = 1;
    // prepara el test
    ListarClienteConfigResponse.Customer customer = null;
    Mockito.when(service.findById(id)).thenReturn(customer);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<ListarClienteConfigResponse.Customer>> customerAux = this.controller
        .getCustomer(id);

    // Comprobar resultados
    Assert.assertEquals(404, customerAux.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).findById(id);

  }

  @Test
  public void shouldReturnIdCustomerCreateOK() {

    // prepara el test
    String id = "1";
    NuevoClienteRequest customerEdit = new NuevoClienteRequest();
    Mockito.when(service.createCustomer(Matchers.any(NuevoClienteRequest.class))).thenReturn(id);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.createCustomer(customerEdit);

    // Comprobar resultados
    Assert.assertEquals(id, response.getBody().getData());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(Matchers.any(NuevoClienteRequest.class));
  }

  @Test
  public void shouldReturnCustomerCreateKO() {

    // prepara el test
    String id = "KO";
    String error = id + "_CREATE_CUSTOMER";
    NuevoClienteRequest customerEdit = new NuevoClienteRequest();
    Mockito.when(service.createCustomer(Matchers.any(NuevoClienteRequest.class))).thenReturn(id);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.createCustomer(customerEdit);

    // Comprobar resultados
    Assert.assertEquals(error, response.getBody().getError());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(Matchers.any(NuevoClienteRequest.class));
  }

  @Test
  public void shouldReturnUpdateOK() {

    // prepara el test
    int id = 1;
    EditarClienteRequest currentCustomer = new EditarClienteRequest();
    Mockito.when(service.updateCustomer(currentCustomer)).thenReturn("OK");

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.updateCustomer(id, currentCustomer);

    // Comprobar resultados
    Assert.assertEquals("OK", response.getBody().getData());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).updateCustomer(currentCustomer);
  }

  @Test
  public void shouldReturnUpdateKO() {

    // prepara el test
    int id = 1;
    EditarClienteRequest currentCustomer = new EditarClienteRequest();
    Mockito.when(service.updateCustomer(currentCustomer)).thenReturn("0");

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.updateCustomer(id, currentCustomer);

    // Comprobar resultados
    Assert.assertNull("0", response.getBody().getData());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).updateCustomer(currentCustomer);
  }

  @Test
  public void shouldReturnNoContentDeleteOK() {

    // prepara el test
    String sRes = "OK";
    int id = 1;
    Mockito.when(service.deleteCustomerById(id)).thenReturn(sRes);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.deleteCustomer(id);

    // Comprobar resultados
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).deleteCustomerById(id);
  }

  @Test
  public void shouldReturnNoContentDeleteKO() {

    // prepara el test
    String sRes = "0";
    int id = 1;
    Mockito.when(service.deleteCustomerById(id)).thenReturn(sRes);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.deleteCustomer(id);

    // Comprobar resultados
    Assert.assertEquals(204, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).deleteCustomerById(id);
  }

  @Test
  public void shouldReturnCustomerInfoWhenGetCustomerCompleteInfo() {

    // prepara el test
    final ListarClienteCompleteConfigResponse.Customer customer = new ListarClienteCompleteConfigResponse.Customer();
    Number id = 1;
    Mockito.when(service.findCompleteById(id)).thenReturn(customer);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<ListarClienteCompleteConfigResponse.Customer>> response = this.controller
        .getCustomerCompleteInfo(id);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).findCompleteById(id);
    Assert.assertEquals(200, response.getStatusCode().value());
  }

  @Test
  public void shouldReturnDuplicateOK() {

    // prepara el test
    String id = "2";
    EditarClienteCompleteConfigRequest customer = new EditarClienteCompleteConfigRequest();
    customer.setAlias("ALIAS");
    Config config = new Config();
    config.setSubalias("subalias");
    List<Config> listconf = new ArrayList<>();
    listconf.add(config);
    customer.getConfig().addAll(listconf);

    AltaListaConfiguracionesResponse cResponse = new AltaListaConfiguracionesResponse();
    cResponse.setRsp("OK");
    Mockito.when(service.createCustomer(any(NuevoClienteRequest.class))).thenReturn(id);
    Mockito.when(cService.createConfigurationList(any())).thenReturn(cResponse);
    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.duplicateCustomer(customer);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(any(NuevoClienteRequest.class));
    Assert.assertEquals(200, response.getStatusCode().value());
  }
  
  @Test
  public void shouldReturnDuplicateKOSomeConfig() {

    // prepara el test
    String id = "2";
    EditarClienteCompleteConfigRequest customer = new EditarClienteCompleteConfigRequest();
    customer.setAlias("ALIAS");
    Config config = new Config();
    config.setSubalias("subalias");
    List<Config> listconf = new ArrayList<>();
    listconf.add(config);
    customer.getConfig().addAll(listconf);

    AltaListaConfiguracionesResponse cResponse = new AltaListaConfiguracionesResponse();
    cResponse.setRsp("OK");
    ConfigDup dup = new ConfigDup();
    cResponse.setRsp("OK");
    cResponse.setConfigDup(dup);
    Mockito.when(service.createCustomer(any(NuevoClienteRequest.class))).thenReturn(id);
    Mockito.when(cService.createConfigurationList(any())).thenReturn(cResponse);
    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.duplicateCustomer(customer);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(any(NuevoClienteRequest.class));
    Assert.assertEquals(200, response.getStatusCode().value());
  }
  
  @Test
  public void duplicateCustomerShouldFailtoCreateAllConfigurations() {

    // prepara el test
    String id = "2";
    EditarClienteCompleteConfigRequest customer = new EditarClienteCompleteConfigRequest();
    customer.setAlias("ALIAS");
    Config config = new Config();
    config.setSubalias("subalias");
    List<Config> listconf = new ArrayList<>();
    listconf.add(config);
    customer.getConfig().addAll(listconf);

    AltaListaConfiguracionesResponse cResponse = new AltaListaConfiguracionesResponse();
    ConfigDup dup = new ConfigDup();
    dup.setDup("DUPLICADOS");
    cResponse.setRsp("OK");
    cResponse.setConfigDup(dup);
    
    Mockito.when(service.createCustomer(any(NuevoClienteRequest.class))).thenReturn(id);
    Mockito.when(cService.createConfigurationList(any())).thenReturn(cResponse);
    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.duplicateCustomer(customer);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(any(NuevoClienteRequest.class));
    Assert.assertEquals(200, response.getStatusCode().value());
  }

  @Test
  public void shouldReturnDuplicateOKWithPgpAndRenombrado() {

    // prepara el test
    String id = "2";
    EditarClienteCompleteConfigRequest customer = new EditarClienteCompleteConfigRequest();
    customer.setAlias("ALIAS");
    Config config = new Config();
    config.setPGP(new PGP());
    Renombrado renom = new Renombrado();
    renom.getValue().add(new Value());
    config.setRenombrado(renom);
    config.setSubalias("subalias");
    List<Config> listconf = new ArrayList<>();
    listconf.add(config);
    customer.getConfig().addAll(listconf);

    AltaListaConfiguracionesResponse cResponse = new AltaListaConfiguracionesResponse();
    cResponse.setRsp("OK");
    Mockito.when(service.createCustomer(any(NuevoClienteRequest.class))).thenReturn(id);
    Mockito.when(cService.createConfigurationList(any())).thenReturn(cResponse);
    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.duplicateCustomer(customer);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(any(NuevoClienteRequest.class));
    Assert.assertEquals(200, response.getStatusCode().value());
  }

  @Test
  public void shouldReturnDuplicateErrorConfigs() {

    // prepara el test
    String id = "2";
    EditarClienteCompleteConfigRequest customer = new EditarClienteCompleteConfigRequest();
    customer.setAlias("ALIAS");
    Config config = new Config();
    config.setSubalias("subalias");
    List<Config> listconf = new ArrayList<>();
    listconf.add(config);
    customer.getConfig().addAll(listconf);

    AltaListaConfiguracionesResponse cResponse = new AltaListaConfiguracionesResponse();
    cResponse.setRsp("KO");
    Mockito.when(service.createCustomer(any(NuevoClienteRequest.class))).thenReturn(id);
    Mockito.when(cService.createConfigurationList(any())).thenReturn(cResponse);
    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.duplicateCustomer(customer);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(any(NuevoClienteRequest.class));
    Assert.assertEquals(200, response.getStatusCode().value());
  }

  @Test
  public void shouldReturnDuplicateOKWithNoConfigs() {

    // prepara el test
    String id = "2";
    EditarClienteCompleteConfigRequest customer = new EditarClienteCompleteConfigRequest();
    customer.setAlias("ALIAS");
    List<Config> listconf = new ArrayList<>();
    customer.getConfig().addAll(listconf);

    AltaListaConfiguracionesResponse cResponse = new AltaListaConfiguracionesResponse();
    cResponse.setRsp("OK");
    Mockito.when(service.createCustomer(any(NuevoClienteRequest.class))).thenReturn(id);
    Mockito.when(cService.createConfigurationList(any())).thenReturn(cResponse);
    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.duplicateCustomer(customer);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(any(NuevoClienteRequest.class));
    Assert.assertEquals(200, response.getStatusCode().value());
  }
  
  @Test
  public void shouldReturnDuplicateKOWithConfigs() {

    // prepara el test
    String id = "2";
    EditarClienteCompleteConfigRequest customer = new EditarClienteCompleteConfigRequest();
    customer.setAlias("ALIAS");
    List<Config> listconf = new ArrayList<>();
    customer.getConfig().addAll(listconf);

    AltaListaConfiguracionesResponse cResponse = new AltaListaConfiguracionesResponse();
    cResponse.setRsp("OK");
    Mockito.when(service.createCustomer(any(NuevoClienteRequest.class))).thenReturn(id);
    Mockito.when(cService.createConfigurationList(any())).thenReturn(cResponse);
    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.duplicateCustomer(customer);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(any(NuevoClienteRequest.class));
    Assert.assertEquals(200, response.getStatusCode().value());
  }

  @Test
  public void getCustomerCompleteInfoReturnNull() {

    // prepara el test
    final ListarClienteCompleteConfigResponse.Customer customer = new ListarClienteCompleteConfigResponse.Customer();
    Number id = 1;
    Mockito.when(service.findCompleteById(id)).thenReturn(customer);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<ListarClienteCompleteConfigResponse.Customer>> response = this.controller
        .getCustomerCompleteInfo(null);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).findCompleteById(null);
    Assert.assertEquals(404, response.getStatusCode().value());
  }
  
  @Test
  public void duplicateCustomerShouldFailtoCreateCustomer() {
    // prepara el test
    EditarClienteCompleteConfigRequest customer = new EditarClienteCompleteConfigRequest();
    customer.setAlias("ALIAS");
    Config config = new Config();
    config.setSubalias("subalias");
    List<Config> listconf = new ArrayList<>();
    listconf.add(config);
    customer.getConfig().addAll(listconf);
    Mockito.when(service.createCustomer(any(NuevoClienteRequest.class))).thenReturn("KO");

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.duplicateCustomer(customer);

    // Comprobar resultados
    Mockito.verify(this.service, Mockito.atLeastOnce()).createCustomer(any(NuevoClienteRequest.class));
    Assert.assertEquals(200, response.getStatusCode().value());
  }

}
