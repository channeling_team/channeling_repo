package com.isb.channeling.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.service.FileMonitorService;
import com.isb.channeling.service.impl.FileMonitorServiceImpl;

import FileMonitor.BuscarFicheroRequest;
import FileMonitor.BuscarFicheroResponse.Fichero;


public class FileMonitorControllerTest {
	
	private FileMonitorController controller;
	
	private FileMonitorService service;

	@Before
	public void init() {
		this.controller = new FileMonitorController();
		service = Mockito.mock(FileMonitorServiceImpl.class);
		controller.setFileMonitorService(service);
	}
	
	@Test
	public void getFilesWithResults() {

		// prepara el test
		List<Fichero> listFiles = new ArrayList<>();
		BuscarFicheroRequest request = new BuscarFicheroRequest();
		Fichero fichero = new Fichero();
		fichero.setCliente("Mercedes");
		fichero.setId("3");
		fichero.setFormato("XML");
		fichero.setNombreOriginal("MERC_001.TXT");
		listFiles.add(fichero);
		
		Mockito.when(service.search(request)).thenReturn(listFiles);

		// invoca metodo a probar
		final JsonResponse<List<Fichero>> files = this.controller.search(request);

		// Comprobar resultados
		Assert.assertNotNull(files);
		Assert.assertEquals(1, listFiles.size());
		Assert.assertEquals("Mercedes", listFiles.get(0).getCliente());
		Assert.assertEquals("3", listFiles.get(0).getId());
		Assert.assertEquals("XML", listFiles.get(0).getFormato());
		Assert.assertEquals("MERC_001.TXT", listFiles.get(0).getNombreOriginal());
		Mockito.verify(this.service, Mockito.atLeastOnce()).search(request);
	}
		
}
