package com.isb.channeling.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.service.ConfigurationService;
import com.isb.channeling.service.impl.ConfigurationServiceImpl;

import Configuration.AltaConfiguracionRequest;
import Configuration.EditarConfiguracionRequest;
import Configuration.ListarConfiguracionResponse;

public class ConfigurationControllerTest {

  private ConfigurationController controller;
  private ConfigurationService service;

  @Before
  public void init() {
    this.controller = new ConfigurationController();
    service = Mockito.mock(ConfigurationServiceImpl.class);
    controller.setConfigurationService(service);
  }

  @Test
  public void getConfigurationWithResultOK() {

    // prepara el test
    int id = 1;
    ListarConfiguracionResponse config = new ListarConfiguracionResponse();
    config.setSubalias("Virgi");
    config.setDestino("MFT");
    config.setOrigen("IPLA");
    config.setNameConf("Nombre");

    Mockito.when(service.findById(id)).thenReturn(config);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<ListarConfiguracionResponse>> response = this.controller.getConfiguration(id);

    // Comprobar resultados
    Assert.assertNotNull(response);
    Assert.assertEquals("Virgi", config.getSubalias());
    Assert.assertEquals("MFT", config.getDestino());
    Assert.assertEquals("IPLA", config.getOrigen());
    Assert.assertEquals("Nombre", config.getNameConf());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).findById(id);
  }

  @Test
  public void getConfigurationWithResultKO() {

    // prepara el test
    int id = 1;
    ListarConfiguracionResponse config = null;

    Mockito.when(service.findById(id)).thenReturn(config);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<ListarConfiguracionResponse>> response = this.controller.getConfiguration(id);

    // Comprobar resultados
    Assert.assertEquals(404, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).findById(id);
  }

  @Test
  public void shouldReturnIdConfigurationCreateOK() {

    // prepara el test
    String id = "1";
    AltaConfiguracionRequest currentConfiguration = new AltaConfiguracionRequest();
    Mockito.when(service.createConfiguration(currentConfiguration)).thenReturn(id);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.createConfiguration(currentConfiguration);

    // Comprobar resultados
    Assert.assertEquals("OK_CREATE_CONFIG", response.getBody().getData());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).createConfiguration(currentConfiguration);
  }

  @Test
  public void shouldReturnConfigurationCreateKO() {

    // prepara el test
    String id = "KO";
    AltaConfiguracionRequest currentConfiguration = new AltaConfiguracionRequest();
    Mockito.when(service.createConfiguration(currentConfiguration)).thenReturn(id);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.createConfiguration(currentConfiguration);

    // Comprobar resultados
    Assert.assertEquals("KO_CREATE_CONFIG", response.getBody().getError());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).createConfiguration(currentConfiguration);
  }

  @Test
  public void shouldReturnUpdateOK() {

    // prepara el test
    int id = 1;
    EditarConfiguracionRequest currentConfiguration = new EditarConfiguracionRequest();
    currentConfiguration.setIdConfig("1");
    Mockito.when(service.updateConfiguration(currentConfiguration)).thenReturn("OK");

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.updateConfiguration(id, currentConfiguration);

    // Comprobar resultados
    Assert.assertEquals("OK_UPDATE_CONFIG", response.getBody().getData());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).updateConfiguration(currentConfiguration);
  }

  @Test
  public void shouldReturnUpdateKO() {

    // prepara el test
    int id = 1;
    EditarConfiguracionRequest currentConfiguration = new EditarConfiguracionRequest();
    currentConfiguration.setIdConfig("1");
    Mockito.when(service.updateConfiguration(currentConfiguration)).thenReturn("3");

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.updateConfiguration(id, currentConfiguration);

    // Comprobar resultados
    Assert.assertEquals("KO_UPDATE_CONFIG", response.getBody().getError());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).updateConfiguration(currentConfiguration);
  }

  @Test
  public void shouldReturnConfigurationDeleteOK() {

    // prepara el test
    Number id = 1;
    AltaConfiguracionRequest currentConfiguration = new AltaConfiguracionRequest();
    Mockito.when(service.deleteConfigurationById(id)).thenReturn("OK");

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.deleteConfiguration(id);

    // Comprobar resultados
    Assert.assertEquals("OK_DELETE_CONFIG", response.getBody().getData());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).deleteConfigurationById(id);
  }

  @Test
  public void shouldReturnConfigurationDeleteKO() {

    // prepara el test
    Number id = 1;
    AltaConfiguracionRequest currentConfiguration = new AltaConfiguracionRequest();
    Mockito.when(service.deleteConfigurationById(id)).thenReturn("3");

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<String>> response = this.controller.deleteConfiguration(id);

    // Comprobar resultados
    Assert.assertEquals("KO_DELETE_CONFIG", response.getBody().getError());
    Assert.assertEquals(200, response.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).deleteConfigurationById(id);
  }

}
