package com.isb.channeling.controller;

import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.service.UserService;
import com.isb.channeling.service.impl.UserServiceImpl;

import User.EditarUsuariosRequest;
import User.ListarUsuariosResponse.User;
import User.NuevoUsuarioRequest;


public class UserControllerTest {
	
	private UserController controller;
	
	private UserService service;

	@Before
	public void init() {
		this.controller = new UserController();
		service = Mockito.mock(UserServiceImpl.class);
		controller.setUserService(service);
	}
	
	@Test
	public void getUserWithResults() {

		// prepara el test
		List<User> listUsers = new ArrayList<>();

		User usuario = new User();
		usuario.setName("Charles");
		usuario.setId("1");
		usuario.setRole("VIEWER");
		usuario.setEmail("charles_brown@gmail.com");
		usuario.setBirthName("Charles");
		usuario.setLastName("Brown");
		usuario.setCreationDate("24/03/2018");
		usuario.setLastLoginDate("24/03/2018");
		listUsers.add(usuario);
		
		Mockito.when(service.findAllUsers()).thenReturn(listUsers);

		// invoca metodo a probar
		final JsonResponse<List<User>> usuarios = this.controller.listAllUsers();

		// Comprobar resultados
		Assert.assertNotNull(usuarios);
		Assert.assertEquals(1, listUsers.size());
		Assert.assertEquals("Charles", listUsers.get(0).getName());
		Assert.assertEquals("1", listUsers.get(0).getId());
		Assert.assertEquals("VIEWER", listUsers.get(0).getRole());
		Assert.assertEquals("charles_brown@gmail.com", listUsers.get(0).getEmail());
		Assert.assertEquals("Charles", listUsers.get(0).getBirthName());
		Assert.assertEquals("Brown", listUsers.get(0).getLastName());
		Assert.assertEquals("24/03/2018", listUsers.get(0).getCreationDate());
		Assert.assertEquals("24/03/2018", listUsers.get(0).getLastLoginDate());
		Mockito.verify(this.service, Mockito.atLeastOnce()).findAllUsers();
	}
	
	
	@Test
	public void shouldGetUserFindByNameOK() {
				
		// prepara el test
		String nombre = "Charles";
		User usuario = new User();
		usuario.setName(nombre);
		usuario.setId("1");
		usuario.setRole("VIEWER");
		usuario.setEmail("charles_brown@gmail.com");
		usuario.setBirthName("Charles");
		usuario.setLastName("Brown");
		usuario.setCreationDate("24/03/2018");
		usuario.setLastLoginDate("24/03/2018");
		
		Mockito.when(service.findByName(nombre)).thenReturn(usuario);

		// invoca metodo a probar
		final ResponseEntity<JsonResponse<User>> user = this.controller.getUser(nombre);

		// Comprobar resultados
		Assert.assertNotNull(user);
		Assert.assertEquals(nombre, usuario.getName());
		Assert.assertEquals("1", usuario.getId());
		Assert.assertEquals("VIEWER", usuario.getRole());
		Assert.assertEquals("charles_brown@gmail.com", usuario.getEmail());
		Assert.assertEquals("Charles", usuario.getBirthName());
		Assert.assertEquals("Brown", usuario.getLastName());
		Assert.assertEquals("24/03/2018", usuario.getCreationDate());
		Assert.assertEquals("24/03/2018", usuario.getLastLoginDate());
		Assert.assertEquals(200, user.getStatusCode().value());
		Mockito.verify(this.service, Mockito.atLeastOnce()).findByName(nombre);
	}
	
	@Test
  public void shouldGetUserFindByNameEmpty() {
        
    // prepara el test
    User usuario = new User();
    usuario.setName("");
    usuario.setId("");
    usuario.setRole("");
    usuario.setEmail("");
    usuario.setBirthName("");
    usuario.setLastName("");
    usuario.setCreationDate("");
    usuario.setLastLoginDate("");
    
    Mockito.when(service.findByName(any())).thenReturn(usuario);

    // invoca metodo a probar
    final ResponseEntity<JsonResponse<User>> user = this.controller.getUser("nombre");

    // Comprobar resultados
    Assert.assertNotNull(user);
    Assert.assertEquals("USER_NOT_EXIST", user.getBody().getError());
    Assert.assertEquals(200, user.getStatusCode().value());
    Mockito.verify(this.service, Mockito.atLeastOnce()).findByName("nombre");
  }
	
	@Test
	public void shouldGetUserFindByNameKO() {
				
		// prepara el test
		String nombre = "Charles";
		User usuario = null;		
		Mockito.when(service.findByName(nombre)).thenReturn(usuario);

		// invoca metodo a probar
		final ResponseEntity<JsonResponse<User>> user = this.controller.getUser(nombre);

		// Comprobar resultados
		Assert.assertEquals(404, user.getStatusCode().value());
		Mockito.verify(this.service, Mockito.atLeastOnce()).findByName(nombre);
	}
	
	@Test
	public void shouldReturnIdUserCreateOK() {
		
		// prepara el test
		String id = "1";
		NuevoUsuarioRequest currentUser = new NuevoUsuarioRequest();
		Mockito.when(service.createUser(currentUser)).thenReturn(id);
		
		// invoca metodo a probar
		final ResponseEntity<JsonResponse<String>> response = this.controller.createUser(currentUser);
		
		// Comprobar resultados
		Assert.assertEquals("OK_CREATE_USER", response.getBody().getData());
		Assert.assertEquals(200, response.getStatusCode().value());
		Mockito.verify(this.service, Mockito.atLeastOnce()).createUser(currentUser);
	}
	
	@Test
	public void shouldReturnIdUserCreateKO() {
		
		// prepara el test
		String id = "KO";
		NuevoUsuarioRequest currentUser = new NuevoUsuarioRequest();
		Mockito.when(service.createUser(currentUser)).thenReturn(id);
		
		// invoca metodo a probar
		final ResponseEntity<JsonResponse<String>> response = this.controller.createUser(currentUser);
		
		// Comprobar resultados
		Assert.assertNotNull(response.getBody().getError());
		Assert.assertEquals(200, response.getStatusCode().value());
		Mockito.verify(this.service, Mockito.atLeastOnce()).createUser(currentUser);
	}
	
	@Test
	public void shouldReturnIdUserUpdateOK() {
		
		// prepara el test
		String id = "1";
		EditarUsuariosRequest currentUser = new EditarUsuariosRequest();
		Mockito.when(service.updateUser(currentUser)).thenReturn(id);
		
		// invoca metodo a probar
		final ResponseEntity<JsonResponse<String>> response = this.controller.updateUser(1, currentUser);
		
		// Comprobar resultados
		Assert.assertEquals("KO_UPDATE_USER", response.getBody().getError());
		Assert.assertEquals(200, response.getStatusCode().value());
		Mockito.verify(this.service, Mockito.atLeastOnce()).updateUser(currentUser);
	}
	
	@Test
	public void shouldReturnIdUserUpdateKO() {
		
		// prepara el test
		String id = "KO";
		EditarUsuariosRequest currentUser = new EditarUsuariosRequest();
		Mockito.when(service.updateUser(currentUser)).thenReturn(id);
		
		// invoca metodo a probar
		final ResponseEntity<JsonResponse<String>> response = this.controller.updateUser(1, currentUser);
		
		// Comprobar resultados
		Assert.assertEquals("KO_UPDATE_USER", response.getBody().getError());
		Assert.assertEquals(200, response.getStatusCode().value());
		Mockito.verify(this.service, Mockito.atLeastOnce()).updateUser(currentUser);
	}
	
	@Test
	public void shouldReturnNoContentDeleteOK() {
		
		// prepara el test
		int id = 1;
		String responseService = "OK";
		//Mockito.doNothing().when(service).deleteUserById(Mockito.anyInt());
		Mockito.when(service.deleteUserById(id)).thenReturn(responseService);
		
		// invoca metodo a probar
		final ResponseEntity<JsonResponse<String>> response = this.controller.deleteUser(id);
		
		// Comprobar resultados		
		Assert.assertEquals(200, response.getStatusCode().value());
		Mockito.verify(this.service, Mockito.atLeastOnce()).deleteUserById(id);
	}
		
}
