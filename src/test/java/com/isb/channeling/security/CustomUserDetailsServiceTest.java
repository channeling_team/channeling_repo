package com.isb.channeling.security;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.isb.channeling.service.impl.UserServiceImpl;

import User.ListarUsuariosResponse.User;

public class CustomUserDetailsServiceTest {

	private UserServiceImpl userService;

	private CustomUserDetailsService customDetailService;

	@Before
	public void init() {
		this.customDetailService = new CustomUserDetailsService();
		userService = Mockito.mock(UserServiceImpl.class);
		customDetailService.setUserService(userService);
	}

	@Test(expected = UsernameNotFoundException.class)
	public void loadUserByUsernameNull() {
		// prepara el test
		String name = null;

		// invoca metodo a probar
		customDetailService.loadUserByUsername(name);
	}

	@Test(expected = UsernameNotFoundException.class)
	public void loadUserByUsernameNotFound() {
		// prepara el test
		String name = "charles";
		Mockito.when(userService.findByName(name)).thenReturn(null);

		// invoca metodo a probar
		customDetailService.loadUserByUsername(name);
	}

	@Test
	public void loadUserByUsernameOk() {

		// prepara el test
		String nombre = "CHARLES";
		User usuario = new User();
		usuario.setName(nombre);
		usuario.setId("1");
		usuario.setRole("VIEWER");
		usuario.setEmail("charles_brown@gmail.com");
		usuario.setBirthName("Charles");
		usuario.setLastName("Brown");
		usuario.setCreationDate("24/03/2018");
		usuario.setLastLoginDate("24/03/2018");

		Mockito.when(userService.logInUser(nombre)).thenReturn(usuario);

		// invoca metodo a probar
		UserDetails result = customDetailService.loadUserByUsername(nombre);

		// Comprobar resultados
		Assert.assertNotNull(result);
		Assert.assertEquals(result.getUsername(), nombre);

	}

}
