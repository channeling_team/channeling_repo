package com.isb.channeling.security;

import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.isb.channeling.domain.JsonResponse;
import com.isb.channeling.util.AuthUtil;
import com.isb.channeling.util.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Utils.class, AuthUtil.class})
public class AuthControllerTest {

	AuthController controller;

	@Mock
	LdapTemplate ldapTemplate;

	@Mock
	UserDetailsService customUserDetailsService;

	private static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCWed2Kvl2NIllDbN99PYpizdg1Vxn3fcn2GWBEi+zcnebPQQwrxpb81uxs4/IkVzarubSu1uoI7nl/RAkud5B3jhA3styPGK8DN2UE+gYW72oziHC5IEnlMUEaFqrCduXHVc66/xLjDDzZ/fttijM9Wj6b9gMPE0RyZo4I5YBcEwIDAQAB";

	@Before
	public void init() {
		this.controller = new AuthController();
		controller.setLdapTemplate(ldapTemplate);
		controller.setCustomUserDetailsService(customUserDetailsService);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void authenticateWithToken() {
		// prepara el test
		String token = "QjQ2NTc5NDJFNTM4RTQwMTMyMkI4M0M5IzE4MC4xMDEuMTE2LjkjMTUwNDA4NjMyMDkyOSNQRDk0Yld3Z2RtVnljMmx2YmowaU1TNHdJaUJsYm1OdlpHbHVaejBpU1ZOUExUZzROVGt0TVNJL1BqeDBiMnRsYmtSbFptbHVhWFJwYjI0K1BHeHZZMkZzUlcxcGRIUmxjajVUVTBORFFqd3ZiRzlqWVd4RmJXbDBkR1Z5UGp4MWMyVnlTVVErZUVsVE1USTNORGM4TDNWelpYSkpSRDQ4Ym1GdFpUNUZVMUJCVWxwQklFUkpRVm9zSUZOUFRFVkVRVVFnUWtWTVJVNDhMMjVoYldVK1BHRnNhV0Z6UG5oSlV6RXlOelEzUEM5aGJHbGhjejQ4ZFhObGNrTnZjbkErZUVsVE1USTNORGM4TDNWelpYSkRiM0p3UGp3dmRHOXJaVzVFWldacGJtbDBhVzl1UGc9PSNERVNlZGUvQ0JDL1BLQ1M1UGFkZGluZyN2MSNDb3JwSW50cmFuZXQjTk9UIFVTRUQjU0hBMXdpdGhSU0EjQ09pRkJHdkhOVnRGQ0pFRWN5blN1czNtTmh0enNLYmZsVkRmWUN5RkpuaW9GSFIxbmNRcVpCVEd0N1IxSkJhcStCc25ucGhGZFZYenFoWmdZN3d4N21wYTFDWmVDakVjcVViR2NHNUx4UEFKdHkyVXlqd1NmYkR2bE9QaWtxZXBwbzNFSnltbWg0THJ1NUJOdW1BSDQ5RHM3M3V1YXVhWFRjZGpEZUxrM0lJPQ==";

		List<Object> aList = new ArrayList<Object>();
		aList.add(Base64.decodeBase64(PUBLIC_KEY));
		String name = "charles";
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_TEST"));

		Mockito.when(customUserDetailsService.loadUserByUsername(any(String.class)))
		    .thenReturn(new User(name, StringUtils.EMPTY, authorities));
		Mockito.when(ldapTemplate.search(any(String.class), any(String.class), any(AttributesMapper.class)))
		    .thenReturn(aList);

		// invoca metodo a probar
		String result = controller.authenticate(token, null);

		// Comprobar resultados
		Assert.assertNotNull(result);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void authenticateWithTokenInvalid() {
		
		controller.setCheckExpired(true);
		
		// prepara el test
		String token = "QjQ2NTc5NDJFNTM4RTQwMTMyMkI4M0M5IzE4MC4xMDEuMTE2LjkjMTUwNDA4NjMyMDkyOSNQRDk0Yld3Z2RtVnljMmx2YmowaU1TNHdJaUJsYm1OdlpHbHVaejBpU1ZOUExUZzROVGt0TVNJL1BqeDBiMnRsYmtSbFptbHVhWFJwYjI0K1BHeHZZMkZzUlcxcGRIUmxjajVUVTBORFFqd3ZiRzlqWVd4RmJXbDBkR1Z5UGp4MWMyVnlTVVErZUVsVE1USTNORGM4TDNWelpYSkpSRDQ4Ym1GdFpUNUZVMUJCVWxwQklFUkpRVm9zSUZOUFRFVkVRVVFnUWtWTVJVNDhMMjVoYldVK1BHRnNhV0Z6UG5oSlV6RXlOelEzUEM5aGJHbGhjejQ4ZFhObGNrTnZjbkErZUVsVE1USTNORGM4TDNWelpYSkRiM0p3UGp3dmRHOXJaVzVFWldacGJtbDBhVzl1UGc9PSNERVNlZGUvQ0JDL1BLQ1M1UGFkZGluZyN2MSNDb3JwSW50cmFuZXQjTk9UIFVTRUQjU0hBMXdpdGhSU0EjQ09pRkJHdkhOVnRGQ0pFRWN5blN1czNtTmh0enNLYmZsVkRmWUN5RkpuaW9GSFIxbmNRcVpCVEd0N1IxSkJhcStCc25ucGhGZFZYenFoWmdZN3d4N21wYTFDWmVDakVjcVViR2NHNUx4UEFKdHkyVXlqd1NmYkR2bE9QaWtxZXBwbzNFSnltbWg0THJ1NUJOdW1BSDQ5RHM3M3V1YXVhWFRjZGpEZUxrM0lJPQ==";

		List<Object> aList = new ArrayList<Object>();
		aList.add(Base64.decodeBase64(PUBLIC_KEY));
		String name = "charles";
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_TEST"));

		Mockito.when(customUserDetailsService.loadUserByUsername(any(String.class)))
		    .thenReturn(new User(name, StringUtils.EMPTY, authorities));
		Mockito.when(ldapTemplate.search(any(String.class), any(String.class), any(AttributesMapper.class)))
		    .thenReturn(aList);
		
		PowerMockito.mockStatic(AuthUtil.class);
		PowerMockito.when(AuthUtil.tokenIsInValid(any(), any())).thenReturn(true);

		// invoca metodo a probar
		String result = controller.authenticate(token, null);

		// Comprobar resultados
		Assert.assertNotNull(result);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void authenticateWithTokenAndExpiry() {
		
		controller.setCheckExpired(true);
		// prepara el test
		String token = "QjQ2NTc5NDJFNTM4RTQwMTMyMkI4M0M5IzE4MC4xMDEuMTE2LjkjMTUwNDA4NjMyMDkyOSNQRDk0Yld3Z2RtVnljMmx2YmowaU1TNHdJaUJsYm1OdlpHbHVaejBpU1ZOUExUZzROVGt0TVNJL1BqeDBiMnRsYmtSbFptbHVhWFJwYjI0K1BHeHZZMkZzUlcxcGRIUmxjajVUVTBORFFqd3ZiRzlqWVd4RmJXbDBkR1Z5UGp4MWMyVnlTVVErZUVsVE1USTNORGM4TDNWelpYSkpSRDQ4Ym1GdFpUNUZVMUJCVWxwQklFUkpRVm9zSUZOUFRFVkVRVVFnUWtWTVJVNDhMMjVoYldVK1BHRnNhV0Z6UG5oSlV6RXlOelEzUEM5aGJHbGhjejQ4ZFhObGNrTnZjbkErZUVsVE1USTNORGM4TDNWelpYSkRiM0p3UGp3dmRHOXJaVzVFWldacGJtbDBhVzl1UGc9PSNERVNlZGUvQ0JDL1BLQ1M1UGFkZGluZyN2MSNDb3JwSW50cmFuZXQjTk9UIFVTRUQjU0hBMXdpdGhSU0EjQ09pRkJHdkhOVnRGQ0pFRWN5blN1czNtTmh0enNLYmZsVkRmWUN5RkpuaW9GSFIxbmNRcVpCVEd0N1IxSkJhcStCc25ucGhGZFZYenFoWmdZN3d4N21wYTFDWmVDakVjcVViR2NHNUx4UEFKdHkyVXlqd1NmYkR2bE9QaWtxZXBwbzNFSnltbWg0THJ1NUJOdW1BSDQ5RHM3M3V1YXVhWFRjZGpEZUxrM0lJPQ==";

		List<Object> aList = new ArrayList<Object>();
		aList.add(Base64.decodeBase64(PUBLIC_KEY));
		String name = "charles";
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_TEST"));

		Mockito.when(customUserDetailsService.loadUserByUsername(any(String.class)))
		    .thenReturn(new User(name, StringUtils.EMPTY, authorities));
		Mockito.when(ldapTemplate.search(any(String.class), any(String.class), any(AttributesMapper.class)))
		    .thenReturn(aList);
		
		PowerMockito.mockStatic(AuthUtil.class);
		PowerMockito.when(AuthUtil.hasExpired(any())).thenReturn(true);

		// invoca metodo a probar
		String result = controller.authenticate(token, null);

		// Comprobar resultados
		Assert.assertNotNull(result);

	}

	@Test
	@SuppressWarnings("unchecked")
	public void authenticateWithCookie() {
		
		String cookie = "B4657942E538470132814004#180.101.116.9#1504083476270#1680000#1504081860270#64000#PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iSVNPLTg4NTktMSI/Pjxjb29raWU+PGRlZmluaXRpb25OYW1lPk5ld1VzZXJQYXNzd29yZENvb2tpZTwvZGVmaW5pdGlvbk5hbWU+PGxvY2FsRW1pdHRlcj5TU0NDQjwvbG9jYWxFbWl0dGVyPjx1c2VySUQ+eElTMTI3NDc8L3VzZXJJRD48bmFtZT5FU1BBUlpBIERJQVosIFNPTEVEQUQgQkVMRU48L25hbWU+PGFsaWFzPnhJUzEyNzQ3PC9hbGlhcz48dXNlckNvcnA+eElTMTI3NDc8L3VzZXJDb3JwPjwvY29va2llPg==#DESede/CBC/PKCS5Padding#v1#CorpIntranet#NOT_USED#SHA1withRSA#f9K+QrmKFlDB7Rc7TIAHJag+ha/quY+xT6ompNFlnozpQuxZFPoJHqMOoDCmQZ7lhX6EVOMR50pDiU3rlJlEdMJcxkveNO02NRo5Y8YcBMxRsM22NZkw39gzqVknEWKjpKD4P97uHRMRTkkcTMuvXFNxMeib5rh71wyh2aWqukU=";

		List<Object> aList = new ArrayList<Object>();
		aList.add(Base64.decodeBase64(PUBLIC_KEY));
		String name = "charles";
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_TEST"));

		Mockito.when(customUserDetailsService.loadUserByUsername(any(String.class)))
		    .thenReturn(new User(name, StringUtils.EMPTY, authorities));
		Mockito.when(ldapTemplate.search(any(String.class), any(String.class), any(AttributesMapper.class)))
		    .thenReturn(aList);

		// invoca metodo a probar
		String result = controller.authenticate(null, cookie);

		// Comprobar resultados
		Assert.assertNotNull(result);

	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void authenticateWithCookieInvalid() {
		
		String cookie = "B4657942E538470132814004#180.101.116.9#1504083476270#1680000#1504081860270#64000#PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iSVNPLTg4NTktMSI/Pjxjb29raWU+PGRlZmluaXRpb25OYW1lPk5ld1VzZXJQYXNzd29yZENvb2tpZTwvZGVmaW5pdGlvbk5hbWU+PGxvY2FsRW1pdHRlcj5TU0NDQjwvbG9jYWxFbWl0dGVyPjx1c2VySUQ+eElTMTI3NDc8L3VzZXJJRD48bmFtZT5FU1BBUlpBIERJQVosIFNPTEVEQUQgQkVMRU48L25hbWU+PGFsaWFzPnhJUzEyNzQ3PC9hbGlhcz48dXNlckNvcnA+eElTMTI3NDc8L3VzZXJDb3JwPjwvY29va2llPg==#DESede/CBC/PKCS5Padding#v1#CorpIntranet#NOT_USED#SHA1withRSA#f9K+QrmKFlDB7Rc7TIAHJag+ha/quY+xT6ompNFlnozpQuxZFPoJHqMOoDCmQZ7lhX6EVOMR50pDiU3rlJlEdMJcxkveNO02NRo5Y8YcBMxRsM22NZkw39gzqVknEWKjpKD4P97uHRMRTkkcTMuvXFNxMeib5rh71wyh2aWqukU=";

		List<Object> aList = new ArrayList<Object>();
		aList.add(Base64.decodeBase64(PUBLIC_KEY));
		String name = "charles";
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_TEST"));

		Mockito.when(customUserDetailsService.loadUserByUsername(any(String.class)))
		    .thenReturn(new User(name, StringUtils.EMPTY, authorities));
		Mockito.when(ldapTemplate.search(any(String.class), any(String.class), any(AttributesMapper.class)))
		    .thenReturn(aList);
		
		PowerMockito.mockStatic(AuthUtil.class);
		PowerMockito.when(AuthUtil.tokenIsInValid(any(), any())).thenReturn(true);

		// invoca metodo a probar
		String result = controller.authenticate(null, cookie);

		// Comprobar resultados
		Assert.assertNotNull(result);

	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void authenticateWithCookieAndCheckExpiry() {
		
		controller.setCheckExpired(true);

		String cookie = "B4657942E538470132814004#180.101.116.9#1504083476270#1680000#1504081860270#64000#PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iSVNPLTg4NTktMSI/Pjxjb29raWU+PGRlZmluaXRpb25OYW1lPk5ld1VzZXJQYXNzd29yZENvb2tpZTwvZGVmaW5pdGlvbk5hbWU+PGxvY2FsRW1pdHRlcj5TU0NDQjwvbG9jYWxFbWl0dGVyPjx1c2VySUQ+eElTMTI3NDc8L3VzZXJJRD48bmFtZT5FU1BBUlpBIERJQVosIFNPTEVEQUQgQkVMRU48L25hbWU+PGFsaWFzPnhJUzEyNzQ3PC9hbGlhcz48dXNlckNvcnA+eElTMTI3NDc8L3VzZXJDb3JwPjwvY29va2llPg==#DESede/CBC/PKCS5Padding#v1#CorpIntranet#NOT_USED#SHA1withRSA#f9K+QrmKFlDB7Rc7TIAHJag+ha/quY+xT6ompNFlnozpQuxZFPoJHqMOoDCmQZ7lhX6EVOMR50pDiU3rlJlEdMJcxkveNO02NRo5Y8YcBMxRsM22NZkw39gzqVknEWKjpKD4P97uHRMRTkkcTMuvXFNxMeib5rh71wyh2aWqukU=";

		List<Object> aList = new ArrayList<Object>();
		aList.add(Base64.decodeBase64(PUBLIC_KEY));
		String name = "charles";
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_TEST"));

		Mockito.when(customUserDetailsService.loadUserByUsername(any(String.class)))
		    .thenReturn(new User(name, StringUtils.EMPTY, authorities));
		Mockito.when(ldapTemplate.search(any(String.class), any(String.class), any(AttributesMapper.class)))
		    .thenReturn(aList);
		
		PowerMockito.mockStatic(AuthUtil.class);
		PowerMockito.when(AuthUtil.hasExpired(cookie)).thenReturn(true);

		// invoca metodo a probar
		String result = controller.authenticate(null, cookie);

		// Comprobar resultados
		Assert.assertNotNull(result);

	}


	@Test
	public void authenticateBlankToken() {

		// invoca metodo a probar
		String result = controller.authenticate(StringUtils.EMPTY, null);

		// Comprobar resultados
		Assert.assertNotNull(result);
	}

	@Test
	public void getRolesOK() {

		// prepara el test
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_TEST"));

		PowerMockito.mockStatic(Utils.class);
		PowerMockito.when(Utils.getLoggedInAuthorities()).thenReturn(authorities);

		// invoca metodo a probar
		JsonResponse<List<String>> result = controller.getRoles();

		// Comprobar resultados
		Assert.assertNotNull(result);
		Assert.assertEquals(result.getData().get(0), authorities.iterator().next().getAuthority());

	}
	
	@Test
	public void checkLogout() {
		
		String redirect = controller.logout();
		
		Assert.assertNotNull(redirect);
	}
	
	@Test
  public void checkHeartBeat() {
    
	  ResponseEntity<Void> response = controller.getHeartbeat();
    
    Assert.assertNotNull(response);
    Assert.assertEquals("200", response.getStatusCode().toString());
  }
	
	

}
